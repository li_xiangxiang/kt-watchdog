package org.pentaho.di.core.database;

import org.pentaho.di.core.plugins.DatabaseMetaPlugin;

/**
 * oracle数据库连接meta
 */
@DatabaseMetaPlugin(type = "Oracle", typeDescription = "Oracle")
public class OracleDatabaseMeta extends OracleRDBDatabaseMeta {

}
