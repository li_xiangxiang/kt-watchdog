package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtDispatcherStepsMapper;
import com.hzzftech.watchdog.busi.domain.KtDispatcherSteps;
import com.hzzftech.watchdog.busi.service.IKtDispatcherStepsService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 任务调度执行步骤Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
@Service
public class KtDispatcherStepsServiceImpl implements IKtDispatcherStepsService 
{
    @Autowired
    private KtDispatcherStepsMapper ktDispatcherStepsMapper;

    /**
     * 查询任务调度执行步骤
     * 
     * @param id 任务调度执行步骤主键
     * @return 任务调度执行步骤
     */
    @Override
    public KtDispatcherSteps selectKtDispatcherStepsById(Long id)
    {
        return ktDispatcherStepsMapper.selectKtDispatcherStepsById(id);
    }

    /**
     * 查询任务调度执行步骤列表
     * 
     * @param ktDispatcherSteps 任务调度执行步骤
     * @return 任务调度执行步骤
     */
    @Override
    public List<KtDispatcherSteps> selectKtDispatcherStepsList(KtDispatcherSteps ktDispatcherSteps)
    {
        return ktDispatcherStepsMapper.selectKtDispatcherStepsList(ktDispatcherSteps);
    }

    /**
     * 新增任务调度执行步骤
     * 
     * @param ktDispatcherSteps 任务调度执行步骤
     * @return 结果
     */
    @Override
    public int insertKtDispatcherSteps(KtDispatcherSteps ktDispatcherSteps)
    {
        return ktDispatcherStepsMapper.insertKtDispatcherSteps(ktDispatcherSteps);
    }

    /**
     * 修改任务调度执行步骤
     * 
     * @param ktDispatcherSteps 任务调度执行步骤
     * @return 结果
     */
    @Override
    public int updateKtDispatcherSteps(KtDispatcherSteps ktDispatcherSteps)
    {
        return ktDispatcherStepsMapper.updateKtDispatcherSteps(ktDispatcherSteps);
    }

    /**
     * 批量删除任务调度执行步骤
     * 
     * @param ids 需要删除的任务调度执行步骤主键
     * @return 结果
     */
    @Override
    public int deleteKtDispatcherStepsByIds(String ids)
    {
        return ktDispatcherStepsMapper.deleteKtDispatcherStepsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务调度执行步骤信息
     * 
     * @param id 任务调度执行步骤主键
     * @return 结果
     */
    @Override
    public int deleteKtDispatcherStepsById(String id)
    {
        return ktDispatcherStepsMapper.deleteKtDispatcherStepsById(id);
    }

    @Override
    public List<KtDispatcherSteps> selectKtDispatcherStepsByTaskId(String taskId) {
        return ktDispatcherStepsMapper.selectKtDispatcherStepsByTaskId(taskId);
    }
}
