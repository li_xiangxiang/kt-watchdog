package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtRepository;
import com.hzzftech.watchdog.busi.domain.KtRepositoryLog;
import com.hzzftech.watchdog.common.utils.ShiroUtils;

/**
 * 文件资源库操作日志Service接口
 * 
 * @author liquanxiang
 * @date 2021-11-24
 */
public interface IKtRepositoryLogService 
{
    /**
     * 查询文件资源库操作日志
     * 
     * @param id 文件资源库操作日志主键
     * @return 文件资源库操作日志
     */
    public KtRepositoryLog selectKtRepositoryLogById(Long id);

    /**
     * 查询文件资源库操作日志列表
     * 
     * @param ktRepositoryLog 文件资源库操作日志
     * @return 文件资源库操作日志集合
     */
    public List<KtRepositoryLog> selectKtRepositoryLogList(KtRepositoryLog ktRepositoryLog);

    /**
     * 新增文件资源库操作日志
     * 
     * @param ktRepositoryLog 文件资源库操作日志
     * @return 结果
     */
    public int insertKtRepositoryLog(KtRepositoryLog ktRepositoryLog);

    /**
     * 修改文件资源库操作日志
     * 
     * @param ktRepositoryLog 文件资源库操作日志
     * @return 结果
     */
    public int updateKtRepositoryLog(KtRepositoryLog ktRepositoryLog);

    /**
     * 批量删除文件资源库操作日志
     * 
     * @param ids 需要删除的文件资源库操作日志主键集合
     * @return 结果
     */
    public int deleteKtRepositoryLogByIds(String ids);

    /**
     * 删除文件资源库操作日志信息
     * 
     * @param id 文件资源库操作日志主键
     * @return 结果
     */
    public int deleteKtRepositoryLogById(Long id);

    default void addCommonHandle(KtRepositoryLog ktRepositoryLog) {
        ktRepositoryLog.setUpdatedBy(ShiroUtils.getSysUser().getUserId());
        ktRepositoryLog.setUpdatedTime(new Date());
    }
}
