package com.hzzftech.watchdog.busi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 任务调度执行步骤对象 kt_dispatcher_steps
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public class KtDispatcherSteps extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 排序 */
    @Excel(name = "排序")
    private Long taskSort;

    /** 步骤名称 */
    @Excel(name = "步骤名称")
    private String stepName;

    /** copyNr */
    @Excel(name = "copyNr")
    private Long copy;

    /** 读取 */
    @Excel(name = "读取")
    private Long linesRead;

    /** 写入 */
    @Excel(name = "写入")
    private Long linesWritten;

    /** 输入 */
    @Excel(name = "输入")
    private Long linesInput;

    /** 输出 */
    @Excel(name = "输出")
    private Long linesOutput;

    /** 更新 */
    @Excel(name = "更新")
    private Long linesUpdated;

    /** 拒绝 */
    @Excel(name = "拒绝")
    private Long linesRejected;

    /** 错误 */
    @Excel(name = "错误")
    private Long logErrors;

    /** 状态 */
    @Excel(name = "状态")
    private String statusDescription;

    /** 耗时 */
    @Excel(name = "耗时")
    private Long logSeconds;

    /** 速度 */
    @Excel(name = "速度")
    private String speed;

    /**  */
    @Excel(name = "")
    private String logPriortty;

    /**  */
    @Excel(name = "")
    private String stopped;

    /**  */
    @Excel(name = "")
    private String paused;

    /** 任务ID */
    @Excel(name = "任务ID")
    private String taskId;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setTaskSort(Long taskSort) 
    {
        this.taskSort = taskSort;
    }

    public Long getTaskSort() 
    {
        return taskSort;
    }
    public void setStepName(String stepName) 
    {
        this.stepName = stepName;
    }

    public String getStepName() 
    {
        return stepName;
    }
    public void setCopy(Long copy) 
    {
        this.copy = copy;
    }

    public Long getCopy() 
    {
        return copy;
    }
    public void setLinesRead(Long linesRead) 
    {
        this.linesRead = linesRead;
    }

    public Long getLinesRead() 
    {
        return linesRead;
    }
    public void setLinesWritten(Long linesWritten) 
    {
        this.linesWritten = linesWritten;
    }

    public Long getLinesWritten() 
    {
        return linesWritten;
    }
    public void setLinesInput(Long linesInput) 
    {
        this.linesInput = linesInput;
    }

    public Long getLinesInput() 
    {
        return linesInput;
    }
    public void setLinesOutput(Long linesOutput) 
    {
        this.linesOutput = linesOutput;
    }

    public Long getLinesOutput() 
    {
        return linesOutput;
    }
    public void setLinesUpdated(Long linesUpdated) 
    {
        this.linesUpdated = linesUpdated;
    }

    public Long getLinesUpdated() 
    {
        return linesUpdated;
    }
    public void setLinesRejected(Long linesRejected) 
    {
        this.linesRejected = linesRejected;
    }

    public Long getLinesRejected() 
    {
        return linesRejected;
    }
    public void setLogErrors(Long logErrors) 
    {
        this.logErrors = logErrors;
    }

    public Long getLogErrors() 
    {
        return logErrors;
    }
    public void setStatusDescription(String statusDescription) 
    {
        this.statusDescription = statusDescription;
    }

    public String getStatusDescription() 
    {
        return statusDescription;
    }
    public void setLogSeconds(Long logSeconds) 
    {
        this.logSeconds = logSeconds;
    }

    public Long getLogSeconds() 
    {
        return logSeconds;
    }
    public void setSpeed(String speed) 
    {
        this.speed = speed;
    }

    public String getSpeed() 
    {
        return speed;
    }
    public void setLogPriortty(String logPriortty) 
    {
        this.logPriortty = logPriortty;
    }

    public String getLogPriortty() 
    {
        return logPriortty;
    }
    public void setStopped(String stopped) 
    {
        this.stopped = stopped;
    }

    public String getStopped() 
    {
        return stopped;
    }
    public void setPaused(String paused) 
    {
        this.paused = paused;
    }

    public String getPaused() 
    {
        return paused;
    }
    public void setTaskId(String taskId) 
    {
        this.taskId = taskId;
    }

    public String getTaskId() 
    {
        return taskId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskSort", getTaskSort())
            .append("stepName", getStepName())
            .append("copy", getCopy())
            .append("linesRead", getLinesRead())
            .append("linesWritten", getLinesWritten())
            .append("linesInput", getLinesInput())
            .append("linesOutput", getLinesOutput())
            .append("linesUpdated", getLinesUpdated())
            .append("linesRejected", getLinesRejected())
            .append("logErrors", getLogErrors())
            .append("statusDescription", getStatusDescription())
            .append("logSeconds", getLogSeconds())
            .append("speed", getSpeed())
            .append("logPriortty", getLogPriortty())
            .append("stopped", getStopped())
            .append("paused", getPaused())
            .append("taskId", getTaskId())
            .toString();
    }
}
