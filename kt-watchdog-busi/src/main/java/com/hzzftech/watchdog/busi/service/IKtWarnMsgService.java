package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtWarnMsg;

/**
 * kettle运行提醒信息Service接口
 * 
 * @author kt-watchdog
 * @date 2022-01-03
 */
public interface IKtWarnMsgService 
{
    /**
     * 查询kettle运行提醒信息
     * 
     * @param id kettle运行提醒信息主键
     * @return kettle运行提醒信息
     */
    public KtWarnMsg selectKtWarnMsgById(Long id);

    /**
     * 查询kettle运行提醒信息列表
     * 
     * @param ktWarnMsg kettle运行提醒信息
     * @return kettle运行提醒信息集合
     */
    public List<KtWarnMsg> selectKtWarnMsgList(KtWarnMsg ktWarnMsg);

    /**
     * 新增kettle运行提醒信息
     * 
     * @param ktWarnMsg kettle运行提醒信息
     * @return 结果
     */
    public int insertKtWarnMsg(KtWarnMsg ktWarnMsg);

    /**
     * 修改kettle运行提醒信息
     * 
     * @param ktWarnMsg kettle运行提醒信息
     * @return 结果
     */
    public int updateKtWarnMsg(KtWarnMsg ktWarnMsg);

    /**
     * 批量删除kettle运行提醒信息
     * 
     * @param ids 需要删除的kettle运行提醒信息主键集合
     * @return 结果
     */
    public int deleteKtWarnMsgByIds(String ids);

    /**
     * 删除kettle运行提醒信息信息
     * 
     * @param id kettle运行提醒信息主键
     * @return 结果
     */
    public int deleteKtWarnMsgById(Long id);

    default void addCommonHandler(KtWarnMsg msg) {
        msg.setCreateTime(new Date());
    }

    default void updateCommonHandler(KtWarnMsg msg){
        msg.setUpdateTime(new Date());
    }

    int sendTestMsg(String ids);
}
