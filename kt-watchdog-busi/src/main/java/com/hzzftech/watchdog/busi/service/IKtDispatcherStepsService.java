package com.hzzftech.watchdog.busi.service;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtDispatcherSteps;
import com.hzzftech.watchdog.busi.domain.KtExecutionLog;

/**
 * 任务调度执行步骤Service接口
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public interface IKtDispatcherStepsService 
{
    /**
     * 查询任务调度执行步骤
     * 
     * @param id 任务调度执行步骤主键
     * @return 任务调度执行步骤
     */
    public KtDispatcherSteps selectKtDispatcherStepsById(Long id);

    /**
     * 查询任务调度执行步骤列表
     * 
     * @param ktDispatcherSteps 任务调度执行步骤
     * @return 任务调度执行步骤集合
     */
    public List<KtDispatcherSteps> selectKtDispatcherStepsList(KtDispatcherSteps ktDispatcherSteps);

    /**
     * 新增任务调度执行步骤
     * 
     * @param ktDispatcherSteps 任务调度执行步骤
     * @return 结果
     */
    public int insertKtDispatcherSteps(KtDispatcherSteps ktDispatcherSteps);

    /**
     * 修改任务调度执行步骤
     * 
     * @param ktDispatcherSteps 任务调度执行步骤
     * @return 结果
     */
    public int updateKtDispatcherSteps(KtDispatcherSteps ktDispatcherSteps);

    /**
     * 批量删除任务调度执行步骤
     * 
     * @param ids 需要删除的任务调度执行步骤主键集合
     * @return 结果
     */
    public int deleteKtDispatcherStepsByIds(String ids);

    /**
     * 删除任务调度执行步骤信息
     * 
     * @param id 任务调度执行步骤主键
     * @return 结果
     */
    public int deleteKtDispatcherStepsById(String id);

    List<KtDispatcherSteps> selectKtDispatcherStepsByTaskId(String taskId);
}
