package com.hzzftech.watchdog.busi.utils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hzzftech.watchdog.busi.objs.WatchdogPageInfo;
import com.hzzftech.watchdog.common.core.page.PageDomain;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;
import com.hzzftech.watchdog.common.core.page.TableSupport;
import com.hzzftech.watchdog.common.utils.StringUtils;
import com.hzzftech.watchdog.common.utils.sql.SqlUtil;

import java.util.List;

public class PageInfoUtils {

    public static WatchdogPageInfo getInfo() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        String orderBy = null;
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize))
        {
            orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
        }
        return new WatchdogPageInfo(pageNum, pageSize, orderBy);
    }

    public static TableDataInfo getDataTable(List<?> list, int total)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(total);
        return rspData;
    }
}
