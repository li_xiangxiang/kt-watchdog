package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 任务调度日志对象 kt_execution_log
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public class KtExecutionLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 日志ID */
    private String id;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 执行方式 */
    @Excel(name = "执行方式")
    private String executionMethod;

    /** 远程服务ID */
    @Excel(name = "远程服务ID")
    private Long slaveId;

    /** 状态（1等待执行，2正在执行，3执行成功，4执行失败，5强制停止） */
    @Excel(name = "状态", readConverterExp = "1=等待执行，2正在执行，3执行成功，4执行失败，5强制停止")
    private String status;

    /** 配置 */
    @Excel(name = "配置")
    private String executionConfiguration;

    /** 任务日志详细信息 */
    @Excel(name = "任务日志详细信息")
    private String executionLog;

    /** 执行方式 1：串行，2并行 */
    @Excel(name = "执行方式 1：串行，2并行")
    private String executionType;

    /** 任务组 */
    @Excel(name = "任务组")
    private String taskGroup;

    /** carte服务的ID */
    @Excel(name = "carte服务的ID")
    private String carteId;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imagePath;

    /** 任务ID */
    @Excel(name = "任务ID")
    private Long taskId;

    /** 排序 */
    @Excel(name = "排序")
    private String taskSort;

    /** 批次 */
    @Excel(name = "批次")
    private Long batchId;

    /** 任务类型：1转换 2job 3任务组 */
    @Excel(name = "任务类型：1转换 2job 3任务组")
    private String taskType;

    // 任务仓库类型 0git仓库 1文件仓库
    private String dpRepoType;

    private Long jobId;

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    private String jobName;

    public int getDatePicker() {
        return datePicker;
    }

    public void setDatePicker(int datePicker) {
        this.datePicker = datePicker;
    }

    private int datePicker;



    public String getDpRepoType() {
        return dpRepoType;
    }

    public void setDpRepoType(String dpRepoType) {
        this.dpRepoType = dpRepoType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTaskName(String taskName)
    {
        this.taskName = taskName;
    }

    public String getTaskName() 
    {
        return taskName;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setExecutionMethod(String executionMethod) 
    {
        this.executionMethod = executionMethod;
    }

    public String getExecutionMethod() 
    {
        return executionMethod;
    }
    public void setSlaveId(Long slaveId) 
    {
        this.slaveId = slaveId;
    }

    public Long getSlaveId() 
    {
        return slaveId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setExecutionConfiguration(String executionConfiguration) 
    {
        this.executionConfiguration = executionConfiguration;
    }

    public String getExecutionConfiguration() 
    {
        return executionConfiguration;
    }
    public void setExecutionLog(String executionLog) 
    {
        this.executionLog = executionLog;
    }

    public String getExecutionLog() 
    {
        return executionLog;
    }
    public void setExecutionType(String executionType) 
    {
        this.executionType = executionType;
    }

    public String getExecutionType() 
    {
        return executionType;
    }
    public void setTaskGroup(String taskGroup) 
    {
        this.taskGroup = taskGroup;
    }

    public String getTaskGroup() 
    {
        return taskGroup;
    }
    public void setCarteId(String carteId)
    {
        this.carteId = carteId;
    }

    public String getCarteId()
    {
        return carteId;
    }
    public void setImagePath(String imagePath) 
    {
        this.imagePath = imagePath;
    }

    public String getImagePath() 
    {
        return imagePath;
    }
    public void setTaskId(Long taskId) 
    {
        this.taskId = taskId;
    }

    public Long getTaskId() 
    {
        return taskId;
    }
    public void setTaskSort(String taskSort) 
    {
        this.taskSort = taskSort;
    }

    public String getTaskSort() 
    {
        return taskSort;
    }
    public void setBatchId(Long batchId) 
    {
        this.batchId = batchId;
    }

    public Long getBatchId() 
    {
        return batchId;
    }
    public void setTaskType(String taskType) 
    {
        this.taskType = taskType;
    }

    public String getTaskType() 
    {
        return taskType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskName", getTaskName())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("executionMethod", getExecutionMethod())
            .append("slaveId", getSlaveId())
            .append("status", getStatus())
            .append("executionConfiguration", getExecutionConfiguration())
            .append("executionLog", getExecutionLog())
            .append("executionType", getExecutionType())
            .append("taskGroup", getTaskGroup())
            .append("carteId", getCarteId())
            .append("imagePath", getImagePath())
            .append("taskId", getTaskId())
            .append("taskSort", getTaskSort())
            .append("batchId", getBatchId())
            .append("taskType", getTaskType())
            .append("remark", getRemark())
            .toString();
    }
}
