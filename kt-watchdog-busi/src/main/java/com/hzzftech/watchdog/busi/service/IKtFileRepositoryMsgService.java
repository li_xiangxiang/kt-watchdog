package com.hzzftech.watchdog.busi.service;

import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtFileRepository;
import com.hzzftech.watchdog.busi.domain.KtFileRepositoryMsg;
import com.hzzftech.watchdog.busi.domain.KtRepositoryFile;
import com.hzzftech.watchdog.common.core.domain.Ztree;
import com.hzzftech.watchdog.common.core.domain.entity.SysDept;

/**
 * 文件资源库-文件信息Service接口
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
public interface IKtFileRepositoryMsgService 
{
    /**
     * 查询文件资源库-文件信息
     * 
     * @param id 文件资源库-文件信息主键
     * @return 文件资源库-文件信息
     */
    public KtFileRepositoryMsg selectKtFileRepositoryMsgById(Long id);

    /**
     * 查询文件资源库-文件信息列表
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 文件资源库-文件信息集合
     */
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgList(KtFileRepositoryMsg ktFileRepositoryMsg);
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgList2(KtFileRepositoryMsg ktFileRepositoryMsg);

    /**
     * 新增文件资源库-文件信息
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 结果
     */
    public int insertKtFileRepositoryMsg(KtFileRepositoryMsg ktFileRepositoryMsg);

    /**
     * 修改文件资源库-文件信息
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 结果
     */
    public int updateKtFileRepositoryMsg(KtFileRepositoryMsg ktFileRepositoryMsg);

    /**
     * 批量删除文件资源库-文件信息
     * 
     * @param ids 需要删除的文件资源库-文件信息主键集合
     * @return 结果
     */
    public int deleteKtFileRepositoryMsgByIds(String ids);

    /**
     * 删除文件资源库-文件信息信息
     * 
     * @param id 文件资源库-文件信息主键
     * @return 结果
     */
    public int deleteKtFileRepositoryMsgById(Long id);

    List<KtFileRepositoryMsg> selectFileRepositoryByParentId(Long repositoryId);

    List<KtFileRepositoryMsg> selectListByRepoId(Long repoId);

    List<Ztree> selectFileTree(Long repoId);

    List<KtFileRepositoryMsg> selectKtFileRepositoryMsgWithChild(Long getfRepoId, Long id);

    List<KtFileRepositoryMsg> selectListByRepoIds(String[] ancestors);

    KtFileRepositoryMsg selectKtFileRepositoryMsgByRepo(Long repoId);

    KtFileRepositoryMsg selectRepoById(Long repoId, Long parentId);

    KtFileRepositoryMsg selectRepositoryFileByRepoIdAndFid(Long repoId, Long id);

    List<KtFileRepositoryMsg> selectKtRepositoryByRepoId(Long repoId);

    int deleteKtFileRepositoryMsgByRepoId(long repoId);
}
