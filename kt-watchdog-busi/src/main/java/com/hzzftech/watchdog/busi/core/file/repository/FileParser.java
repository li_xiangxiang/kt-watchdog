package com.hzzftech.watchdog.busi.core.file.repository;

import java.io.File;
import java.util.List;

public interface FileParser<E extends FileParser> {

    E parse(File f);

    <L extends Object> List<L> getResultList();
}
