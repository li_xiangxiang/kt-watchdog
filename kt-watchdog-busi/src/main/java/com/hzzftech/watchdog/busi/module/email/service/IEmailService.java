package com.hzzftech.watchdog.busi.module.email.service;

import java.util.List;

public interface IEmailService {
    boolean sendAttachMail(String target, String subject, String content);
    boolean sendAttachMail(String target, String subject, String content, List<String> filepaths);
}
