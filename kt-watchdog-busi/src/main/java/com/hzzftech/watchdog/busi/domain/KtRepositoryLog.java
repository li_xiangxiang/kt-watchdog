package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 文件资源库操作日志对象 kt_repository_log
 * 
 * @author liquanxiang
 * @date 2021-11-24
 */
public class KtRepositoryLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** git仓库更新日志 */
    private Long id;

    /** 资源库ID */
    @Excel(name = "资源库ID")
    private Long repositoryId;

    /** git命令 */
    @Excel(name = "git命令")
    private String gitCommand;

    /** git命令的执行状态 */
    @Excel(name = "git命令的执行状态")
    private String gitExecutedStatus;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private Long updatedBy;

    /** git地址 */
    @Excel(name = "git地址")
    private String gitRemoteUri;

    /** git分支 */
    @Excel(name = "git分支")
    private String gitBranch;

    /** git最近一次提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "git最近一次提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gitCommitedTime;

    /** git最近一次hash */
    @Excel(name = "git最近一次hash")
    private String gitCommitedHash;

    /** git最近一次提交人 */
    @Excel(name = "git最近一次提交人")
    private String gitCommitedPerson;

    /** git最近一次提交信息 */
    @Excel(name = "git最近一次提交信息")
    private String gitCommitedMessage;

    /** git执行失败信息 */
    @Excel(name = "git执行失败信息")
    private String gitExecutedErrorMsg;

    private String userName;
    private String repositoryName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getGitCommitedPerson() {
        return gitCommitedPerson;
    }

    public void setGitCommitedPerson(String gitCommitedPerson) {
        this.gitCommitedPerson = gitCommitedPerson;
    }

    public Long getId()
    {
        return id;
    }
    public void setRepositoryId(Long repositoryId) 
    {
        this.repositoryId = repositoryId;
    }

    public Long getRepositoryId() 
    {
        return repositoryId;
    }
    public void setGitCommand(String gitCommand) 
    {
        this.gitCommand = gitCommand;
    }

    public String getGitCommand() 
    {
        return gitCommand;
    }
    public void setGitExecutedStatus(String gitExecutedStatus) 
    {
        this.gitExecutedStatus = gitExecutedStatus;
    }

    public String getGitExecutedStatus() 
    {
        return gitExecutedStatus;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setGitRemoteUri(String gitRemoteUri) 
    {
        this.gitRemoteUri = gitRemoteUri;
    }

    public String getGitRemoteUri() 
    {
        return gitRemoteUri;
    }
    public void setGitBranch(String gitBranch) 
    {
        this.gitBranch = gitBranch;
    }

    public String getGitBranch() 
    {
        return gitBranch;
    }
    public void setGitCommitedTime(Date gitCommitedTime) 
    {
        this.gitCommitedTime = gitCommitedTime;
    }

    public Date getGitCommitedTime() 
    {
        return gitCommitedTime;
    }
    public void setGitCommitedHash(String gitCommitedHash) 
    {
        this.gitCommitedHash = gitCommitedHash;
    }

    public String getGitCommitedHash() 
    {
        return gitCommitedHash;
    }
    public void setGitCommitedMessage(String gitCommitedMessage) 
    {
        this.gitCommitedMessage = gitCommitedMessage;
    }

    public String getGitCommitedMessage() 
    {
        return gitCommitedMessage;
    }
    public void setGitExecutedErrorMsg(String gitExecutedErrorMsg) 
    {
        this.gitExecutedErrorMsg = gitExecutedErrorMsg;
    }

    public String getGitExecutedErrorMsg() 
    {
        return gitExecutedErrorMsg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("repositoryId", getRepositoryId())
            .append("gitCommand", getGitCommand())
            .append("gitExecutedStatus", getGitExecutedStatus())
            .append("updatedTime", getUpdatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("gitRemoteUri", getGitRemoteUri())
            .append("gitBranch", getGitBranch())
            .append("gitCommitedTime", getGitCommitedTime())
            .append("gitCommitedHash", getGitCommitedHash())
            .append("gitCommitedMessage", getGitCommitedMessage())
            .append("gitExecutedErrorMsg", getGitExecutedErrorMsg())
            .toString();
    }
}
