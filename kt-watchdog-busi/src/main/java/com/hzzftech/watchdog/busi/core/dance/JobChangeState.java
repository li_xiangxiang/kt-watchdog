package com.hzzftech.watchdog.busi.core.dance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JobChangeState {

    public static Logger logger = LoggerFactory.getLogger(JobChangeState.class);

    private JobChangeState() {

    }

    public static JobChangeState instance = new JobChangeState();

    /**
     * 日志是否有更新
     */
    public volatile long version = 1;

    public JobChangeState getInstance() {

        return instance;
    }


    public synchronized void jobStateChange() {
        version ++;
        logger.info("版本变化[version]="+version);
    }

    public synchronized long getjobStateVersion() {
        logger.info("获取版本[version]="+version);
        return  version;
    }

}
