package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import java.util.Objects;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.core.util.CarteClient;
import com.hzzftech.watchdog.busi.domain.KtExecutionLog;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.http.HttpUtils;
import com.hzzftech.watchdog.system.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtRepositoryNodeMapper;
import com.hzzftech.watchdog.busi.domain.KtRepositoryNode;
import com.hzzftech.watchdog.busi.service.IKtRepositoryNodeService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 资源库节点Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
@Service
public class KtRepositoryNodeServiceImpl implements IKtRepositoryNodeService 
{
    @Autowired
    private KtRepositoryNodeMapper ktRepositoryNodeMapper;

    /**
     * 查询资源库节点
     * 
     * @param nodeId 资源库节点主键
     * @return 资源库节点
     */
    @Override
    public KtRepositoryNode selectKtRepositoryNodeByNodeId(Long nodeId)
    {
        return ktRepositoryNodeMapper.selectKtRepositoryNodeByNodeId(nodeId);
    }

    /**
     * 查询资源库节点列表
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 资源库节点
     */
    @Override
    public List<KtRepositoryNode> selectKtRepositoryNodeList(KtRepositoryNode ktRepositoryNode)
    {
        return ktRepositoryNodeMapper.selectKtRepositoryNodeList(ktRepositoryNode);
    }

    /**
     * 新增资源库节点
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 结果
     */
    @Override
    public int insertKtRepositoryNode(KtRepositoryNode ktRepositoryNode)
    {
        ktRepositoryNode.setStatus(BusiConstant.STATUS_YES);
        addCommonHandle(ktRepositoryNode);
        return ktRepositoryNodeMapper.insertKtRepositoryNode(ktRepositoryNode);
    }

    /**
     * 修改资源库节点
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 结果
     */
    @Override
    public int updateKtRepositoryNode(KtRepositoryNode ktRepositoryNode)
    {
        ktRepositoryNode.setStatus(BusiConstant.STATUS_YES);
        updateCommonHandle(ktRepositoryNode);
        return ktRepositoryNodeMapper.updateKtRepositoryNode(ktRepositoryNode);
    }

    /**
     * 批量删除资源库节点
     * 
     * @param nodeIds 需要删除的资源库节点主键
     * @return 结果
     */
    @Override
    public int deleteKtRepositoryNodeByNodeIds(String nodeIds)
    {
        return ktRepositoryNodeMapper.deleteKtRepositoryNodeByNodeIds(Convert.toStrArray(nodeIds));
    }

    /**
     * 删除资源库节点信息
     * 
     * @param nodeId 资源库节点主键
     * @return 结果
     */
    @Override
    public int deleteKtRepositoryNodeByNodeId(Long nodeId)
    {
        return ktRepositoryNodeMapper.deleteKtRepositoryNodeByNodeId(nodeId);
    }

    // 检查节点状态
    @Override
    public AjaxResult checkNodeStatus(Long nodeId) {
        if (Objects.isNull(nodeId)) {
            return AjaxResult.error();
        }

        KtRepositoryNode ktRepositoryNode = ktRepositoryNodeMapper.selectKtRepositoryNodeByNodeId(nodeId);
        if (Objects.isNull(ktRepositoryNode)) {
            return AjaxResult.error();
        }

        // 通过CarteClient客户端，检查节点状态
        CarteClient carteClient = new CarteClient(ktRepositoryNode);
        String status = carteClient.getStatusOrNull();
        if (Objects.isNull(status)) {
            return AjaxResult.error("节点异常");
        } else {
            return AjaxResult.success("节点正常");
        }
    }

    @Override
    public List<KtRepositoryNode> selectKtRepositoryNodeListMonitor(KtRepositoryNode ktRepositoryNode) {
        return ktRepositoryNodeMapper.selectKtRepositoryNodeListMonitor(ktRepositoryNode);
    }

    @Override
    public List<Item> selectNodeRunLogByCarteId(KtExecutionLog log) {
        return ktRepositoryNodeMapper.selectNodeRunLogByCarteId(log);
    }
}
