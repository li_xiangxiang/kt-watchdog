package com.hzzftech.watchdog.busi.service;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtRepositoryFile;
import com.hzzftech.watchdog.common.core.domain.entity.SysMenu;

/**
 * 仓库文件Service接口
 * 
 * @author liquanxiang
 * @date 2021-12-04
 */
public interface IKtRepositoryFileService 
{
    /**
     * 查询仓库文件
     * 
     * @param id 仓库文件主键
     * @return 仓库文件
     */
    public KtRepositoryFile selectKtRepositoryFileById(Long id);

    /**
     * 查询仓库文件列表
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 仓库文件集合
     */
    public List<KtRepositoryFile> selectKtRepositoryFileList(KtRepositoryFile ktRepositoryFile);

    /**
     * 新增仓库文件
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 结果
     */
    public int insertKtRepositoryFile(KtRepositoryFile ktRepositoryFile);

    /**
     * 修改仓库文件
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 结果
     */
    public int updateKtRepositoryFile(KtRepositoryFile ktRepositoryFile);

    /**
     * 批量删除仓库文件
     * 
     * @param ids 需要删除的仓库文件主键集合
     * @return 结果
     */
    public int deleteKtRepositoryFileByIds(String ids);

    /**
     * 删除仓库文件信息
     * 
     * @param id 仓库文件主键
     * @return 结果
     */
    public int deleteKtRepositoryFileById(Long id);

    void clearRepositoryById(Long repositoryId);

    void cleanRepositoryFileByIds(Long repositoryId, Long parentId, String fName);

    KtRepositoryFile selectRepositoryFileByIds(Long repositoryId, Long parentId, String fName);

    KtRepositoryFile selectKtRepositoryByName(String name);

    List<KtRepositoryFile> selectKtRepositoryFileByParentId(Long repositoryId);

    KtRepositoryFile selectRepoById(Long repositoryId,Long parentId);

    KtRepositoryFile selectRepositoryFileByRepoIdAndFid(Long repoId, Long fileId);

    List<KtRepositoryFile> selectKtRepositoryByRepoId(Long repoId);
}
