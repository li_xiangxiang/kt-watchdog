package com.hzzftech.watchdog.busi.service;

import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtDispatcherFailedMsg;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author kt-watchdog
 * @date 2022-03-06
 */
public interface IKtDispatcherFailedMsgService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public KtDispatcherFailedMsg selectKtDispatcherFailedMsgById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<KtDispatcherFailedMsg> selectKtDispatcherFailedMsgList(KtDispatcherFailedMsg ktDispatcherFailedMsg);

    /**
     * 新增【请填写功能名称】
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 结果
     */
    public int insertKtDispatcherFailedMsg(KtDispatcherFailedMsg ktDispatcherFailedMsg);

    /**
     * 修改【请填写功能名称】
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 结果
     */
    public int updateKtDispatcherFailedMsg(KtDispatcherFailedMsg ktDispatcherFailedMsg);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteKtDispatcherFailedMsgByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteKtDispatcherFailedMsgById(Long id);
}
