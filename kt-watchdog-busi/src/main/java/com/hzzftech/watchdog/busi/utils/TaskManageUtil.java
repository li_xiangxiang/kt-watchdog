package com.hzzftech.watchdog.busi.utils;

import java.util.HashMap;
import java.util.Map;

public class TaskManageUtil {
    private static TaskManageUtil instance = new TaskManageUtil();

    public static Map<String, Object> map = new HashMap<>();

    public static TaskManageUtil getInstance() {
        return instance;
    }

    public void add(String key, Object obj) {
        map.put(key, obj);
    }

    public Object get(String key) {
        return map.get(key);
    }

    public void remove(String key) {
        map.remove(key);
    }

    public int getSize() {
        return map.size();
    }

    private TaskManageUtil() {}
}
