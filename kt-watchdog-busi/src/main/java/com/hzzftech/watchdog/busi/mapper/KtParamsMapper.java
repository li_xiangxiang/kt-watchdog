package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtParams;

/**
 * 运行参数Mapper接口
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
public interface KtParamsMapper 
{
    /**
     * 查询运行参数
     * 
     * @param paramsId 运行参数主键
     * @return 运行参数
     */
    public KtParams selectKtParamsByParamsId(Long paramsId);

    /**
     * 查询运行参数列表
     * 
     * @param ktParams 运行参数
     * @return 运行参数集合
     */
    public List<KtParams> selectKtParamsList(KtParams ktParams);

    /**
     * 新增运行参数
     * 
     * @param ktParams 运行参数
     * @return 结果
     */
    public int insertKtParams(KtParams ktParams);

    /**
     * 修改运行参数
     * 
     * @param ktParams 运行参数
     * @return 结果
     */
    public int updateKtParams(KtParams ktParams);

    /**
     * 删除运行参数
     * 
     * @param paramsId 运行参数主键
     * @return 结果
     */
    public int deleteKtParamsByParamsId(Long paramsId);

    /**
     * 批量删除运行参数
     * 
     * @param paramsIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtParamsByParamsIds(String[] paramsIds);

    List<KtParams> selectKtParamsEnableListByGroupId(Long repoId);

    int deleteKtParamsByParamsGroupId(Long paramGroupId);
}
