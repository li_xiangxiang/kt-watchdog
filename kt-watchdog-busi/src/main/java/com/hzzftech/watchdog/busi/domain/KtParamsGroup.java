package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * 运行参数组对象 kt_params_group
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
public class KtParamsGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 参数组ID */
    private Long paramsGroupId;

    /** 参数组名 */
    @Excel(name = "参数组名")
    @NotBlank
    private String paramsGroupName;

    /** 状态 */
    @Excel(name = "状态")
    @NotBlank
    private String status;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    public void setParamsGroupId(Long paramsGroupId) 
    {
        this.paramsGroupId = paramsGroupId;
    }

    public Long getParamsGroupId() 
    {
        return paramsGroupId;
    }
    public void setParamsGroupName(String paramsGroupName) 
    {
        this.paramsGroupName = paramsGroupName;
    }

    public String getParamsGroupName() 
    {
        return paramsGroupName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("paramsGroupId", getParamsGroupId())
            .append("paramsGroupName", getParamsGroupName())
            .append("status", getStatus())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
