package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtRepositoryFile;
import org.apache.ibatis.annotations.Param;

/**
 * 仓库文件Mapper接口
 * 
 * @author liquanxiang
 * @date 2021-12-04
 */
public interface KtRepositoryFileMapper 
{
    /**
     * 查询仓库文件
     * 
     * @param id 仓库文件主键
     * @return 仓库文件
     */
    public KtRepositoryFile selectKtRepositoryFileById(Long id);

    /**
     * 查询仓库文件列表
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 仓库文件集合
     */
    public List<KtRepositoryFile> selectKtRepositoryFileList(KtRepositoryFile ktRepositoryFile);

    /**
     * 新增仓库文件
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 结果
     */
    public int insertKtRepositoryFile(KtRepositoryFile ktRepositoryFile);

    /**
     * 修改仓库文件
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 结果
     */
    public int updateKtRepositoryFile(KtRepositoryFile ktRepositoryFile);

    /**
     * 删除仓库文件
     * 
     * @param id 仓库文件主键
     * @return 结果
     */
    public int deleteKtRepositoryFileById(Long id);

    /**
     * 批量删除仓库文件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtRepositoryFileByIds(String[] ids);

    void deleteKtRepositoryFileByRepositoryId(Long repositoryId);

    void deleteRepositoryFileByIds(@Param("repositoryId") Long repositoryId, @Param("parentId") Long parentId, @Param("fName") String fName);

    KtRepositoryFile selectRepositoryFileByIds(@Param("repositoryId") Long repositoryId, @Param("parentId") Long parentId, @Param("fName") String fName);

    KtRepositoryFile selectKtRepositoryByName(@Param("name") String name);

    List<KtRepositoryFile> selectKtRepositoryFileByParentId(@Param("repositoryId") Long repositoryId);

    KtRepositoryFile selectRepoById(@Param("repositoryId") Long repositoryId,@Param("parentId") Long parentId);

    KtRepositoryFile selectRepositoryFileByRepoIdAndPid(@Param("repoId") Long repoId, @Param("fileId") Long fileId);

    List<KtRepositoryFile> selectKtRepositoryByRepoId(@Param("repoId") Long repoId);
}
