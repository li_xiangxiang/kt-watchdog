package com.hzzftech.watchdog.busi.controller;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.module.dingding.service.DingService;
import com.hzzftech.watchdog.busi.module.email.service.IEmailService;
import com.hzzftech.watchdog.common.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtWarnMsg;
import com.hzzftech.watchdog.busi.service.IKtWarnMsgService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * kettle运行提醒信息Controller
 * 
 * @author kt-watchdog
 * @date 2022-01-03
 */
@Controller
@RequestMapping("/busi/warnmsg")
public class KtWarnMsgController extends BaseController
{
    private String prefix = "busi/warnmsg";

    @Autowired
    private IKtWarnMsgService ktWarnMsgService;

    @RequiresPermissions("busi:warnmsg:view")
    @GetMapping()
    public String warnmsg()
    {
        return prefix + "/warnmsg";
    }

    /**
     * 查询kettle运行提醒信息列表
     */
    @RequiresPermissions("busi:warnmsg:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtWarnMsg ktWarnMsg)
    {
        startPage();
        List<KtWarnMsg> list = ktWarnMsgService.selectKtWarnMsgList(ktWarnMsg);
        return getDataTable(list);
    }

    /**
     * 导出kettle运行提醒信息列表
     */
    @RequiresPermissions("busi:warnmsg:export")
    @Log(title = "kettle运行提醒信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtWarnMsg ktWarnMsg)
    {
        List<KtWarnMsg> list = ktWarnMsgService.selectKtWarnMsgList(ktWarnMsg);
        ExcelUtil<KtWarnMsg> util = new ExcelUtil<KtWarnMsg>(KtWarnMsg.class);
        return util.exportExcel(list, "kettle运行提醒信息数据");
    }

    /**
     * 新增kettle运行提醒信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    private   String phoneRegExp = "^((13[0-9])|(14[5,7,9])|(15[0-3,5-9])|(166)|(17[3,5,6,7,8])" +
            "|(18[0-9])|(19[8,9]))\\d{8}$";

    private Pattern p = Pattern.compile(phoneRegExp);

    /**
     * 新增保存kettle运行提醒信息
     */
    @RequiresPermissions("busi:warnmsg:add")
    @Log(title = "kettle运行提醒信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtWarnMsg ktWarnMsg)
    {

        if (ktWarnMsg.getContactType().equalsIgnoreCase(BusiConstant.CONTACT_TYPE_PHONE)) {
            // 短信验证
            String phoneNum = ktWarnMsg.getPhoneNum();
            Matcher m = p.matcher(phoneNum);
            if (!m.matches()) {
                return AjaxResult.error("手机号码格式错误，请输入正确的手机号码！");
            }
        } else if (ktWarnMsg.getContactType().equalsIgnoreCase(BusiConstant.CONTACT_TYPE_EMAIL)) {
            // email验证
            String email = ktWarnMsg.geteMailAddr();
            if (StringUtils.isEmpty(email) || !email.matches("[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+")) {
                return AjaxResult.error("电子邮件格式错误，请输入正确的电子邮件地址！");
            }
        } else {
            // 钉钉验证
            String dingWebHook = ktWarnMsg.getDingWebHook();
            if (StringUtils.isEmpty(dingWebHook)) {
                return AjaxResult.error("请输入钉钉Webhook！");
            }
        }
        return toAjax(ktWarnMsgService.insertKtWarnMsg(ktWarnMsg));
    }

    /**
     * 修改kettle运行提醒信息
     */
    @RequiresPermissions("busi:warnmsg:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        KtWarnMsg ktWarnMsg = ktWarnMsgService.selectKtWarnMsgById(id);
        mmap.put("ktWarnMsg", ktWarnMsg);
        return prefix + "/edit";
    }

    /**
     * 修改保存kettle运行提醒信息
     */
    @RequiresPermissions("busi:warnmsg:edit")
    @Log(title = "kettle运行提醒信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtWarnMsg ktWarnMsg)
    {
        return toAjax(ktWarnMsgService.updateKtWarnMsg(ktWarnMsg));
    }

    /**
     * 删除kettle运行提醒信息
     */
    @RequiresPermissions("busi:warnmsg:remove")
    @Log(title = "kettle运行提醒信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktWarnMsgService.deleteKtWarnMsgByIds(ids));
    }

    /**
     * 发送测试信息
     */
    @RequiresPermissions("busi:warnmsg:test")
    @Log(title = "kettle运行提醒信息", businessType = BusinessType.DELETE)
    @PostMapping( "/sendTestMsg")
    @ResponseBody
    public AjaxResult sendTestMsg(String ids) {
        return toAjax(ktWarnMsgService.sendTestMsg(ids));
    }
}
