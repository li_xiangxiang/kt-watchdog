package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtFileVersion;

/**
 * 文件版本信息Service接口
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
public interface IKtFileVersionService 
{
    /**
     * 查询文件版本信息
     * 
     * @param id 文件版本信息主键
     * @return 文件版本信息
     */
    public KtFileVersion selectKtFileVersionById(Long id);

    /**
     * 查询文件版本信息列表
     * 
     * @param ktFileVersion 文件版本信息
     * @return 文件版本信息集合
     */
    public List<KtFileVersion> selectKtFileVersionList(KtFileVersion ktFileVersion);

    /**
     * 新增文件版本信息
     * 
     * @param ktFileVersion 文件版本信息
     * @return 结果
     */
    public int insertKtFileVersion(KtFileVersion ktFileVersion);

    /**
     * 修改文件版本信息
     * 
     * @param ktFileVersion 文件版本信息
     * @return 结果
     */
    public int updateKtFileVersion(KtFileVersion ktFileVersion);

    /**
     * 批量删除文件版本信息
     * 
     * @param ids 需要删除的文件版本信息主键集合
     * @return 结果
     */
    public int deleteKtFileVersionByIds(String ids);

    /**
     * 删除文件版本信息信息
     * 
     * @param id 文件版本信息主键
     * @return 结果
     */
    public int deleteKtFileVersionById(Long id);

    void deleteByFileId(Long repoId, Long fileId);

    List<KtFileVersion> selectKtFileVersionByRepo(Long repoId);

    List<KtFileVersion> selectKtFileVersionByRepoAndFileId(Long getfRepoId, Long id);

    KtFileVersion selectKtFileVersionByVersion(String enableVersion);

}
