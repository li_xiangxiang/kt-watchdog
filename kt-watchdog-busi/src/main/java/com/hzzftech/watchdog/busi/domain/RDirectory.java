package com.hzzftech.watchdog.busi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 数据库资源库_文件夹对象 R_DIRECTORY
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public class RDirectory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idDirectory;

    /**  */
    @Excel(name = "")
    private Long idDirectoryParent;

    /**  */
    @Excel(name = "")
    private String directoryName;

    public void setIdDirectory(Long idDirectory) 
    {
        this.idDirectory = idDirectory;
    }

    public Long getIdDirectory() 
    {
        return idDirectory;
    }
    public void setIdDirectoryParent(Long idDirectoryParent) 
    {
        this.idDirectoryParent = idDirectoryParent;
    }

    public Long getIdDirectoryParent() 
    {
        return idDirectoryParent;
    }
    public void setDirectoryName(String directoryName) 
    {
        this.directoryName = directoryName;
    }

    public String getDirectoryName() 
    {
        return directoryName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDirectory", getIdDirectory())
            .append("idDirectoryParent", getIdDirectoryParent())
            .append("directoryName", getDirectoryName())
            .toString();
    }
}
