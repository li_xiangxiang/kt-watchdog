package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtParams;
import com.hzzftech.watchdog.busi.domain.KtParamsGroup;
import com.hzzftech.watchdog.common.utils.ShiroUtils;

/**
 * 运行参数Service接口
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
public interface IKtParamsService 
{
    /**
     * 查询运行参数
     * 
     * @param paramsId 运行参数主键
     * @return 运行参数
     */
    public KtParams selectKtParamsByParamsId(Long paramsId);

    /**
     * 查询带密文的运行参数
     * @param paramsId
     * @return
     */
    public KtParams selectKtParamsByParamsIdWithCiphertext(Long paramsId);

    /**
     * 查询运行参数列表
     * 
     * @param ktParams 运行参数
     * @return 运行参数集合
     */
    public List<KtParams> selectKtParamsList(KtParams ktParams);

    public List<KtParams> selectKtParamsEnableListByGroupId(Long repoId);

    /**
     * 新增运行参数
     * 
     * @param ktParams 运行参数
     * @return 结果
     */
    public int insertKtParams(KtParams ktParams);

    /**
     * 修改运行参数
     * 
     * @param ktParams 运行参数
     * @return 结果
     */
    public int updateKtParams(KtParams ktParams);

    /**
     * 批量删除运行参数
     * 
     * @param paramsIds 需要删除的运行参数主键集合
     * @return 结果
     */
    public int deleteKtParamsByParamsIds(String paramsIds);

    /**
     * 删除运行参数信息
     * 
     * @param paramsId 运行参数主键
     * @return 结果
     */
    public int deleteKtParamsByParamsId(Long paramsId);

    public default void addCommHandle(KtParams ktRepository) {
        ktRepository.setCreatedTime(new Date());
        ktRepository.setCreatedBy(ShiroUtils.getSysUser().getUserId());
    }

    public default void updateCommHandle(KtParams ktRepository) {
        ktRepository.setUpdatedTime(new Date());
        ktRepository.setUpdatedBy(ShiroUtils.getSysUser().getUserId());
    }

    int deleteKtParamsByParamsGroupId(long id);
}
