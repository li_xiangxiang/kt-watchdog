package com.hzzftech.watchdog.busi.controller;

import java.util.List;

import com.hzzftech.watchdog.busi.core.dance.JobChangeState;
import com.hzzftech.watchdog.busi.domain.KtDispatcher;
import com.hzzftech.watchdog.busi.domain.KtExecutionLog;
import com.hzzftech.watchdog.busi.service.IKtDispatcherService;
import com.hzzftech.watchdog.common.core.text.Convert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtRepositoryNode;
import com.hzzftech.watchdog.busi.service.IKtRepositoryNodeService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 资源库节点Controller
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
@Controller
@RequestMapping("/watchdog/node")
public class KtRepositoryNodeController extends BaseController
{
    private String prefix = "busi/node";

    @Autowired
    private IKtRepositoryNodeService ktRepositoryNodeService;

    @RequiresPermissions("busi:node:view")
    @GetMapping()
    public String node()
    {
        return prefix + "/node";
    }

    @RequiresPermissions("busi:node:view")
    @GetMapping("/monitor")
    public String nodeMonitor() {
        return "busi/monitor/carteMonitor";
    }

    @RequiresPermissions("busi:node:view")
    @GetMapping("/carteMonitorDetail/{carteId}")
    public String nodeMonitorDetail(@PathVariable("carteId") Long carteId, ModelMap mmap) {

        mmap.addAttribute("carteInfo", ktRepositoryNodeService.selectKtRepositoryNodeByNodeId(carteId));
        KtExecutionLog log = new KtExecutionLog();
        log.setCarteId(carteId.toString());
        mmap.addAttribute("items", ktRepositoryNodeService.selectNodeRunLogByCarteId(log));
        return "busi/monitor/carteMonitorDetail";
    }

    /**
     * 查询资源库节点列表
     */
    @RequiresPermissions("busi:node:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtRepositoryNode ktRepositoryNode)
    {
        startPage();
        List<KtRepositoryNode> list = ktRepositoryNodeService.selectKtRepositoryNodeList(ktRepositoryNode);
        return getDataTable(list);
    }

    /**
     * 查询资源库节点列表
     */
    @RequiresPermissions("busi:node:list")
    @PostMapping("/listMonitor")
    @ResponseBody
    public TableDataInfo listMonitor(KtRepositoryNode ktRepositoryNode)
    {
        startPage();
        List<KtRepositoryNode> list = ktRepositoryNodeService.selectKtRepositoryNodeListMonitor(ktRepositoryNode);
        return getDataTable(list);
    }

    /**
     * 导出资源库节点列表
     */
    @RequiresPermissions("busi:node:export")
    @Log(title = "资源库节点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtRepositoryNode ktRepositoryNode)
    {
        List<KtRepositoryNode> list = ktRepositoryNodeService.selectKtRepositoryNodeList(ktRepositoryNode);
        ExcelUtil<KtRepositoryNode> util = new ExcelUtil<KtRepositoryNode>(KtRepositoryNode.class);
        return util.exportExcel(list, "资源库节点数据");
    }

    /**
     * 新增资源库节点
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存资源库节点
     */
    @RequiresPermissions("busi:node:add")
    @Log(title = "资源库节点", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtRepositoryNode ktRepositoryNode)
    {
        // 触发监控更新
        JobChangeState.instance.jobStateChange();
        return toAjax(ktRepositoryNodeService.insertKtRepositoryNode(ktRepositoryNode));
    }

    /**
     * 修改资源库节点
     */
    @RequiresPermissions("busi:node:edit")
    @GetMapping("/edit/{nodeId}")
    public String edit(@PathVariable("nodeId") Long nodeId, ModelMap mmap)
    {
        KtRepositoryNode ktRepositoryNode = ktRepositoryNodeService.selectKtRepositoryNodeByNodeId(nodeId);
        mmap.put("ktRepositoryNode", ktRepositoryNode);
        return prefix + "/edit";
    }

    /**
     * 修改保存资源库节点
     */
    @RequiresPermissions("busi:node:edit")
    @Log(title = "资源库节点", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtRepositoryNode ktRepositoryNode)
    {
        // 触发监控更新
        JobChangeState.instance.jobStateChange();
        return toAjax(ktRepositoryNodeService.updateKtRepositoryNode(ktRepositoryNode));
    }

    @Autowired
    private IKtDispatcherService dispatcherService;

    /**
     * 删除资源库节点
     */
    @RequiresPermissions("busi:node:remove")
    @Log(title = "资源库节点", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        int count = 0;
        for (String id : Convert.toStrArray(ids)) {
            long _id = Long.parseLong(id);
            KtDispatcher dispatcher = new KtDispatcher();
            dispatcher.setCarteId(_id);
            List<KtDispatcher> list = dispatcherService.selectKtDispatcherList(dispatcher);

            if (!list.isEmpty()) {
                return AjaxResult.error("删除失败，有任务引用了该节点");
            }
            ktRepositoryNodeService.deleteKtRepositoryNodeByNodeId(_id);
            count ++;
        }
        // 触发监控更新
        JobChangeState.instance.jobStateChange();
        return toAjax(count);
    }


    @RequiresPermissions("busi:node:edit")
    @GetMapping("/nodeStatus/{nodeId}")
    @ResponseBody
    public AjaxResult nodeStatus(@PathVariable("nodeId") Long nodeId)
    {
        return ktRepositoryNodeService.checkNodeStatus(nodeId);
    }
}
