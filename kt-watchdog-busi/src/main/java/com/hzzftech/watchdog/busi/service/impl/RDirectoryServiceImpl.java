package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.RDirectoryMapper;
import com.hzzftech.watchdog.busi.domain.RDirectory;
import com.hzzftech.watchdog.busi.service.IRDirectoryService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 数据库资源库_文件夹Service业务层处理
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
@Service
public class RDirectoryServiceImpl implements IRDirectoryService 
{
    @Autowired
    private RDirectoryMapper rDirectoryMapper;

    /**
     * 查询数据库资源库_文件夹
     * 
     * @param idDirectory 数据库资源库_文件夹主键
     * @return 数据库资源库_文件夹
     */
    @Override
    public RDirectory selectRDirectoryByIdDirectory(Long idDirectory)
    {
        return rDirectoryMapper.selectRDirectoryByIdDirectory(idDirectory);
    }

    /**
     * 查询数据库资源库_文件夹列表
     * 
     * @param rDirectory 数据库资源库_文件夹
     * @return 数据库资源库_文件夹
     */
    @Override
    public List<RDirectory> selectRDirectoryList(RDirectory rDirectory)
    {
        return rDirectoryMapper.selectRDirectoryList(rDirectory);
    }

    /**
     * 新增数据库资源库_文件夹
     * 
     * @param rDirectory 数据库资源库_文件夹
     * @return 结果
     */
    @Override
    public int insertRDirectory(RDirectory rDirectory)
    {
        return rDirectoryMapper.insertRDirectory(rDirectory);
    }

    /**
     * 修改数据库资源库_文件夹
     * 
     * @param rDirectory 数据库资源库_文件夹
     * @return 结果
     */
    @Override
    public int updateRDirectory(RDirectory rDirectory)
    {
        return rDirectoryMapper.updateRDirectory(rDirectory);
    }

    /**
     * 批量删除数据库资源库_文件夹
     * 
     * @param idDirectorys 需要删除的数据库资源库_文件夹主键
     * @return 结果
     */
    @Override
    public int deleteRDirectoryByIdDirectorys(String idDirectorys)
    {
        return rDirectoryMapper.deleteRDirectoryByIdDirectorys(Convert.toStrArray(idDirectorys));
    }

    /**
     * 删除数据库资源库_文件夹信息
     * 
     * @param idDirectory 数据库资源库_文件夹主键
     * @return 结果
     */
    @Override
    public int deleteRDirectoryByIdDirectory(Long idDirectory)
    {
        return rDirectoryMapper.deleteRDirectoryByIdDirectory(idDirectory);
    }
}
