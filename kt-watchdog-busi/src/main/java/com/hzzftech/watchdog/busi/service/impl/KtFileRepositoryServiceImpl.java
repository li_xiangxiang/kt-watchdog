package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtFileRepositoryMapper;
import com.hzzftech.watchdog.busi.domain.KtFileRepository;
import com.hzzftech.watchdog.busi.service.IKtFileRepositoryService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 文件资源库Service业务层处理
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
@Service("KtFileRepositoryServiceImpl")
public class KtFileRepositoryServiceImpl implements IKtFileRepositoryService 
{
    @Autowired
    private KtFileRepositoryMapper ktFileRepositoryMapper;

    /**
     * 查询文件资源库
     * 
     * @param id 文件资源库主键
     * @return 文件资源库
     */
    @Override
    public KtFileRepository selectKtFileRepositoryById(Long id)
    {
        return ktFileRepositoryMapper.selectKtFileRepositoryById(id);
    }

    /**
     * 查询文件资源库列表
     * 
     * @param ktFileRepository 文件资源库
     * @return 文件资源库
     */
    @Override
    public List<KtFileRepository> selectKtFileRepositoryList(KtFileRepository ktFileRepository)
    {
        return ktFileRepositoryMapper.selectKtFileRepositoryList(ktFileRepository);
    }

    /**
     * 新增文件资源库
     * 
     * @param ktFileRepository 文件资源库
     * @return 结果
     */
    @Override
    public int insertKtFileRepository(KtFileRepository ktFileRepository)
    {
        return ktFileRepositoryMapper.insertKtFileRepository(ktFileRepository);
    }

    /**
     * 修改文件资源库
     * 
     * @param ktFileRepository 文件资源库
     * @return 结果
     */
    @Override
    public int updateKtFileRepository(KtFileRepository ktFileRepository)
    {
        return ktFileRepositoryMapper.updateKtFileRepository(ktFileRepository);
    }

    /**
     * 批量删除文件资源库
     * 
     * @param ids 需要删除的文件资源库主键
     * @return 结果
     */
    @Override
    public int deleteKtFileRepositoryByIds(String ids)
    {
        return ktFileRepositoryMapper.deleteKtFileRepositoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文件资源库信息
     * 
     * @param id 文件资源库主键
     * @return 结果
     */
    @Override
    public int deleteKtFileRepositoryById(Long id)
    {
        return ktFileRepositoryMapper.deleteKtFileRepositoryById(id);
    }

    @Override
    public List<KtFileRepository> selectAllEnableFileRepository() {
        return ktFileRepositoryMapper.selectAllEnableFileRepository();
    }

    @Override
    public List<KtFileRepository> selectDirectoryUnique(Long id, String directory) {
        return ktFileRepositoryMapper.selectDirectoryUnique(id, directory);
    }
}
