package com.hzzftech.watchdog.busi.service.impl;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.domain.KtParamsGroup;
import com.hzzftech.watchdog.busi.mapper.KtParamsGroupMapper;
import com.hzzftech.watchdog.busi.service.IKtParamsGroupService;
import com.hzzftech.watchdog.common.core.text.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 运行参数组Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
@Service
public class KtParamsGroupServiceImpl implements IKtParamsGroupService 
{
    @Autowired
    private KtParamsGroupMapper ktParamsGroupMapper;

    /**
     * 查询运行参数组
     * 
     * @param paramsGroupId 运行参数组主键
     * @return 运行参数组
     */
    @Override
    public KtParamsGroup selectKtParamsGroupByParamsGroupId(Long paramsGroupId)
    {
        return ktParamsGroupMapper.selectKtParamsGroupByParamsGroupId(paramsGroupId);
    }

    /**
     * 查询运行参数组列表
     * 
     * @param ktParamsGroup 运行参数组
     * @return 运行参数组
     */
    @Override
    public List<KtParamsGroup> selectKtParamsGroupList(KtParamsGroup ktParamsGroup)
    {
        return ktParamsGroupMapper.selectKtParamsGroupList(ktParamsGroup);
    }

    /**
     * 新增运行参数组
     * 
     * @param ktParamsGroup 运行参数组
     * @return 结果
     */
    @Override
    public int insertKtParamsGroup(KtParamsGroup ktParamsGroup)
    {
        addCommHandle(ktParamsGroup);
        return ktParamsGroupMapper.insertKtParamsGroup(ktParamsGroup);
    }

    /**
     * 修改运行参数组
     * 
     * @param ktParamsGroup 运行参数组
     * @return 结果
     */
    @Override
    public int updateKtParamsGroup(KtParamsGroup ktParamsGroup)
    {
        updateCommHandle(ktParamsGroup);
        return ktParamsGroupMapper.updateKtParamsGroup(ktParamsGroup);
    }

    /**
     * 批量删除运行参数组
     * 
     * @param paramsGroupIds 需要删除的运行参数组主键
     * @return 结果
     */
    @Override
    public int deleteKtParamsGroupByParamsGroupIds(String paramsGroupIds)
    {
        return ktParamsGroupMapper.deleteKtParamsGroupByParamsGroupIds(Convert.toStrArray(paramsGroupIds));
    }

    /**
     * 删除运行参数组信息
     * 
     * @param paramsGroupId 运行参数组主键
     * @return 结果
     */
    @Override
    public int deleteKtParamsGroupByParamsGroupId(Long paramsGroupId)
    {
        return ktParamsGroupMapper.deleteKtParamsGroupByParamsGroupId(paramsGroupId);
    }

    @Override
    public String checkDictTypeUnique(KtParamsGroup ktParamsGroup) {
        KtParamsGroup dictType = ktParamsGroupMapper.selectKtParamsGroupByParamsGroupName(ktParamsGroup.getParamsGroupName());
        return Objects.isNull(dictType) ? BusiConstant.PARAM_GROUP_UNIQUE : BusiConstant.PARAM_GROUP_NOT_UNIQUE;
    }

    @Override
    public List<KtParamsGroup> selectAllKtParamsGroupList() {
        return ktParamsGroupMapper.selectAllKtParamsGroupList();
    }

    @Override
    public KtParamsGroup selectKtParamsGroupByName(String paramsGroupName) {
        return ktParamsGroupMapper.selectKtParamsGroupByName(paramsGroupName);
    }
}
