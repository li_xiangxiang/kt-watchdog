package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 文件资源库对象 kt_repository
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
public class KtRepository extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 资源仓库ID */
    private Long repositoryId;

    /** 资源仓库名称 */
    @Excel(name = "资源仓库名称")
    private String repositoryName;

    /** 资源仓库路径 */
    @Excel(name = "资源仓库路径")
    private String repositoryLocation;

    /** 资源仓库描述 */
    @Excel(name = "资源仓库描述")
    private String repositoryDescription;

    /** 资源仓库类型 */
    @Excel(name = "资源仓库类型")
    private String repositoryType;

    /** git权限认证方式 */
    @Excel(name = "git权限认证方式")
    private String gitAuthType;

    /** git地址 */
    @Excel(name = "git地址")
    private String gitUri;

    /** git分支 */
    @Excel(name = "git分支")
    private String gitBranch;

    /** git的SSHkey，用于认证 */
    @Excel(name = "git的SSHkey，用于认证")
    private String gitSshKey;

    /** git用户名 */
    @Excel(name = "git用户名")
    private String gitUsername;

    /** git密码 */
    @Excel(name = "git密码")
    private String gitPassword;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    public void setRepositoryId(Long repositoryId) 
    {
        this.repositoryId = repositoryId;
    }

    public Long getRepositoryId() 
    {
        return repositoryId;
    }
    public void setRepositoryName(String repositoryName) 
    {
        this.repositoryName = repositoryName;
    }

    public String getRepositoryName() 
    {
        return repositoryName;
    }
    public void setRepositoryLocation(String repositoryLocation) 
    {
        this.repositoryLocation = repositoryLocation;
    }

    public String getRepositoryLocation() 
    {
        return repositoryLocation;
    }
    public void setRepositoryDescription(String repositoryDescription) 
    {
        this.repositoryDescription = repositoryDescription;
    }

    public String getRepositoryDescription() 
    {
        return repositoryDescription;
    }
    public void setRepositoryType(String repositoryType) 
    {
        this.repositoryType = repositoryType;
    }

    public String getRepositoryType() 
    {
        return repositoryType;
    }
    public void setGitAuthType(String gitAuthType) 
    {
        this.gitAuthType = gitAuthType;
    }

    public String getGitAuthType() 
    {
        return gitAuthType;
    }
    public void setGitUri(String gitUri) 
    {
        this.gitUri = gitUri;
    }

    public String getGitUri() 
    {
        return gitUri;
    }
    public void setGitSshKey(String gitSshKey) 
    {
        this.gitSshKey = gitSshKey;
    }

    public String getGitSshKey() 
    {
        return gitSshKey;
    }
    public void setGitUsername(String gitUsername) 
    {
        this.gitUsername = gitUsername;
    }

    public String getGitUsername() 
    {
        return gitUsername;
    }
    public void setGitPassword(String gitPassword) 
    {
        this.gitPassword = gitPassword;
    }

    public String getGitPassword() 
    {
        return gitPassword;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getGitBranch() {
        return gitBranch;
    }

    public void setGitBranch(String gitBranch) {
        this.gitBranch = gitBranch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("repositoryId", getRepositoryId())
            .append("repositoryName", getRepositoryName())
            .append("repositoryLocation", getRepositoryLocation())
            .append("repositoryDescription", getRepositoryDescription())
            .append("repositoryType", getRepositoryType())
            .append("gitAuthType", getGitAuthType())
            .append("gitUri", getGitUri())
            .append("gitSshKey", getGitSshKey())
            .append("gitUsername", getGitUsername())
            .append("gitPassword", getGitPassword())
            .append("status", getStatus())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .append("remark", getRemark())
            .toString();
    }
}
