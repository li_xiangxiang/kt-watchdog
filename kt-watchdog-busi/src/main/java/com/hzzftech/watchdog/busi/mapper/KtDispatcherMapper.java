package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import java.util.Map;

import com.hzzftech.watchdog.busi.domain.KtDispatcher;
import com.hzzftech.watchdog.common.annotation.Log;

/**
 * 任务调度Mapper接口
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public interface KtDispatcherMapper 
{
    /**
     * 查询任务调度
     * 
     * @param id 任务调度主键
     * @return 任务调度
     */
    public KtDispatcher selectKtDispatcherByIdG(Long id);
    public KtDispatcher selectKtDispatcherByIdF(Long id);
    public KtDispatcher selectKtDispatcherByIdDB(Long id);

    /**
     * 查询任务调度列表
     * 
     * @param ktDispatcher 任务调度
     * @return 任务调度集合
     */
    public List<KtDispatcher> selectKtDispatcherListG(KtDispatcher ktDispatcher);
    public List<KtDispatcher> selectKtDispatcherListF(KtDispatcher ktDispatcher);
    public List<KtDispatcher> selectKtDispatcherListDB(KtDispatcher ktDispatcher);

    List<KtDispatcher> selectKtDispatcherList(KtDispatcher ktDispatcher);

    /**
     * 新增任务调度
     * 
     * @param ktDispatcher 任务调度
     * @return 结果
     */
    public int insertKtDispatcher(KtDispatcher ktDispatcher);

    /**
     * 修改任务调度
     * 
     * @param ktDispatcher 任务调度
     * @return 结果
     */
    public int updateKtDispatcher(KtDispatcher ktDispatcher);

    /**
     * 删除任务调度
     * 
     * @param id 任务调度主键
     * @return 结果
     */
    public int deleteKtDispatcherById(Long id);

    /**
     * 批量删除任务调度
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtDispatcherByIds(String[] ids);

    List<KtDispatcher> selectKtDispatcherByIdGs(String[] ids);
    List<KtDispatcher> selectKtDispatcherByIdFs(String[] ids);
    List<KtDispatcher> selectKtDispatcherByIdDBs(String[] ids);

    List<KtDispatcher> selectAllEnable();

    KtDispatcher selectKtDispatcherById(Long id);

    List<KtDispatcher> selectKtDispatcherByDpId(Long dpId);

    KtDispatcher selectKtDispatcherByDpName(String dpName);

    Map<String, Object> selectKtDispatcherForMonitor(KtDispatcher dispatcher);
}
