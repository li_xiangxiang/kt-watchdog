package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import java.util.Objects;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.encrypt.DESEncrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtParamsMapper;
import com.hzzftech.watchdog.busi.domain.KtParams;
import com.hzzftech.watchdog.busi.service.IKtParamsService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 运行参数Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
@Service
public class KtParamsServiceImpl implements IKtParamsService 
{
    @Autowired
    private KtParamsMapper ktParamsMapper;

    @Value("${kt-watchdog.DES_SECURITY_KEY}")
    private String DES_SECURITY_KEY;

    public KtParams encryptReplace(KtParams param) {
        if (param.getEncrypt().equals(BusiConstant.DES_ENCRYPT_YES)) {
            param.setParamsValue(BusiConstant.ENCRPT_STR);
        }
        return param;
    }

    /**
     * 查询运行参数
     * 
     * @param paramsId 运行参数主键
     * @return 运行参数
     */
    @Override
    public KtParams selectKtParamsByParamsId(Long paramsId)
    {
        return encryptReplace(ktParamsMapper.selectKtParamsByParamsId(paramsId));
    }

    @Override
    public KtParams selectKtParamsByParamsIdWithCiphertext(Long paramsId) {
        return ktParamsMapper.selectKtParamsByParamsId(paramsId);
    }

    /**
     * 查询运行参数列表
     * 
     * @param ktParams 运行参数
     * @return 运行参数
     */
    @Override
    public List<KtParams> selectKtParamsList(KtParams ktParams)
    {
        List<KtParams> params = ktParamsMapper.selectKtParamsList(ktParams);
        params.forEach(this::encryptReplace);
        return params;
    }

    @Override
    public List<KtParams> selectKtParamsEnableListByGroupId(Long repoId) {
        return ktParamsMapper.selectKtParamsEnableListByGroupId(repoId);
    }

    /**
     * 新增运行参数
     * 
     * @param ktParams 运行参数
     * @return 结果
     */
    @Override
    public int insertKtParams(KtParams ktParams)
    {
        if (ktParams.getEncrypt().equals(BusiConstant.DES_ENCRYPT_YES)) {
            String s = DESEncrypt.encryptNull(ktParams.getParamsValue(), DES_SECURITY_KEY);
            if (Objects.isNull(s)) {
                return 0;
            } else {
                ktParams.setParamsValue(s);
            }
        }
        addCommHandle(ktParams);
        return ktParamsMapper.insertKtParams(ktParams);
    }

    /**
     * 修改运行参数
     * 
     * @param ktParams 运行参数
     * @return 结果
     */
    @Override
    public int updateKtParams(KtParams ktParams)
    {
        if (Objects.isNull(ktParams.getParamsId())) {
            return 0; // 跟新失败
        } else {
            KtParams orgParam = selectKtParamsByParamsId(ktParams.getParamsId());
            if (BusiConstant.DES_ENCRYPT_YES.equals(orgParam.getEncrypt()) && BusiConstant.DES_ENCRYPT_NO.equals(ktParams.getEncrypt())){
                // 试图修改已加密字符串为不加密，危险，不允许
                return 0;
            }
            if (BusiConstant.DES_ENCRYPT_YES.equals(ktParams.getEncrypt())) {
                if (BusiConstant.ENCRPT_STR.equals(ktParams.getParamsValue())) {
                    // 参数值没有变化，设置为原来的值
                    ktParams.setParamsValue(orgParam.getParamsValue());
                } else {
                    ktParams.setParamsValue(DESEncrypt.encryptNull(ktParams.getParamsValue(), DES_SECURITY_KEY));
                }
            }
        }
        updateCommHandle(ktParams);
        return ktParamsMapper.updateKtParams(ktParams);
    }

    /**
     * 批量删除运行参数
     * 
     * @param paramsIds 需要删除的运行参数主键
     * @return 结果
     */
    @Override
    public int deleteKtParamsByParamsIds(String paramsIds)
    {
        return ktParamsMapper.deleteKtParamsByParamsIds(Convert.toStrArray(paramsIds));
    }

    /**
     * 删除运行参数信息
     * 
     * @param paramsId 运行参数主键
     * @return 结果
     */
    @Override
    public int deleteKtParamsByParamsId(Long paramsId)
    {
        return ktParamsMapper.deleteKtParamsByParamsId(paramsId);
    }

    @Override
    public int deleteKtParamsByParamsGroupId(long id) {
        return ktParamsMapper.deleteKtParamsByParamsGroupId(id);
    }
}
