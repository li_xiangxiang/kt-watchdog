package com.hzzftech.watchdog.busi.controller;

import java.util.Date;
import java.util.List;

import com.google.common.eventbus.DeadEvent;
import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.domain.KtDispatcher;
import com.hzzftech.watchdog.busi.domain.KtJob;
import com.hzzftech.watchdog.busi.service.IKtDispatcherService;
import com.hzzftech.watchdog.busi.service.IKtJobService;
import com.hzzftech.watchdog.common.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtDispatcherHangup;
import com.hzzftech.watchdog.busi.service.IKtDispatcherHangupService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 任务挂起Controller
 * 
 * @author kt-watchdog
 * @date 2022-02-21
 */
@Controller
@RequestMapping("/busi/hangup")
public class KtDispatcherHangupController extends BaseController
{
    private String prefix = "busi/hangup";

    @Autowired
    private IKtDispatcherHangupService ktDispatcherHangupService;

    @Autowired
    private IKtJobService jobService;

    @RequiresPermissions("busi:hangup:view")
    @GetMapping("/{jobId}")
    public String hangup(@PathVariable("jobId") Long jobId, ModelMap mmap)
    {
        KtJob job = jobService.selectJobById(jobId);
        mmap.addAttribute("jobId", jobId);
        mmap.addAttribute("jobName", job.getJobName());

        return prefix + "/hangup";
    }

    /**
     * 查询任务挂起列表
     */
    @RequiresPermissions("busi:hangup:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtDispatcherHangup ktDispatcherHangup)
    {
        startPage();
        List<KtDispatcherHangup> list = ktDispatcherHangupService.selectKtDispatcherHangupList(ktDispatcherHangup);
        return getDataTable(list);
    }

    /**
     * 导出任务挂起列表
     */
    @RequiresPermissions("busi:hangup:export")
    @Log(title = "任务挂起", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtDispatcherHangup ktDispatcherHangup)
    {
        List<KtDispatcherHangup> list = ktDispatcherHangupService.selectKtDispatcherHangupList(ktDispatcherHangup);
        ExcelUtil<KtDispatcherHangup> util = new ExcelUtil<KtDispatcherHangup>(KtDispatcherHangup.class);
        return util.exportExcel(list, "任务挂起数据");
    }

    /**
     * 新增任务挂起
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    @GetMapping("/add/{jobId}")
    public String add(@PathVariable("jobId") Long jobId, ModelMap mmap)
    {
        mmap.addAttribute("jobId", jobId);
        KtJob job = jobService.selectJobById(jobId);
        mmap.addAttribute("jobName", job.getJobName());
        return prefix + "/add";
    }

    /**
     * 新增保存任务挂起
     */
    @RequiresPermissions("busi:hangup:add")
    @Log(title = "任务挂起", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtDispatcherHangup ktDispatcherHangup)
    {
        ktDispatcherHangup.setCreatedBy(ShiroUtils.getUserId());
        ktDispatcherHangup.setCreatedTime(new Date());
        ktDispatcherHangup.setHangStatus(BusiConstant.NOT_HANG_UP);
        if (ktDispatcherHangup.getBeginTime().before(new Date())) {
            return AjaxResult.error("挂起开始时间不能在当前时间之前");
        }
        if (ktDispatcherHangup.getBeginTime().after(ktDispatcherHangup.getEndTime())) {
            return AjaxResult.error("挂起开始时间不能在挂起结束时间之后");
        }

        ktDispatcherHangupService.insertKtDispatcherHangup(ktDispatcherHangup);
        int exeStatus = ktDispatcherHangupService.hangUpJob(ktDispatcherHangup);
        if (exeStatus == -1) {
            ktDispatcherHangupService.deleteKtDispatcherHangupById(ktDispatcherHangup.getJobId());
            return AjaxResult.error("挂起任务失败");
        }

        return AjaxResult.success();
    }

    /**
     * 修改任务挂起
     */
    @RequiresPermissions("busi:hangup:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        KtDispatcherHangup ktDispatcherHangup = ktDispatcherHangupService.selectKtDispatcherHangupById(id);
        mmap.put("ktDispatcherHangup", ktDispatcherHangup);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务挂起
     */
    @RequiresPermissions("busi:hangup:edit")
    @Log(title = "任务挂起", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtDispatcherHangup ktDispatcherHangup)
    {
        return toAjax(ktDispatcherHangupService.updateKtDispatcherHangup(ktDispatcherHangup));
    }

    /**
     * 删除任务挂起
     */
    @RequiresPermissions("busi:hangup:remove")
    @Log(title = "任务挂起", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktDispatcherHangupService.deleteKtDispatcherHangupByIds(ids));
    }
}
