package com.hzzftech.watchdog.busi.controller.jobversion;

import com.hzzftech.watchdog.busi.core.dance.JobChangeState;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobVersionController {
    @GetMapping("getVersion")
    public Long getVersion() {
        return JobChangeState.instance.getjobStateVersion();
    }

}
