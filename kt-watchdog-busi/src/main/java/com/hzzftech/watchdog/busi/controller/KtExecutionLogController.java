package com.hzzftech.watchdog.busi.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtDispatcherSteps;
import com.hzzftech.watchdog.busi.service.IKtDispatcherStepsService;
import com.hzzftech.watchdog.busi.service.impl.KtDispatcherStepsServiceImpl;
import org.apache.commons.codec.binary.Base64;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtExecutionLog;
import com.hzzftech.watchdog.busi.service.IKtExecutionLogService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 任务调度日志Controller
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
@Controller
@RequestMapping("/busi/executionLog")
public class KtExecutionLogController extends BaseController
{
    private String prefix = "busi/executionLog";

    @Autowired
    private IKtExecutionLogService ktExecutionLogService;

    @RequiresPermissions("busi:executionLog:view")
    @GetMapping("/{taskId}")
    public String executionLog(@PathVariable("taskId") Long taskId, ModelMap mmap)
    {
        mmap.addAttribute("taskId", taskId);
        return prefix + "/executionLog";
    }

    @RequiresPermissions("busi:executionLog:view")
    @GetMapping("/carteMonitorDetail/{slaveId}")
    public String executionLogCarte(@PathVariable("slaveId") Long slaveId, ModelMap mmap)
    {
        mmap.addAttribute("slaveId", slaveId);
        return  "busi/monitor/carteMonitorDetail";
    }

    @RequiresPermissions("busi:executionLog:view")
    @GetMapping("/taskMonitorDetail/{taskId}")
    public String executionLogTask(@PathVariable("taskId") Long taskId, ModelMap mmap)
    {
        mmap.addAttribute("taskId", taskId);
        return  "busi/monitor/dispatcherMonitorDetail";
    }

    @RequiresPermissions("busi:executionLog:view")
    @GetMapping()
    public String executionLogAll(ModelMap mmap)
    {
        mmap.addAttribute("taskId", -1);
        return prefix + "/executionLog";
    }


    /**
     * 查询任务调度日志列表
     */
    @RequiresPermissions("busi:executionLog:list")
    @PostMapping("/list/{taskId}")
    @ResponseBody
    public TableDataInfo list2(@PathVariable("taskId") Long taskId, KtExecutionLog ktExecutionLog)
    {
        startPage();
        ktExecutionLog.setTaskId(taskId);
        List<KtExecutionLog> list = ktExecutionLogService.selectKtExecutionLogList(ktExecutionLog);
        return getDataTable(list);
    }

    /**
     * 查询任务调度日志列表
     */
    @RequiresPermissions("busi:executionLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtExecutionLog ktExecutionLog)
    {
        startPage();
        List<KtExecutionLog> list = ktExecutionLogService.selectKtExecutionLogList(ktExecutionLog);
        return getDataTable(list);
    }

    /**
     * 导出任务调度日志列表
     */
    @RequiresPermissions("busi:executionLog:export")
    @Log(title = "任务调度日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtExecutionLog ktExecutionLog)
    {
        List<KtExecutionLog> list = ktExecutionLogService.selectKtExecutionLogList(ktExecutionLog);
        ExcelUtil<KtExecutionLog> util = new ExcelUtil<KtExecutionLog>(KtExecutionLog.class);
        return util.exportExcel(list, "任务调度日志数据");
    }

    /**
     * 新增任务调度日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务调度日志
     */
    @RequiresPermissions("busi:executionLog:add")
    @Log(title = "任务调度日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtExecutionLog ktExecutionLog)
    {
        return toAjax(ktExecutionLogService.insertKtExecutionLog(ktExecutionLog));
    }

    /**
     * 修改任务调度日志
     */
    @RequiresPermissions("busi:executionLog:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        KtExecutionLog ktExecutionLog = ktExecutionLogService.selectKtExecutionLogById(id);
        mmap.put("ktExecutionLog", ktExecutionLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务调度日志
     */
    @RequiresPermissions("busi:executionLog:edit")
    @Log(title = "任务调度日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtExecutionLog ktExecutionLog)
    {
        return toAjax(ktExecutionLogService.updateKtExecutionLog(ktExecutionLog));
    }

    /**
     * 删除任务调度日志
     */
    @RequiresPermissions("busi:executionLog:remove")
    @Log(title = "任务调度日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktExecutionLogService.deleteKtExecutionLogByIds(ids));
    }

    @Autowired
    private IKtDispatcherStepsService ktDispatcherStepsService;

    @GetMapping("/detail/{logId}")
    public String logDetail(@PathVariable("logId") String logId, ModelMap mmap) {
        KtExecutionLog log = ktExecutionLogService.selectKtExecutionLogById(logId);
        List<KtDispatcherSteps> steps = ktDispatcherStepsService.selectKtDispatcherStepsByTaskId(logId);
        // 日志详细信息
        mmap.addAttribute("log", log);
        // 执行步骤
        mmap.addAttribute("steps", steps);
        // 执行图片
        mmap.addAttribute("imgBase64", getImgBase64(log));
        return prefix+"/detail";
    }

    @GetMapping("/ajax/detail/{logId}")
    @ResponseBody
    public AjaxResult logDetailAjax(@PathVariable("logId") String logId) {
        KtExecutionLog log = ktExecutionLogService.selectKtExecutionLogById(logId);
        return AjaxResult.success(log);
    }

    public String getImgBase64(KtExecutionLog log) {
        String logFPath = log.getImagePath();
        File f = new File(logFPath);
        if (!f.exists()) {
            return "";
        }

        try {
            FileInputStream in = new FileInputStream(f);
            long size = f.length();
            byte[] buffer = new byte[(int)size];
            in.read(buffer, 0, (int)size);
            in.close();
            return Base64.encodeBase64String(buffer);
        } catch (Exception e) {
            logger.error("读写文件错误",e);
        }
        return "";
    }
}
