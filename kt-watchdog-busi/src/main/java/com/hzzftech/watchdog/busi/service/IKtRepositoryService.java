package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtRepository;
import com.hzzftech.watchdog.common.core.domain.entity.SysUser;
import com.hzzftech.watchdog.common.utils.ShiroUtils;

/**
 * 文件资源库Service接口
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
public interface IKtRepositoryService 
{
    /**
     * 查询文件资源库
     * 
     * @param repositoryId 文件资源库主键
     * @return 文件资源库
     */
    public KtRepository selectKtRepositoryByRepositoryId(Long repositoryId);

    /**
     * 查询文件资源库列表
     * 
     * @param ktRepository 文件资源库
     * @return 文件资源库集合
     */
    public List<KtRepository> selectKtRepositoryList(KtRepository ktRepository);

    /**
     * 新增文件资源库
     * 
     * @param ktRepository 文件资源库
     * @return 结果
     */
    public int insertKtRepository(KtRepository ktRepository);

    /**
     * 修改文件资源库
     * 
     * @param ktRepository 文件资源库
     * @return 结果
     */
    public int updateKtRepository(KtRepository ktRepository);

    /**
     * 批量删除文件资源库
     * 
     * @param repositoryIds 需要删除的文件资源库主键集合
     * @return 结果
     */
    public int deleteKtRepositoryByRepositoryIds(String repositoryIds);

    /**
     * 删除文件资源库信息
     * 
     * @param repositoryId 文件资源库主键
     * @return 结果
     */
    public int deleteKtRepositoryByRepositoryId(Long repositoryId);

    public default void addCommHandle(KtRepository ktRepository) {
        ktRepository.setCreatedTime(new Date());
        ktRepository.setCreatedBy(ShiroUtils.getSysUser().getUserId());
    }

    public default void updateCommHandle(KtRepository ktRepository) {
        ktRepository.setUpdatedTime(new Date());
        ktRepository.setUpdatedBy(ShiroUtils.getSysUser().getUserId());
    }

    boolean updateGitResources(Long repositoryId);

    boolean forceUpdateGitResources(Long repositoryId);

    List<String> findAllBranch(Long repositoryId);

    boolean checkRespNameOnly(String repositoryName);

    boolean checkRespLocationOnly(String repositoryLocation);

    void deleteResource(String repositoryLocation);
}
