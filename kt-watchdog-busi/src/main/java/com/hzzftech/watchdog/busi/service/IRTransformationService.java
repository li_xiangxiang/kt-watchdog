package com.hzzftech.watchdog.busi.service;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.RTransformation;

/**
 * 数据库资源库_transService接口
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public interface IRTransformationService 
{
    /**
     * 查询数据库资源库_trans
     * 
     * @param idTransformation 数据库资源库_trans主键
     * @return 数据库资源库_trans
     */
    public RTransformation selectRTransformationByIdTransformation(Long idTransformation);

    /**
     * 查询数据库资源库_trans列表
     *
     * @param rTransformation 数据库资源库_trans
     * @return 数据库资源库_trans集合
     */
    public Integer selectRTransformationCount(RTransformation rTransformation);
    /**
     * 查询数据库资源库_trans列表
     * 
     * @param rTransformation 数据库资源库_trans
     * @return 数据库资源库_trans集合
     */
    public List<RTransformation> selectRTransformationList(RTransformation rTransformation,  Integer pgNum, Integer pgSize, String orderBy);

    /**
     * 新增数据库资源库_trans
     * 
     * @param rTransformation 数据库资源库_trans
     * @return 结果
     */
    public int insertRTransformation(RTransformation rTransformation);

    /**
     * 修改数据库资源库_trans
     * 
     * @param rTransformation 数据库资源库_trans
     * @return 结果
     */
    public int updateRTransformation(RTransformation rTransformation);

    /**
     * 批量删除数据库资源库_trans
     * 
     * @param idTransformations 需要删除的数据库资源库_trans主键集合
     * @return 结果
     */
    public int deleteRTransformationByIdTransformations(String idTransformations);

    /**
     * 删除数据库资源库_trans信息
     * 
     * @param idTransformation 数据库资源库_trans主键
     * @return 结果
     */
    public int deleteRTransformationByIdTransformation(Long idTransformation);
}
