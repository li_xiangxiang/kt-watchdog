package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 任务调度对象 kt_dispatcher
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public class KtDispatcher extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    private Long dpId;

    public Long getDpId() {
        return dpId;
    }

    public void setDpId(Long dpId) {
        this.dpId = dpId;
    }

    /** 调度名称 */
    @Excel(name = "调度名称")
    private String dpName;

    /** 调度类型 */
    @Excel(name = "调度类型")
    private String dpType;

    private String dpTypeName;

    /** 调度的仓库类型 （0GIT仓库 1文件资源仓库） */
    @Excel(name = "调度的仓库类型 （0GIT仓库 1文件资源仓库 2数据库资源库）")
    private String dpRepoType;

    /** 调度地址 */
    @Excel(name = "调度地址")
    private String dpPath;

    /** 仓库地址 */
    @Excel(name = "仓库地址")
    private Long repositoryId;

    /** carte服务ID */
    @Excel(name = "carte服务ID")
    private Long carteId;

    /** 参数组ID */
    @Excel(name = "参数组ID")
    private Long paramsGroupId;

    private String warnMsgIds;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long createdBy;

    private String createdName;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updatedBy;

    private String updatedName;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    // 启用/停用
    private String status;

    private String repoName;
    private String repoLocal;
    private String carteName;
    private String carteUri;
    private String paramsGroupName;

    private String dpIds;

    private String currentStatus;

    private Long exeCount;

    private Long exeSuccessCount;

    private Long exeFailedCount;

    private int warnLevel;

    public int getWarnLevel() {
        return warnLevel;
    }

    public void setWarnLevel(int warnLevel) {
        this.warnLevel = warnLevel;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Long getExeCount() {
        return exeCount;
    }

    public void setExeCount(Long exeCount) {
        this.exeCount = exeCount;
    }

    public Long getExeSuccessCount() {
        return exeSuccessCount;
    }

    public void setExeSuccessCount(Long exeSuccessCount) {
        this.exeSuccessCount = exeSuccessCount;
    }

    public Long getExeFailedCount() {
        return exeFailedCount;
    }

    public void setExeFailedCount(Long exeFailedCount) {
        this.exeFailedCount = exeFailedCount;
    }


    public String getDpIds() {
        return dpIds;
    }

    public void setDpIds(String dpIds) {
        this.dpIds = dpIds;
    }

    public String getWarnMsgIds() {
        return warnMsgIds;
    }

    public void setWarnMsgIds(String warnMsgIds) {
        this.warnMsgIds = warnMsgIds;
    }

    public String getParamsGroupName() {
        return paramsGroupName;
    }

    public void setParamsGroupName(String paramsGroupName) {
        this.paramsGroupName = paramsGroupName;
    }

    public String getDpTypeName() {
        return dpTypeName;
    }

    public void setDpTypeName(String dpTypeName) {
        this.dpTypeName = dpTypeName;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public String getDpRepoType() {
        return dpRepoType;
    }

    public void setDpRepoType(String dpRepoType) {
        this.dpRepoType = dpRepoType;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoLocal() {
        return repoLocal;
    }

    public void setRepoLocal(String repoLocal) {
        this.repoLocal = repoLocal;
    }

    public Long getCarteId() {
        return carteId;
    }

    public void setCarteId(Long carteId) {
        this.carteId = carteId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDpName(String dpName) 
    {
        this.dpName = dpName;
    }

    public String getDpName() 
    {
        return dpName;
    }
    public void setDpType(String dpType) 
    {
        this.dpType = dpType;
    }

    public String getDpType() 
    {
        return dpType;
    }
    public void setDpPath(String dpPath) 
    {
        this.dpPath = dpPath;
    }

    public String getDpPath() 
    {
        return dpPath;
    }
    public void setRepositoryId(Long repositoryId) 
    {
        this.repositoryId = repositoryId;
    }

    public Long getRepositoryId() 
    {
        return repositoryId;
    }
    public void setParamsGroupId(Long paramsGroupId) 
    {
        this.paramsGroupId = paramsGroupId;
    }

    public Long getParamsGroupId() 
    {
        return paramsGroupId;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    public String getCarteName() {
        return carteName;
    }

    public void setCarteName(String carteName) {
        this.carteName = carteName;
    }

    public String getCarteUri() {
        return carteUri;
    }

    public void setCarteUri(String carteUri) {
        this.carteUri = carteUri;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dpName", getDpName())
            .append("dpType", getDpType())
            .append("dpPath", getDpPath())
            .append("repositoryId", getRepositoryId())
            .append("paramsGroupId", getParamsGroupId())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
