package com.hzzftech.watchdog.busi.service.impl;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.domain.KtWarnMsg;
import com.hzzftech.watchdog.busi.mapper.KtWarnMsgMapper;
import com.hzzftech.watchdog.busi.module.dingding.service.DingService;
import com.hzzftech.watchdog.busi.module.email.service.IEmailService;
import com.hzzftech.watchdog.busi.service.IKtWarnMsgService;
import com.hzzftech.watchdog.common.core.text.Convert;
import com.hzzftech.watchdog.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * kettle运行提醒信息Service业务层处理
 * 
 * @author kt-watchdog
 * @date 2022-01-03
 */
@Service
public class KtWarnMsgServiceImpl implements IKtWarnMsgService 
{
    @Autowired
    private KtWarnMsgMapper ktWarnMsgMapper;

    /**
     * 查询kettle运行提醒信息
     * 
     * @param id kettle运行提醒信息主键
     * @return kettle运行提醒信息
     */
    @Override
    public KtWarnMsg selectKtWarnMsgById(Long id)
    {
        return ktWarnMsgMapper.selectKtWarnMsgById(id);
    }

    /**
     * 查询kettle运行提醒信息列表
     * 
     * @param ktWarnMsg kettle运行提醒信息
     * @return kettle运行提醒信息
     */
    @Override
    public List<KtWarnMsg> selectKtWarnMsgList(KtWarnMsg ktWarnMsg)
    {
        return ktWarnMsgMapper.selectKtWarnMsgList(ktWarnMsg);
    }

    /**
     * 新增kettle运行提醒信息
     * 
     * @param ktWarnMsg kettle运行提醒信息
     * @return 结果
     */
    @Override
    public int insertKtWarnMsg(KtWarnMsg ktWarnMsg)
    {
        ktWarnMsg.setCreateTime(DateUtils.getNowDate());
        return ktWarnMsgMapper.insertKtWarnMsg(ktWarnMsg);
    }

    /**
     * 修改kettle运行提醒信息
     * 
     * @param ktWarnMsg kettle运行提醒信息
     * @return 结果
     */
    @Override
    public int updateKtWarnMsg(KtWarnMsg ktWarnMsg)
    {
        ktWarnMsg.setUpdateTime(DateUtils.getNowDate());
        return ktWarnMsgMapper.updateKtWarnMsg(ktWarnMsg);
    }

    /**
     * 批量删除kettle运行提醒信息
     * 
     * @param ids 需要删除的kettle运行提醒信息主键
     * @return 结果
     */
    @Override
    public int deleteKtWarnMsgByIds(String ids)
    {
        return ktWarnMsgMapper.deleteKtWarnMsgByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除kettle运行提醒信息信息
     * 
     * @param id kettle运行提醒信息主键
     * @return 结果
     */
    @Override
    public int deleteKtWarnMsgById(Long id)
    {
        return ktWarnMsgMapper.deleteKtWarnMsgById(id);
    }

    @Autowired
    private IEmailService emailService;

    @Autowired
    private DingService dingService;

    @Value("${kt-watchdog.DING_TALK_WEB_HOOK}")
    private String DING_TALK_WEB_HOOK;

    @Override
    public int sendTestMsg(String ids) {
        String[] strings = Convert.toStrArray(ids);
        int count = 0;
        for (String id : strings) {
            KtWarnMsg ktWarnMsg = ktWarnMsgMapper.selectKtWarnMsgById(Long.parseLong(id));
            if (BusiConstant.CONTACT_TYPE_PHONE.equalsIgnoreCase(ktWarnMsg.getContactType())) {
                // TODO 发送短信
                count++;
            } else if (BusiConstant.CONTACT_TYPE_EMAIL.equalsIgnoreCase(ktWarnMsg.getContactType())) {
                emailService.sendAttachMail(ktWarnMsg.geteMailAddr(), "watchdog测试", "测试邮件，请忽略！");
                count++;
            } else if (BusiConstant.CONTACT_TYPE_DINGDING.equalsIgnoreCase(ktWarnMsg.getContactType())) {
                dingService.sendDingTalk("watchdog测试\n\n测试消息，请忽略！");
                count++;
            }
        }
        return count;
    }
}
