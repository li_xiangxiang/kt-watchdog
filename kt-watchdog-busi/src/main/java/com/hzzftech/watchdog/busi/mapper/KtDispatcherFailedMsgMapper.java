package com.hzzftech.watchdog.busi.mapper;

import com.hzzftech.watchdog.busi.domain.KtDispatcherFailedMsg;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author kt-watchdog
 * @date 2022-03-06
 */
public interface KtDispatcherFailedMsgMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public KtDispatcherFailedMsg selectKtDispatcherFailedMsgById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<KtDispatcherFailedMsg> selectKtDispatcherFailedMsgList(KtDispatcherFailedMsg ktDispatcherFailedMsg);

    /**
     * 新增【请填写功能名称】
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 结果
     */
    public int insertKtDispatcherFailedMsg(KtDispatcherFailedMsg ktDispatcherFailedMsg);

    /**
     * 修改【请填写功能名称】
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 结果
     */
    public int updateKtDispatcherFailedMsg(KtDispatcherFailedMsg ktDispatcherFailedMsg);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteKtDispatcherFailedMsgById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtDispatcherFailedMsgByIds(String[] ids);
}
