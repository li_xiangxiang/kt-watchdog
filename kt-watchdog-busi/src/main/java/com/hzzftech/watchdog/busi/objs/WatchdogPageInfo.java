package com.hzzftech.watchdog.busi.objs;

/**
 * 分页信息
 */
public class WatchdogPageInfo {

    private Integer pgNum;
    private Integer pgSize;
    private String orderBy;

    public WatchdogPageInfo() {
    }

    public WatchdogPageInfo(Integer pgNum, Integer pgSize, String orderBy) {
        this.pgNum = pgNum;
        this.pgSize = pgSize;
        this.orderBy = orderBy;
    }

    public Integer getPgNum() {
        return pgNum;
    }

    public void setPgNum(Integer pgNum) {
        this.pgNum = pgNum;
    }

    public Integer getPgSize() {
        return pgSize;
    }

    public void setPgSize(Integer pgSize) {
        this.pgSize = pgSize;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
