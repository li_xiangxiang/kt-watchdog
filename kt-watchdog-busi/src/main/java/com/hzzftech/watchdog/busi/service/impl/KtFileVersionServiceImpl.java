package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtFileVersionMapper;
import com.hzzftech.watchdog.busi.domain.KtFileVersion;
import com.hzzftech.watchdog.busi.service.IKtFileVersionService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 文件版本信息Service业务层处理
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
@Service
public class KtFileVersionServiceImpl implements IKtFileVersionService 
{
    @Autowired
    private KtFileVersionMapper ktFileVersionMapper;

    /**
     * 查询文件版本信息
     * 
     * @param id 文件版本信息主键
     * @return 文件版本信息
     */
    @Override
    public KtFileVersion selectKtFileVersionById(Long id)
    {
        return ktFileVersionMapper.selectKtFileVersionById(id);
    }

    /**
     * 查询文件版本信息列表
     * 
     * @param ktFileVersion 文件版本信息
     * @return 文件版本信息
     */
    @Override
    public List<KtFileVersion> selectKtFileVersionList(KtFileVersion ktFileVersion)
    {
        return ktFileVersionMapper.selectKtFileVersionList(ktFileVersion);
    }

    /**
     * 新增文件版本信息
     * 
     * @param ktFileVersion 文件版本信息
     * @return 结果
     */
    @Override
    public int insertKtFileVersion(KtFileVersion ktFileVersion)
    {
        return ktFileVersionMapper.insertKtFileVersion(ktFileVersion);
    }

    /**
     * 修改文件版本信息
     * 
     * @param ktFileVersion 文件版本信息
     * @return 结果
     */
    @Override
    public int updateKtFileVersion(KtFileVersion ktFileVersion)
    {
        return ktFileVersionMapper.updateKtFileVersion(ktFileVersion);
    }

    /**
     * 批量删除文件版本信息
     * 
     * @param ids 需要删除的文件版本信息主键
     * @return 结果
     */
    @Override
    public int deleteKtFileVersionByIds(String ids)
    {
        int count = 0;
        String[] strings = Convert.toStrArray(ids);
        for (String v:
             strings) {
            long l = Long.parseLong(v);
            KtFileVersion ktFileVersion = ktFileVersionMapper.selectKtFileVersionById(l);
            if (ktFileVersion.get_enable().equals(BusiConstant.FILE_VERSION_ENABLE_NO)) {
                count += ktFileVersionMapper.deleteKtFileVersionById(l);
            }
        }
        return count;
    }

    /**
     * 删除文件版本信息信息
     * 
     * @param id 文件版本信息主键
     * @return 结果
     */
    @Override
    public int deleteKtFileVersionById(Long id)
    {
        return ktFileVersionMapper.deleteKtFileVersionById(id);
    }

    @Override
    public void deleteByFileId(Long repoId, Long fileId) {
        ktFileVersionMapper.deleteByFileId(repoId, fileId);
    }

    @Override
    public List<KtFileVersion> selectKtFileVersionByRepo(Long repoId) {
        return ktFileVersionMapper.selectKtFileVersionByRepo(repoId);
    }

    @Override
    public List<KtFileVersion> selectKtFileVersionByRepoAndFileId(Long repoId, Long fileId) {
        return ktFileVersionMapper.selectKtFileVersionByRepoAndFileId(repoId, fileId);
    }

    @Override
    public KtFileVersion selectKtFileVersionByVersion(String enableVersion) {
        return ktFileVersionMapper.selectKtFileVersionByVersion(enableVersion);
    }
}
