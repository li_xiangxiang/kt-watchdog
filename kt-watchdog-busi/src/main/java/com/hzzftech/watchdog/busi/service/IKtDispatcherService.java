package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.domain.KtDispatcher;
import com.hzzftech.watchdog.common.utils.ShiroUtils;
import org.pentaho.di.core.exception.KettleMissingPluginsException;
import org.pentaho.di.core.exception.KettleXMLException;

/**
 * 任务调度Service接口
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public interface IKtDispatcherService 
{
    /**
     * 查询任务调度
     * 
     * @param id 任务调度主键
     * @return 任务调度
     */
    public KtDispatcher selectKtDispatcherById(Long id, String repoType);

    /**
     * 查询任务调度
     *
     * @param id 任务调度主键
     * @return 任务调度
     */
    public KtDispatcher selectKtDispatcherById(Long id);

    /**
     * 查询任务调度列表
     * 
     * @param ktDispatcher 任务调度
     * @return 任务调度集合
     */
    public List<KtDispatcher> selectKtDispatcherList(KtDispatcher ktDispatcher, String repoType);

    public List<KtDispatcher> selectKtDispatcherList(KtDispatcher ktDispatcher);

    /**
     * 新增任务调度
     * 
     * @param ktDispatcher 任务调度
     * @return 结果
     */
    public int insertKtDispatcher(KtDispatcher ktDispatcher);

    /**
     * 修改任务调度
     * 
     * @param ktDispatcher 任务调度
     * @return 结果
     */
    public int updateKtDispatcher(KtDispatcher ktDispatcher);

    /**
     * 批量删除任务调度
     * 
     * @param ids 需要删除的任务调度主键集合
     * @return 结果
     */
    public int deleteKtDispatcherByIds(String ids);

    /**
     * 删除任务调度信息
     * 
     * @param id 任务调度主键
     * @return 结果
     */
    public int deleteKtDispatcherById(Long id);

    default void addProcess(KtDispatcher ktDispatcher) {
        ktDispatcher.setCreatedBy(ShiroUtils.getUserId());
        ktDispatcher.setCreatedTime(new Date());
        ktDispatcher.setStatus(BusiConstant.STATUS_YES);
    }

    default void updateProcess(KtDispatcher ktDispatcher) {
        ktDispatcher.setUpdatedBy(ShiroUtils.getUserId());
        ktDispatcher.setUpdatedTime(new Date());
    }

    int launchGJob(String ids) throws KettleMissingPluginsException, KettleXMLException;

    int launchFJob(String ids, Long jobId) throws KettleMissingPluginsException, KettleXMLException;

    int launchDBJob(String ids) throws KettleMissingPluginsException, KettleXMLException;

    List<KtDispatcher> selectAllEnable();

    KtDispatcher selectKtDispatcherByDpName(String dpName);

    KtDispatcher selectKtDispatcherForMonitor(KtDispatcher dispatcher);
}
