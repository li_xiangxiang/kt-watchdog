package com.hzzftech.watchdog.busi.mapper;

import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtFileRepository;
import com.hzzftech.watchdog.busi.domain.KtFileRepositoryMsg;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * 文件资源库-文件信息Mapper接口
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
public interface KtFileRepositoryMsgMapper 
{
    /**
     * 查询文件资源库-文件信息
     * 
     * @param id 文件资源库-文件信息主键
     * @return 文件资源库-文件信息
     */
    public KtFileRepositoryMsg selectKtFileRepositoryMsgById(Long id);

    /**
     * 查询文件资源库-文件信息列表
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 文件资源库-文件信息集合
     */
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgList(KtFileRepositoryMsg ktFileRepositoryMsg);
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgList2(KtFileRepositoryMsg ktFileRepositoryMsg);

    /**
     * 新增文件资源库-文件信息
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 结果
     */
    public int insertKtFileRepositoryMsg(KtFileRepositoryMsg ktFileRepositoryMsg);

    /**
     * 修改文件资源库-文件信息
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 结果
     */
    public int updateKtFileRepositoryMsg(KtFileRepositoryMsg ktFileRepositoryMsg);

    /**
     * 删除文件资源库-文件信息
     * 
     * @param id 文件资源库-文件信息主键
     * @return 结果
     */
    public int deleteKtFileRepositoryMsgById(Long id);

    /**
     * 批量删除文件资源库-文件信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtFileRepositoryMsgByIds(String[] ids);

    List<KtFileRepositoryMsg> selectFileRepositoryByParentId(Long repositoryId);

    List<KtFileRepositoryMsg> selectListByRepoId(Long repoId);

    List<KtFileRepositoryMsg> selectKtFileRepositoryMsgWithChild(@Param("fRepoId") Long fRepoId,@Param("id") Long id);

    List<KtFileRepositoryMsg> selectListByRepoIds(String[] ancestors);

    int deleteKtFileRepositoryMsgAndChildById(Long id);

    KtFileRepositoryMsg selectKtFileRepositoryMsgByRepo(@Param("fRepoId") Long repoId);

    KtFileRepositoryMsg selectRepoById(@Param("repoId") Long repoId,@Param("parentId") Long parentId);

    KtFileRepositoryMsg selectRepositoryFileByRepoIdAndFid(@Param("repoId") Long repoId,@Param("id") Long id);

    List<KtFileRepositoryMsg> selectKtRepositoryByRepoId(@Param("repoId") Long repoId);

    int deleteKtFileRepositoryMsgByRepoId(long repoId);
}
