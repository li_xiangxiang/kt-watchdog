package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 资源库节点对象 kt_repository_node
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
public class KtRepositoryNode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private Long nodeId;

    /** 仓库ID */
    @Excel(name = "仓库ID")
    private Long repositoryId;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String name;

    /** 节点IP地址 */
    @Excel(name = "节点IP地址")
    private String nodeHost;

    /** 节点端口 */
    @Excel(name = "节点端口")
    private String nodePort;

    /** 节点用户名称 */
    @Excel(name = "节点用户名称")
    private String nodeUsername;

    /** 节点密码 */
    @Excel(name = "节点密码")
    private String nodePassword;

    /** 节点描述 */
    @Excel(name = "节点描述")
    private String nodeDescription;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    private String createdUser;

    private String updatedUser;

    // 运行次数
    private int runCount;

    // 运行成功次数
    private int runSuccessCount;

    // 运行失败次数
    private int runFailedCount;

    // 正在运行任务数量
    private int runningCount;

    public int getRunCount() {
        return runCount;
    }

    public void setRunCount(int runCount) {
        this.runCount = runCount;
    }

    public int getRunSuccessCount() {
        return runSuccessCount;
    }

    public void setRunSuccessCount(int runSuccessCount) {
        this.runSuccessCount = runSuccessCount;
    }

    public int getRunFailedCount() {
        return runFailedCount;
    }

    public void setRunFailedCount(int runFailedCount) {
        this.runFailedCount = runFailedCount;
    }

    public int getRunningCount() {
        return runningCount;
    }

    public void setRunningCount(int runningCount) {
        this.runningCount = runningCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public void setNodeId(Long nodeId)
    {
        this.nodeId = nodeId;
    }

    public Long getNodeId() 
    {
        return nodeId;
    }
    public void setRepositoryId(Long repositoryId) 
    {
        this.repositoryId = repositoryId;
    }

    public Long getRepositoryId() 
    {
        return repositoryId;
    }
    public void setNodeHost(String nodeHost) 
    {
        this.nodeHost = nodeHost;
    }

    public String getNodeHost() 
    {
        return nodeHost;
    }
    public void setNodePort(String nodePort) 
    {
        this.nodePort = nodePort;
    }

    public String getNodePort() 
    {
        return nodePort;
    }
    public void setNodeUsername(String nodeUsername) 
    {
        this.nodeUsername = nodeUsername;
    }

    public String getNodeUsername() 
    {
        return nodeUsername;
    }
    public void setNodePassword(String nodePassword) 
    {
        this.nodePassword = nodePassword;
    }

    public String getNodePassword() 
    {
        return nodePassword;
    }
    public void setNodeDescription(String nodeDescription) 
    {
        this.nodeDescription = nodeDescription;
    }

    public String getNodeDescription() 
    {
        return nodeDescription;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("nodeId", getNodeId())
            .append("repositoryId", getRepositoryId())
            .append("nodeHost", getNodeHost())
            .append("nodePort", getNodePort())
            .append("nodeUsername", getNodeUsername())
            .append("nodePassword", getNodePassword())
            .append("nodeDescription", getNodeDescription())
            .append("status", getStatus())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .append("remark", getRemark())
            .toString();
    }
}
