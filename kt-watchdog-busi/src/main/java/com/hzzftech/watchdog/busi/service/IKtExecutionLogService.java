package com.hzzftech.watchdog.busi.service;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtExecutionLog;

/**
 * 任务调度日志Service接口
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
public interface IKtExecutionLogService 
{
    /**
     * 查询任务调度日志
     * 
     * @param id 任务调度日志主键
     * @return 任务调度日志
     */
    public KtExecutionLog selectKtExecutionLogById(String id);

    /**
     * 查询任务调度日志列表
     * 
     * @param ktExecutionLog 任务调度日志
     * @return 任务调度日志集合
     */
    public List<KtExecutionLog> selectKtExecutionLogList(KtExecutionLog ktExecutionLog);

    /**
     * 新增任务调度日志
     * 
     * @param ktExecutionLog 任务调度日志
     * @return 结果
     */
    public int insertKtExecutionLog(KtExecutionLog ktExecutionLog);

    /**
     * 修改任务调度日志
     * 
     * @param ktExecutionLog 任务调度日志
     * @return 结果
     */
    public int updateKtExecutionLog(KtExecutionLog ktExecutionLog);

    /**
     * 批量删除任务调度日志
     * 
     * @param ids 需要删除的任务调度日志主键集合
     * @return 结果
     */
    public int deleteKtExecutionLogByIds(String ids);

    /**
     * 删除任务调度日志信息
     * 
     * @param id 任务调度日志主键
     * @return 结果
     */
    public int deleteKtExecutionLogById(Long id);
}
