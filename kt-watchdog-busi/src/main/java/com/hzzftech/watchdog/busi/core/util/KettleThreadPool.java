package com.hzzftech.watchdog.busi.core.util;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class KettleThreadPool {
    private static ExecutorService service = Executors.newFixedThreadPool(6);
    private static KettleThreadPool pool = new KettleThreadPool();

    public static KettleThreadPool getInstance() {
        return pool;
    }

    public <T> Future<T> run(Callable<T> call) {
        return service.submit(call);
    }

    public Future<?> run(Runnable runnable) {
        return service.submit(runnable);
    }
}
