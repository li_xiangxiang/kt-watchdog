package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtDispatcherFailedMsg;
import com.hzzftech.watchdog.busi.mapper.KtDispatcherFailedMsgMapper;
import com.hzzftech.watchdog.busi.service.IKtDispatcherFailedMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author kt-watchdog
 * @date 2022-03-06
 */
@Service
public class KtDispatcherFailedMsgServiceImpl implements IKtDispatcherFailedMsgService
{
    @Autowired
    private KtDispatcherFailedMsgMapper ktDispatcherFailedMsgMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public KtDispatcherFailedMsg selectKtDispatcherFailedMsgById(Long id)
    {
        return ktDispatcherFailedMsgMapper.selectKtDispatcherFailedMsgById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<KtDispatcherFailedMsg> selectKtDispatcherFailedMsgList(KtDispatcherFailedMsg ktDispatcherFailedMsg)
    {
        return ktDispatcherFailedMsgMapper.selectKtDispatcherFailedMsgList(ktDispatcherFailedMsg);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertKtDispatcherFailedMsg(KtDispatcherFailedMsg ktDispatcherFailedMsg)
    {
        return ktDispatcherFailedMsgMapper.insertKtDispatcherFailedMsg(ktDispatcherFailedMsg);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param ktDispatcherFailedMsg 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateKtDispatcherFailedMsg(KtDispatcherFailedMsg ktDispatcherFailedMsg)
    {
        return ktDispatcherFailedMsgMapper.updateKtDispatcherFailedMsg(ktDispatcherFailedMsg);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteKtDispatcherFailedMsgByIds(String ids)
    {
        return ktDispatcherFailedMsgMapper.deleteKtDispatcherFailedMsgByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteKtDispatcherFailedMsgById(Long id)
    {
        return ktDispatcherFailedMsgMapper.deleteKtDispatcherFailedMsgById(id);
    }
}
