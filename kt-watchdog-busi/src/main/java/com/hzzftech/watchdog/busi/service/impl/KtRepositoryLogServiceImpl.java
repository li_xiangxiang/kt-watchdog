package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtRepositoryLogMapper;
import com.hzzftech.watchdog.busi.domain.KtRepositoryLog;
import com.hzzftech.watchdog.busi.service.IKtRepositoryLogService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 文件资源库操作日志Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-11-24
 */
@Service
public class KtRepositoryLogServiceImpl implements IKtRepositoryLogService 
{
    @Autowired
    private KtRepositoryLogMapper ktRepositoryLogMapper;

    /**
     * 查询文件资源库操作日志
     * 
     * @param id 文件资源库操作日志主键
     * @return 文件资源库操作日志
     */
    @Override
    public KtRepositoryLog selectKtRepositoryLogById(Long id)
    {
        return ktRepositoryLogMapper.selectKtRepositoryLogById(id);
    }

    /**
     * 查询文件资源库操作日志列表
     * 
     * @param ktRepositoryLog 文件资源库操作日志
     * @return 文件资源库操作日志
     */
    @Override
    public List<KtRepositoryLog> selectKtRepositoryLogList(KtRepositoryLog ktRepositoryLog)
    {
        return ktRepositoryLogMapper.selectKtRepositoryLogList(ktRepositoryLog);
    }

    /**
     * 新增文件资源库操作日志
     * 
     * @param ktRepositoryLog 文件资源库操作日志
     * @return 结果
     */
    @Override
    public int insertKtRepositoryLog(KtRepositoryLog ktRepositoryLog)
    {
        return ktRepositoryLogMapper.insertKtRepositoryLog(ktRepositoryLog);
    }

    /**
     * 修改文件资源库操作日志
     * 
     * @param ktRepositoryLog 文件资源库操作日志
     * @return 结果
     */
    @Override
    public int updateKtRepositoryLog(KtRepositoryLog ktRepositoryLog)
    {
        return ktRepositoryLogMapper.updateKtRepositoryLog(ktRepositoryLog);
    }

    /**
     * 批量删除文件资源库操作日志
     * 
     * @param ids 需要删除的文件资源库操作日志主键
     * @return 结果
     */
    @Override
    public int deleteKtRepositoryLogByIds(String ids)
    {
        return ktRepositoryLogMapper.deleteKtRepositoryLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文件资源库操作日志信息
     * 
     * @param id 文件资源库操作日志主键
     * @return 结果
     */
    @Override
    public int deleteKtRepositoryLogById(Long id)
    {
        return ktRepositoryLogMapper.deleteKtRepositoryLogById(id);
    }
}
