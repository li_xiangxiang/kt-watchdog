package com.hzzftech.watchdog.busi.mapper;

import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtExecutionLog;
import com.hzzftech.watchdog.busi.domain.KtRepositoryNode;
import com.hzzftech.watchdog.system.domain.Item;

/**
 * 资源库节点Mapper接口
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
public interface KtRepositoryNodeMapper 
{
    /**
     * 查询资源库节点
     * 
     * @param nodeId 资源库节点主键
     * @return 资源库节点
     */
    public KtRepositoryNode selectKtRepositoryNodeByNodeId(Long nodeId);

    /**
     * 查询资源库节点列表
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 资源库节点集合
     */
    public List<KtRepositoryNode> selectKtRepositoryNodeList(KtRepositoryNode ktRepositoryNode);

    /**
     * 新增资源库节点
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 结果
     */
    public int insertKtRepositoryNode(KtRepositoryNode ktRepositoryNode);

    /**
     * 修改资源库节点
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 结果
     */
    public int updateKtRepositoryNode(KtRepositoryNode ktRepositoryNode);

    /**
     * 删除资源库节点
     * 
     * @param nodeId 资源库节点主键
     * @return 结果
     */
    public int deleteKtRepositoryNodeByNodeId(Long nodeId);

    /**
     * 批量删除资源库节点
     * 
     * @param nodeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtRepositoryNodeByNodeIds(String[] nodeIds);

    List<KtRepositoryNode> selectKtRepositoryNodeListMonitor(KtRepositoryNode ktRepositoryNode);

    List<Item> selectNodeRunLogByCarteId(KtExecutionLog log);
}
