package com.hzzftech.watchdog.busi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KettleConfig {
    // git 资源库根路径
    @Value("${kt-watchdog.git_file_repository_location}")
    private String GIT_FILE_REPOSITORY_LOCATION;
    // git 资源库日志位置
    @Value("${kt-watchdog.git_file_repository_log_file}")
    private String GIT_FILE_REPOSITORY_LOG_FILE;
    // 文件资源库根路径
    @Value("${kt-watchdog.file_repository_location}")
    private String FILE_REPOSITORY_LOCATION;
    // 文件资源库日志文件路径
    @Value("${kt-watchdog.file_repository_log_file}")
    private String FILE_REPOSITORY_LOG_FILE;

    // 数据资源库地址
    @Value("${kt-watchdog.ip}")
    private String ip;

    // 端口
    @Value("${kt-watchdog.port}")
    private String port;

    // 数据资源库名
    @Value("${kt-watchdog.dbName}")
    private String dbName;

    // 资源库版本
    @Value("${kt-watchdog.dbVersion}")
    private String dbVersion;

    // 驱动类
    @Value("${kt-watchdog.driverClassName}")
    private String driverClassName;

    // 数据资源库用户名
    @Value("${kt-watchdog.userName}")
    private String userName;

    // 数据资源库密码
    @Value("${kt-watchdog.password}")
    private String password;

    // 资源库的名称，默认ip+数据库名称（如：127.0.0.1_kettle_db）
    @Value("${kt-watchdog.repoName}")
    private String repoName;
    // 资源库连接用户名
    @Value("${kt-watchdog.repoUserName}")
    private String repoUserName;
    // 资源库连接密码
    @Value("${kt-watchdog.repoPassword}")
    private String repoPassword;

    public String getGIT_FILE_REPOSITORY_LOCATION() {
        return GIT_FILE_REPOSITORY_LOCATION;
    }

    public void setGIT_FILE_REPOSITORY_LOCATION(String GIT_FILE_REPOSITORY_LOCATION) {
        this.GIT_FILE_REPOSITORY_LOCATION = GIT_FILE_REPOSITORY_LOCATION;
    }

    public String getGIT_FILE_REPOSITORY_LOG_FILE() {
        return GIT_FILE_REPOSITORY_LOG_FILE;
    }

    public void setGIT_FILE_REPOSITORY_LOG_FILE(String GIT_FILE_REPOSITORY_LOG_FILE) {
        this.GIT_FILE_REPOSITORY_LOG_FILE = GIT_FILE_REPOSITORY_LOG_FILE;
    }

    public String getFILE_REPOSITORY_LOCATION() {
        return FILE_REPOSITORY_LOCATION;
    }

    public void setFILE_REPOSITORY_LOCATION(String FILE_REPOSITORY_LOCATION) {
        this.FILE_REPOSITORY_LOCATION = FILE_REPOSITORY_LOCATION;
    }

    public String getFILE_REPOSITORY_LOG_FILE() {
        return FILE_REPOSITORY_LOG_FILE;
    }

    public void setFILE_REPOSITORY_LOG_FILE(String FILE_REPOSITORY_LOG_FILE) {
        this.FILE_REPOSITORY_LOG_FILE = FILE_REPOSITORY_LOG_FILE;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbVersion() {
        return dbVersion;
    }

    public void setDbVersion(String dbVersion) {
        this.dbVersion = dbVersion;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoUserName() {
        return repoUserName;
    }

    public void setRepoUserName(String repoUserName) {
        this.repoUserName = repoUserName;
    }

    public String getRepoPassword() {
        return repoPassword;
    }

    public void setRepoPassword(String repoPassword) {
        this.repoPassword = repoPassword;
    }
}
