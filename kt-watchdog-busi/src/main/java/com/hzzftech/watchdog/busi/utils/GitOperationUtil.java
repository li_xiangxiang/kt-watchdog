package com.hzzftech.watchdog.busi.utils;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;

import java.util.List;

public class GitOperationUtil {
    public static boolean branchNameExists(Git git, String branchName) throws GitAPIException {
        List<Ref> refs = git.branchList().call();
        for (Ref f : refs) {
            if (f.getName().contains(branchName)) {
                return true;
            }
        }
        return false;
    }

    public static void checkOut(Git git, String branchName) throws GitAPIException {
        if (branchNameExists(git, branchName)) {
            git.checkout().setCreateBranch(false).setName(branchName).call();
        } else {
            git.checkout().setCreateBranch(true).setName(branchName).setStartPoint("origin/"+branchName).call();
        }
    }
}
