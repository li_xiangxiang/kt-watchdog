package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtParamsGroup;
import com.hzzftech.watchdog.busi.domain.KtRepository;
import com.hzzftech.watchdog.common.utils.ShiroUtils;

/**
 * 运行参数组Service接口
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
public interface IKtParamsGroupService 
{
    /**
     * 查询运行参数组
     * 
     * @param paramsGroupId 运行参数组主键
     * @return 运行参数组
     */
    public KtParamsGroup selectKtParamsGroupByParamsGroupId(Long paramsGroupId);

    /**
     * 查询运行参数组列表
     * 
     * @param ktParamsGroup 运行参数组
     * @return 运行参数组集合
     */
    public List<KtParamsGroup> selectKtParamsGroupList(KtParamsGroup ktParamsGroup);

    /**
     * 新增运行参数组
     * 
     * @param ktParamsGroup 运行参数组
     * @return 结果
     */
    public int insertKtParamsGroup(KtParamsGroup ktParamsGroup);

    /**
     * 修改运行参数组
     * 
     * @param ktParamsGroup 运行参数组
     * @return 结果
     */
    public int updateKtParamsGroup(KtParamsGroup ktParamsGroup);

    /**
     * 批量删除运行参数组
     * 
     * @param paramsGroupIds 需要删除的运行参数组主键集合
     * @return 结果
     */
    public int deleteKtParamsGroupByParamsGroupIds(String paramsGroupIds);

    /**
     * 删除运行参数组信息
     * 
     * @param paramsGroupId 运行参数组主键
     * @return 结果
     */
    public int deleteKtParamsGroupByParamsGroupId(Long paramsGroupId);

    String checkDictTypeUnique(KtParamsGroup ktParamsGroup);

    public default void addCommHandle(KtParamsGroup ktRepository) {
        ktRepository.setCreatedTime(new Date());
        ktRepository.setCreatedBy(ShiroUtils.getSysUser().getUserId());
    }

    public default void updateCommHandle(KtParamsGroup ktRepository) {
        ktRepository.setUpdatedTime(new Date());
        ktRepository.setUpdatedBy(ShiroUtils.getSysUser().getUserId());
    }

    List<KtParamsGroup> selectAllKtParamsGroupList();

    KtParamsGroup selectKtParamsGroupByName(String paramsGroupName);
}
