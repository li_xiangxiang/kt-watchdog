package com.hzzftech.watchdog.busi.service.impl;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.core.file.repository.FileRepositoryManager;
import com.hzzftech.watchdog.busi.core.file.repository.KtFileRepositoryFileParser;
import com.hzzftech.watchdog.busi.domain.KtFileRepository;
import com.hzzftech.watchdog.busi.domain.KtFileRepositoryMsg;
import com.hzzftech.watchdog.busi.domain.KtFileVersion;
import com.hzzftech.watchdog.busi.mapper.KtFileRepositoryMsgMapper;
import com.hzzftech.watchdog.busi.service.IKtFileRepositoryMsgService;
import com.hzzftech.watchdog.busi.service.IKtFileRepositoryService;
import com.hzzftech.watchdog.busi.service.IKtFileVersionService;
import com.hzzftech.watchdog.busi.utils.FileUtils;
import com.hzzftech.watchdog.common.core.domain.Ztree;
import com.hzzftech.watchdog.common.core.text.Convert;
import com.hzzftech.watchdog.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 文件资源库-文件信息Service业务层处理
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
@Service
public class KtFileRepositoryMsgServiceImpl implements IKtFileRepositoryMsgService 
{
    public static Logger logger = LoggerFactory.getLogger(KtFileRepositoryMsgServiceImpl.class);

    @Autowired
    private KtFileRepositoryMsgMapper ktFileRepositoryMsgMapper;

    /**
     * 查询文件资源库-文件信息
     * 
     * @param id 文件资源库-文件信息主键
     * @return 文件资源库-文件信息
     */
    @Override
    public KtFileRepositoryMsg selectKtFileRepositoryMsgById(Long id)
    {
        return ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgById(id);
    }

    /**
     * 查询文件资源库-文件信息列表
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 文件资源库-文件信息
     */
    @Override
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgList(KtFileRepositoryMsg ktFileRepositoryMsg)
    {
        return ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgList(ktFileRepositoryMsg);
    }

    @Override
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgList2(KtFileRepositoryMsg ktFileRepositoryMsg)
    {
        return ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgList2(ktFileRepositoryMsg);
    }

    /**
     * 新增文件资源库-文件信息
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 结果
     */
    @Override
    public int insertKtFileRepositoryMsg(KtFileRepositoryMsg ktFileRepositoryMsg)
    {
        return ktFileRepositoryMsgMapper.insertKtFileRepositoryMsg(ktFileRepositoryMsg);
    }

    /**
     * 修改文件资源库-文件信息
     * 
     * @param ktFileRepositoryMsg 文件资源库-文件信息
     * @return 结果
     */
    @Override
    public int updateKtFileRepositoryMsg(KtFileRepositoryMsg ktFileRepositoryMsg)
    {
        return ktFileRepositoryMsgMapper.updateKtFileRepositoryMsg(ktFileRepositoryMsg);
    }

    /**
     * 批量删除文件资源库-文件信息
     * 
     * @param ids 需要删除的文件资源库-文件信息主键
     * @return 结果
     */
    @Override
    public int deleteKtFileRepositoryMsgByIds(String ids)
    {
        return ktFileRepositoryMsgMapper.deleteKtFileRepositoryMsgByIds(Convert.toStrArray(ids));
    }


    @Autowired
    private IKtFileRepositoryMsgService ktFileRepositoryMsgService;


    @Autowired
    private IKtFileVersionService versionService;

    @Autowired
    private IKtFileRepositoryService fileRepositoryService;

    @Value("${kt-watchdog.profile}")
    private String PROFIX_STR;

    @Value("${kt-watchdog.file_repository_location}")
    private String FILE_REPOSITORY_LOCATION;

    /**
     * 删除文件资源库-文件信息信息
     * 
     * @param id 文件资源库-文件信息主键
     * @return 结果
     */
    @Override
    public int deleteKtFileRepositoryMsgById(Long id)
    {
        KtFileRepositoryMsg msg = ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgById(id);

        if (Objects.isNull(msg)) {
            return 0;
        }

        /**
         * 删除文件版本信息
         */
        List<KtFileRepositoryMsg> ktFileRepositoryMsgs = ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgWithChild(msg.getfRepoId(), msg.getId());
        for (KtFileRepositoryMsg m:
                ktFileRepositoryMsgs) {
            List<KtFileVersion> versions = versionService.selectKtFileVersionByRepoAndFileId(m.getfRepoId(), m.getId());
            for (KtFileVersion v : versions) {
                String fpath = FILE_REPOSITORY_LOCATION+File.separator+"version_"+m.getFileName()+File.separator+v.getVersion();
                FileUtils.delete(fpath);
            }
            versionService.deleteByFileId(m.getfRepoId(), m.getId());
        }
        /**
         * 删除文件信息和目录下的信息
         */
        KtFileRepository ktFileRepository = fileRepositoryService.selectKtFileRepositoryById(msg.getfRepoId());
        KtFileRepositoryFileParser ktFileRepositoryFileParser = new KtFileRepositoryFileParser(ktFileRepositoryMsgService, versionService, ktFileRepository.getId(), FILE_REPOSITORY_LOCATION);
        String filePath = ktFileRepositoryFileParser.getFilePath(msg);
        FileUtils.delete(FILE_REPOSITORY_LOCATION + filePath);
        return ktFileRepositoryMsgMapper.deleteKtFileRepositoryMsgAndChildById(id);

    }

    @Override
    public List<KtFileRepositoryMsg> selectFileRepositoryByParentId(Long repositoryId) {
        return ktFileRepositoryMsgMapper.selectFileRepositoryByParentId(repositoryId);
    }

    @Override
    public List<Ztree> selectFileTree(Long repoId) {
        List<KtFileRepositoryMsg> ktFileRepositoryMsgs = ktFileRepositoryMsgMapper.selectListByRepoId(repoId);
        return initZtree(ktFileRepositoryMsgs);
    }

    // 初始话树结构
    public List<Ztree> initZtree(List<KtFileRepositoryMsg> ktFileRepositoryMsgs) {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(ktFileRepositoryMsgs);
        for (KtFileRepositoryMsg msg : ktFileRepositoryMsgs)
        {
            if (BusiConstant.STATUS_YES.equals(msg.getStatus()))
            {
                Ztree ztree = new Ztree();
                ztree.setId(msg.getId());
                ztree.setpId(msg.getParentId());
                ztree.setName(msg.getFileName());
                ztree.setTitle(msg.getFileName());
                if (isCheck)
                {
                    ztree.setChecked(true);
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }

    @Override
    public List<KtFileRepositoryMsg> selectListByRepoId(Long repoId) {
        return ktFileRepositoryMsgMapper.selectListByRepoId(repoId);
    }

    @Override
    public List<KtFileRepositoryMsg> selectKtFileRepositoryMsgWithChild(Long getfRepoId, Long id) {
        return ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgWithChild(getfRepoId, id);
    }

    @Override
    public List<KtFileRepositoryMsg> selectListByRepoIds(String[] ancestors) {
        return ktFileRepositoryMsgMapper.selectListByRepoIds(ancestors);
    }

    @Override
    public KtFileRepositoryMsg selectKtFileRepositoryMsgByRepo(Long repoId) {
        return ktFileRepositoryMsgMapper.selectKtFileRepositoryMsgByRepo(repoId);
    }

    @Override
    public KtFileRepositoryMsg selectRepoById(Long repoId, Long parentId) {
        return ktFileRepositoryMsgMapper.selectRepoById(repoId, parentId);
    }

    @Override
    public KtFileRepositoryMsg selectRepositoryFileByRepoIdAndFid(Long repoId, Long id) {
        return ktFileRepositoryMsgMapper.selectRepositoryFileByRepoIdAndFid(repoId, id);
    }

    @Override
    public List<KtFileRepositoryMsg> selectKtRepositoryByRepoId(Long repoId) {
        return ktFileRepositoryMsgMapper.selectKtRepositoryByRepoId(repoId);
    }

    @Override
    public int deleteKtFileRepositoryMsgByRepoId(long repoId) {
        return ktFileRepositoryMsgMapper.deleteKtFileRepositoryMsgByRepoId(repoId);
    }
}
