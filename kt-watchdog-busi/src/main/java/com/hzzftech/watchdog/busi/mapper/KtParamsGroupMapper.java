package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtParamsGroup;

/**
 * 运行参数组Mapper接口
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
public interface KtParamsGroupMapper 
{
    /**
     * 查询运行参数组
     * 
     * @param paramsGroupId 运行参数组主键
     * @return 运行参数组
     */
    public KtParamsGroup selectKtParamsGroupByParamsGroupId(Long paramsGroupId);
    public KtParamsGroup selectKtParamsGroupByParamsGroupName(String paramsGroupName);

    /**
     * 查询运行参数组列表
     * 
     * @param ktParamsGroup 运行参数组
     * @return 运行参数组集合
     */
    public List<KtParamsGroup> selectKtParamsGroupList(KtParamsGroup ktParamsGroup);

    /**
     * 新增运行参数组
     * 
     * @param ktParamsGroup 运行参数组
     * @return 结果
     */
    public int insertKtParamsGroup(KtParamsGroup ktParamsGroup);

    /**
     * 修改运行参数组
     * 
     * @param ktParamsGroup 运行参数组
     * @return 结果
     */
    public int updateKtParamsGroup(KtParamsGroup ktParamsGroup);

    /**
     * 删除运行参数组
     * 
     * @param paramsGroupId 运行参数组主键
     * @return 结果
     */
    public int deleteKtParamsGroupByParamsGroupId(Long paramsGroupId);

    /**
     * 批量删除运行参数组
     * 
     * @param paramsGroupIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtParamsGroupByParamsGroupIds(String[] paramsGroupIds);

    List<KtParamsGroup> selectAllKtParamsGroupList();

    KtParamsGroup selectKtParamsGroupByName(String paramsGroupName);
}
