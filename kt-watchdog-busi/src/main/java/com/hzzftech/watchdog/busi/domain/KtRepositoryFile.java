    package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 仓库文件对象 kt_repository_file
 * 
 * @author liquanxiang
 * @date 2021-12-04
 */
public class KtRepositoryFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    public Long getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
        this.repositoryId = repositoryId;
    }

    /**
     * 文件所属仓库ID
     */
    private Long repositoryId;

    /** 上级文件夹ID */
    @Excel(name = "上级文件夹ID")
    private Long parentId;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String fName;

    /** 文件类型，后缀名 */
    @Excel(name = "文件类型，后缀名")
    private String fType;

    /** 类型 0文件夹 1文件 */
    @Excel(name = "类型 0文件夹 1文件")
    private String type;

    /** 文件创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "文件创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fCreatedTime;

    /** 磁盘大小 */
    @Excel(name = "磁盘大小")
    private Long size;

    /** git仓库有效版本 */
    @Excel(name = "git仓库有效版本")
    private String gitEnableVersion;

    /** 是否启用 0启用 1禁用 */
    @Excel(name = "是否启用 0启用 1禁用")
    private String status;

    /** 是否被删除 0正常 1删除 */
    private String delFlag;

    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delDate;

    /** 仓库名称 */
    @Excel(name = "删除时间", width = 30)
    private String repositoryName;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setfName(String fName) 
    {
        this.fName = fName;
    }

    public String getfName() 
    {
        return fName;
    }
    public void setfType(String fType) 
    {
        this.fType = fType;
    }

    public String getfType() 
    {
        return fType;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setfCreatedTime(Date fCreatedTime) 
    {
        this.fCreatedTime = fCreatedTime;
    }

    public Date getfCreatedTime() 
    {
        return fCreatedTime;
    }
    public void setSize(Long size) 
    {
        this.size = size;
    }

    public Long getSize() 
    {
        return size;
    }
    public void setGitEnableVersion(String gitEnableVersion) 
    {
        this.gitEnableVersion = gitEnableVersion;
    }

    public String getGitEnableVersion() 
    {
        return gitEnableVersion;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setDelDate(Date delDate) 
    {
        this.delDate = delDate;
    }

    public Date getDelDate() 
    {
        return delDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("fName", getfName())
            .append("fType", getfType())
            .append("type", getType())
            .append("fCreatedTime", getfCreatedTime())
            .append("size", getSize())
            .append("gitEnableVersion", getGitEnableVersion())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("delDate", getDelDate())
            .toString();
    }

    public KtRepositoryFile(){super();}

    public KtRepositoryFile(Long repositoryId, Long parentId, String fName, String fType, String type, Long size) {
        this.repositoryId = repositoryId;
        this.parentId = parentId;
        this.fName = fName;
        this.fType = fType;
        this.type = type;
        this.size = size;
    }
}
