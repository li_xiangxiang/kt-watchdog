package com.hzzftech.watchdog.busi.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtRepositoryLog;
import com.hzzftech.watchdog.busi.service.IKtRepositoryLogService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 文件资源库操作日志Controller
 * 
 * @author liquanxiang
 * @date 2021-11-24
 */
@Controller
@RequestMapping("/watchdog/repository-log")
public class KtRepositoryLogController extends BaseController
{
    private String prefix = "busi/repository-log";

    @Autowired
    private IKtRepositoryLogService ktRepositoryLogService;

    @RequiresPermissions("watchdog:repository_log:view")
    @GetMapping()
    public String repository_log()
    {
        return prefix + "/repository_log";
    }

    /**
     * 查询文件资源库操作日志列表
     */
    @RequiresPermissions("watchdog:repository_log:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtRepositoryLog ktRepositoryLog)
    {
        startPage();
        List<KtRepositoryLog> list = ktRepositoryLogService.selectKtRepositoryLogList(ktRepositoryLog);
        return getDataTable(list);
    }

    /**
     * 导出文件资源库操作日志列表
     */
    @RequiresPermissions("watchdog:repository_log:export")
    @Log(title = "文件资源库操作日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtRepositoryLog ktRepositoryLog)
    {
        List<KtRepositoryLog> list = ktRepositoryLogService.selectKtRepositoryLogList(ktRepositoryLog);
        ExcelUtil<KtRepositoryLog> util = new ExcelUtil<KtRepositoryLog>(KtRepositoryLog.class);
        return util.exportExcel(list, "文件资源库操作日志数据");
    }

    /**
     * 新增文件资源库操作日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存文件资源库操作日志
     */
    @RequiresPermissions("watchdog:repository_log:add")
    @Log(title = "文件资源库操作日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtRepositoryLog ktRepositoryLog)
    {
        return toAjax(ktRepositoryLogService.insertKtRepositoryLog(ktRepositoryLog));
    }

    /**
     * 修改文件资源库操作日志
     */
    @RequiresPermissions("watchdog:repository_log:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        KtRepositoryLog ktRepositoryLog = ktRepositoryLogService.selectKtRepositoryLogById(id);
        mmap.put("ktRepositoryLog", ktRepositoryLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存文件资源库操作日志
     */
    @RequiresPermissions("watchdog:repository_log:edit")
    @Log(title = "文件资源库操作日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtRepositoryLog ktRepositoryLog)
    {
        return toAjax(ktRepositoryLogService.updateKtRepositoryLog(ktRepositoryLog));
    }

    /**
     * 删除文件资源库操作日志
     */
    @RequiresPermissions("watchdog:repository_log:remove")
    @Log(title = "文件资源库操作日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktRepositoryLogService.deleteKtRepositoryLogByIds(ids));
    }

}
