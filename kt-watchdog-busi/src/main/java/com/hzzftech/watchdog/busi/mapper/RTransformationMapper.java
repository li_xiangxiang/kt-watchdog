package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.RTransformation;

/**
 * 数据库资源库_transMapper接口
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public interface RTransformationMapper 
{
    /**
     * 查询数据库资源库_trans
     * 
     * @param idTransformation 数据库资源库_trans主键
     * @return 数据库资源库_trans
     */
    public RTransformation selectRTransformationByIdTransformation(Long idTransformation);

    /**
     * 查询数据库资源库_trans列表
     * 
     * @param rTransformation 数据库资源库_trans
     * @return 数据库资源库_trans集合
     */
    public List<RTransformation> selectRTransformationList(RTransformation rTransformation);

    /**
     * 新增数据库资源库_trans
     * 
     * @param rTransformation 数据库资源库_trans
     * @return 结果
     */
    public int insertRTransformation(RTransformation rTransformation);

    /**
     * 修改数据库资源库_trans
     * 
     * @param rTransformation 数据库资源库_trans
     * @return 结果
     */
    public int updateRTransformation(RTransformation rTransformation);

    /**
     * 删除数据库资源库_trans
     * 
     * @param idTransformation 数据库资源库_trans主键
     * @return 结果
     */
    public int deleteRTransformationByIdTransformation(Long idTransformation);

    /**
     * 批量删除数据库资源库_trans
     * 
     * @param idTransformations 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRTransformationByIdTransformations(String[] idTransformations);
}
