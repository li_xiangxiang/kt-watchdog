package com.hzzftech.watchdog.busi.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtDispatcherSteps;
import com.hzzftech.watchdog.busi.service.IKtDispatcherStepsService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 任务调度执行步骤Controller
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
@Controller
@RequestMapping("/busi/dispatchSteps")
public class KtDispatcherStepsController extends BaseController
{
    private String prefix = "busi/dispatchSteps";

    @Autowired
    private IKtDispatcherStepsService ktDispatcherStepsService;

    @RequiresPermissions("busi:dispatchSteps:view")
    @GetMapping()
    public String dispatchSteps()
    {
        return prefix + "/dispatchSteps";
    }

    /**
     * 查询任务调度执行步骤列表
     */
    @RequiresPermissions("busi:dispatchSteps:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtDispatcherSteps ktDispatcherSteps)
    {
        startPage();
        List<KtDispatcherSteps> list = ktDispatcherStepsService.selectKtDispatcherStepsList(ktDispatcherSteps);
        return getDataTable(list);
    }

    /**
     * 导出任务调度执行步骤列表
     */
    @RequiresPermissions("busi:dispatchSteps:export")
    @Log(title = "任务调度执行步骤", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtDispatcherSteps ktDispatcherSteps)
    {
        List<KtDispatcherSteps> list = ktDispatcherStepsService.selectKtDispatcherStepsList(ktDispatcherSteps);
        ExcelUtil<KtDispatcherSteps> util = new ExcelUtil<KtDispatcherSteps>(KtDispatcherSteps.class);
        return util.exportExcel(list, "任务调度执行步骤数据");
    }

    /**
     * 新增任务调度执行步骤
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务调度执行步骤
     */
    @RequiresPermissions("busi:dispatchSteps:add")
    @Log(title = "任务调度执行步骤", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtDispatcherSteps ktDispatcherSteps)
    {
        return toAjax(ktDispatcherStepsService.insertKtDispatcherSteps(ktDispatcherSteps));
    }

    /**
     * 修改任务调度执行步骤
     */
    @RequiresPermissions("busi:dispatchSteps:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        KtDispatcherSteps ktDispatcherSteps = ktDispatcherStepsService.selectKtDispatcherStepsById(id);
        mmap.put("ktDispatcherSteps", ktDispatcherSteps);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务调度执行步骤
     */
    @RequiresPermissions("busi:dispatchSteps:edit")
    @Log(title = "任务调度执行步骤", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtDispatcherSteps ktDispatcherSteps)
    {
        return toAjax(ktDispatcherStepsService.updateKtDispatcherSteps(ktDispatcherSteps));
    }

    /**
     * 删除任务调度执行步骤
     */
    @RequiresPermissions("busi:dispatchSteps:remove")
    @Log(title = "任务调度执行步骤", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktDispatcherStepsService.deleteKtDispatcherStepsByIds(ids));
    }
}
