package com.hzzftech.watchdog.busi.constants;

/**
 * @PackageName com.hzzftech.watchdog.busi.constants
 * @Author xiangxiang
 * @Time 2021-11-27 23:35
 */

public class BusiConstant {
    // SSH 认证
    public static final String AUTH_TYPE_SSH = "S";
    // 用户名密码认证
    public static final String AUTH_TYPE_PASS = "P";

    // 字段被加密后，返回给前端的字符串
    public static final String ENCRPT_STR = "******";

    // git命令执行状态 0 成功
    public static final String GIT_COMMAND_SUCCESS = "0";

    // git命令执行状态 1 失败
    public static final String GIT_COMMAND_FAILED = "1";

    /** 参数组是否唯一的返回结果码 */
    public final static String PARAM_GROUP_UNIQUE = "0";
    public final static String PARAM_GROUP_NOT_UNIQUE = "1";

    // 是否使用DES加密
    public final static String DES_ENCRYPT_YES = "1";
    public final static String DES_ENCRYPT_NO = "0";

    // 通用状态 正常
    public final static String STATUS_YES = "0";
    // 通用状态 禁用
    public final static String STATUS_NO = "1";

    // 文件夹
    public final static String FILE_TYPE_DIRECTORY = "0";
    // 文件
    public final static String FILE_TYPE_F = "1";

    // 文件删除标志 未删除
    public final static String FILE_DEL_NO = "0";
    // 文件删除标志 删除
    public final static String FILE_DEL_YES = "1";

    // 文件版本启用状态
    public final static String FILE_VERSION_ENABLE_YES = "0";
    // 未启用
    public final static String FILE_VERSION_ENABLE_NO = "1";

    // 仓库类型 GIT
    public final static String REPOSITORY_TYPE_GIT = "0";
    // 仓库类型 文件
    public final static String REPOSITORY_TYPE_FILE = "1";
    // 仓库类型 数据库
    public final static String REPOSITORY_TYPE_DB = "2";

    // 任务-trans
    public final static String KETTLE_TYPE_TRANS = "ktr";
    // 任务-JOB
    public final static String KETTLE_TYPE_JOB = "kjb";

    // 任务-trans
    public final static String KETTLE_TYPE_FLAG_TRANS = "1";
    // 任务-JOB
    public final static String KETTLE_TYPE_FLAG_JOB = "2";

    // 执行日志键-前缀
    public final static String KEY_PREFFIX = "ks_";

    // 预警方式-手机
    public static final String CONTACT_TYPE_PHONE = "1";
    // 预警方式-email
    public static String CONTACT_TYPE_EMAIL = "2";
    // 预警方式-钉钉
    public static String CONTACT_TYPE_DINGDING = "3";

    // 运行异常消息-前缀
    public static String CONTACT_HEADER_MSG="kettle运行异常";

    // 用于数据库资源库
    public static Long ID_PREFIX = 10000L;

    // 挂起状态 1未挂起 2挂起 3挂起结束 4挂起失败
    public static String NOT_HANG_UP = "1";
    public static String HANG_UP = "2";
    public static String HANG_UP_END = "3";
    public static String HANG_UP_FAILED = "4";


}
