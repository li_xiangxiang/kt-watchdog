package com.hzzftech.watchdog.busi.core.executor;

public enum DispatcherStatus {

    STATUS_UNKNOW(-1, "未知状态"),
    STATUS_INIT(0, "初始化"),
    STATUS_WAITTING(1, "等待执行"),
    STATUS_RUNNING(2,"正在执行"),
    STATUS_SUCCESS(3, "执行成功"),
    STATUS_FAIL(4, "执行失败"),
    STATUS_CLOSED(5,"用户关闭"),
    STATUS_CHECKED_ERROR(6, "校验错误"),
    STATUS_PART_SUCCESS(32, "任务部分执行成功");

    int status;
    String desc;
    DispatcherStatus(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public static DispatcherStatus getStatus(int status) {
        for (DispatcherStatus s:
             values()) {
            if (s.status == status) return s;
        }
        return STATUS_UNKNOW;
    }

    public int getStatus() {
        return status;
    }
}
