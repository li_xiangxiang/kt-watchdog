package com.hzzftech.watchdog.busi.utils;

import com.hzzftech.watchdog.common.utils.StringUtils;
import com.sun.jna.platform.win32.WinBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Objects;

/**
 * @PackageName com.hzzftech.watchdog.busi.utils
 * @Author xiangxiang
 * @Time 2021-11-28 13:51
 */

public class FileUtils {

    public static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     * 删除单个文件
     * @param path
     * @return
     */
    public static boolean deleteFile(String path) {
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            return file.delete();
        }
        return false;
    }

    /**
     * 删除文件夹
     * @param path
     * @return
     */
    public static boolean deleteDirectory(String path) {
        //如果path不以文件分隔符结尾，自动添加文件分隔符
        if (!path.endsWith(File.separator)) {
            path = path + File.separator;
        }
        File dirFile = new File(path);
        //如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        //删除文件夹下的所有文件(包括子目录)
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            //删除子文件
            if (files[i].isFile()) {
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag) break;
            } //删除子目录
            else {
                flag = deleteDirectory(files[i].getAbsolutePath());
                if (!flag) break;
            }
        }
        if (!flag) return false;
        //删除当前目录
        if (dirFile.delete()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除文件夹或者文件
     * @param path
     * @return
     */
    public static boolean delete(String path) {
        File file = new File(path);
        // 判断目录或文件是否存在
        if (!file.exists()) {  // 不存在返回 false
            return false;
        } else {
            // 判断是否为文件
            if (file.isFile()) {  // 为文件时调用删除文件方法
                return deleteFile(path);
            } else {  // 为目录时调用删除目录方法
                return deleteDirectory(path);
            }
        }
    }

    /**
     * 获取文件前缀名
     * @param name
     * @return
     */
    public static String getPrefixName(String name) {
        if(StringUtils.isEmpty(name)) {
            throw new NullPointerException();
        }
        return name.substring(0,name.lastIndexOf("."));
    }

    /**
     * 获取文件后缀名
     * @return
     */
    public static String getSuffixName(String name) {
        if(StringUtils.isEmpty(name)) {
            throw new NullPointerException();
        }

        if (!name.contains(".")) return "-";

        String[] strArray = name.split("\\.");
        int suffixIndex = strArray.length -1;

        return strArray[suffixIndex];
    }

    public static void copyFile(String source, String target) {
        File f = new File(source);
        File targetf = new File(target);

        if (!f.exists()||!f.isFile()) {
            throw new RuntimeException("要拷贝的文件异常："+ source);
        } else if (targetf.exists()) {
            throw new RuntimeException("文件已存在"+targetf.getAbsolutePath());
        }

        // 创建文件
        if (!targetf.getParentFile().exists()) {
            targetf.getParentFile().mkdirs();
        }
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(f);
            out = new FileOutputStream(targetf);

            byte[] buffer = new byte[1024 * 10];
            int len;

            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } catch (Exception e) {
            logger.error("拷贝失败", e);
        } finally  {
           if (!Objects.isNull(in)) {
               try {
                   in.close();
               } catch (IOException e) {
                   logger.error("关闭输入流失败", e);
               }
           }

           if (!Objects.isNull(out)) {
               try {
                   out.close();
               } catch (IOException e) {
                   logger.error("关闭输出流失败", e);
               }
           }
        }
    }

    public static boolean exists(String filePath) {
        if (StringUtils.isEmpty(filePath)) {
            return false;
        }

        return new File(filePath).exists();
    }
}
