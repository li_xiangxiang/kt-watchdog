package com.hzzftech.watchdog.busi.exception;

/**
 * 文件类型错误异常
 */
public class FileTypeException extends Exception{
    public FileTypeException() {
        super();
    }

    public FileTypeException(String s) {
        super(s);
    }

    public FileTypeException( Throwable e) {
        super(e);
    }

    public FileTypeException(String s, Throwable e) {
        super(s, e);
    }
}
