package com.hzzftech.watchdog.busi.domain;

import com.hzzftech.watchdog.quartz.domain.SysJob;

public class KtJob extends SysJob {
    private Long dpId;

    private String dpIds;

    public String getDpIds() {
        return dpIds;
    }

    public void setDpIds(String dpIds) {
        this.dpIds = dpIds;
    }

    public Long getDpId() {
        return dpId;
    }

    public void setDpId(Long dpId) {
        this.dpId = dpId;
    }

    private String dpName;

    private String repoName;

    private String dpTypeName;

    private String dpRepoType;

    public String getDpName() {
        return dpName;
    }

    public void setDpName(String dpName) {
        this.dpName = dpName;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getDpRepoType() {
        return dpRepoType;
    }

    public void setDpRepoType(String dpRepoType) {
        this.dpRepoType = dpRepoType;
    }

    public String getDpTypeName() {
        return dpTypeName;
    }

    public void setDpTypeName(String dpTypeName) {
        this.dpTypeName = dpTypeName;
    }
}
