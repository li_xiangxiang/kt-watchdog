package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 文件资源库-文件信息对象 kt_file_repository_msg
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
public class KtFileRepositoryMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 父目录ID */
    @Excel(name = "父目录ID")
    private Long parentId;

    private String ancestors;

    /** 所属资源库ID */
    @Excel(name = "所属资源库ID")
    private Long fRepoId;

    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    private String fileType;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private Long fileSize;

    private String delFlag;

    private Date delTime;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    private String status;

    private String repositoryName;

    private String enableVersion;

    private Long createdBy;
    private String createdName;
    private String updatedBy;
    private String updatedName;

    public String getEnableVersion() {
        return enableVersion;
    }

    public void setEnableVersion(String enableVersion) {
        this.enableVersion = enableVersion;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public KtFileRepositoryMsg() {
        super();
    }

    public KtFileRepositoryMsg(Long parentId,String ancestors, Long fRepoId, String fileName, String enableVersion, String fileType, Long fileSize, String delFlag,Long createdBy, Date createdTime, String status) {
        this.parentId = parentId;
        this.ancestors = ancestors;
        this.fRepoId = fRepoId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.delFlag = delFlag;
        this.createdTime = createdTime;
        this.status = status;
        this.enableVersion = enableVersion;
        this.createdBy = createdBy;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public Date getDelTime() {
        return delTime;
    }

    public void setDelTime(Date delTime) {
        this.delTime = delTime;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setfRepoId(Long fRepoId) 
    {
        this.fRepoId = fRepoId;
    }

    public Long getfRepoId() 
    {
        return fRepoId;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setFileSize(Long fileSize) 
    {
        this.fileSize = fileSize;
    }

    public Long getFileSize() 
    {
        return fileSize;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("fRepoId", getfRepoId())
            .append("fileName", getFileName())
            .append("fileSize", getFileSize())
            .append("createdTime", getCreatedTime())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
