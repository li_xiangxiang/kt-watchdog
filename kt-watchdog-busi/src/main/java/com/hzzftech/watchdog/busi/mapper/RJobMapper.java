package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.RJob;

/**
 * 数据库资源库_jobMapper接口
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public interface RJobMapper 
{
    /**
     * 查询数据库资源库_job
     * 
     * @param idJob 数据库资源库_job主键
     * @return 数据库资源库_job
     */
    public RJob selectRJobByIdJob(Long idJob);

    /**
     * 查询数据库资源库_job列表
     * 
     * @param rJob 数据库资源库_job
     * @return 数据库资源库_job集合
     */
    public List<RJob> selectRJobList(RJob rJob);

    /**
     * 新增数据库资源库_job
     * 
     * @param rJob 数据库资源库_job
     * @return 结果
     */
    public int insertRJob(RJob rJob);

    /**
     * 修改数据库资源库_job
     * 
     * @param rJob 数据库资源库_job
     * @return 结果
     */
    public int updateRJob(RJob rJob);

    /**
     * 删除数据库资源库_job
     * 
     * @param idJob 数据库资源库_job主键
     * @return 结果
     */
    public int deleteRJobByIdJob(Long idJob);

    /**
     * 批量删除数据库资源库_job
     * 
     * @param idJobs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRJobByIdJobs(String[] idJobs);
}
