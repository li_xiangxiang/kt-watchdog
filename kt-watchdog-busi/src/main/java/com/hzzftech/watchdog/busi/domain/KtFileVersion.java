package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 文件版本信息对象 kt_file_version
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
public class KtFileVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 仓库ID */
    @Excel(name = "仓库ID")
    private Long repoId;

    /** 文件ID */
    @Excel(name = "文件ID")
    private Long fileId;

    /** 版本号 */
    @Excel(name = "版本号")
    private String version;

    /** 是否启用 */
    private String _enable;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 是否被删除 */
    @Excel(name = "是否被删除")
    private String isDeleted;

    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deletedTime;

    /** 删除人 */
    @Excel(name = "删除人")
    private Long deletedBy;

    // 仓库名称
    private String repoName;

    // 仓库文件夹
    private String repoDirect;

    // 文件名称
    private String fileName;

    public KtFileVersion(Long repoId, Long fileId, String version, String _enable, Date createdTime, String isDeleted) {
        this.repoId = repoId;
        this.fileId = fileId;
        this.version = version;
        this._enable = _enable;
        this.createdTime = createdTime;
        this.isDeleted = isDeleted;
    }

    public KtFileVersion() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoDirect() {
        return repoDirect;
    }

    public void setRepoDirect(String repoDirect) {
        this.repoDirect = repoDirect;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String get_enable() {
        return _enable;
    }

    public void set_enable(String _enable) {
        this._enable = _enable;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRepoId(Long repoId) 
    {
        this.repoId = repoId;
    }

    public Long getRepoId() 
    {
        return repoId;
    }
    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVersion() 
    {
        return version;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setIsDeleted(String isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public String getIsDeleted() 
    {
        return isDeleted;
    }
    public void setDeletedTime(Date deletedTime) 
    {
        this.deletedTime = deletedTime;
    }

    public Date getDeletedTime() 
    {
        return deletedTime;
    }
    public void setDeletedBy(Long deletedBy) 
    {
        this.deletedBy = deletedBy;
    }

    public Long getDeletedBy() 
    {
        return deletedBy;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("repoId", getRepoId())
            .append("fileId", getFileId())
            .append("version", getVersion())
            .append("createdTime", getCreatedTime())
            .append("isDeleted", getIsDeleted())
            .append("deletedTime", getDeletedTime())
            .append("deletedBy", getDeletedBy())
            .toString();
    }
}
