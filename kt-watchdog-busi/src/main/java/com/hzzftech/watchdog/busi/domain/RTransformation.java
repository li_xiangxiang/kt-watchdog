package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 数据库资源库_trans对象 R_TRANSFORMATION
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public class RTransformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idTransformation;

    /**  */
    @Excel(name = "")
    private Long idDirectory;

    private String directoryPath;

    /**  */
    @Excel(name = "")
    private String NAME;

    /**  */
    @Excel(name = "")
    private String DESCRIPTION;

    /**  */
    @Excel(name = "")
    private String extendedDescription;

    /**  */
    @Excel(name = "")
    private String transVersion;

    /**  */
    @Excel(name = "")
    private Long transStatus;

    /**  */
    @Excel(name = "")
    private Long idStepRead;

    /**  */
    @Excel(name = "")
    private Long idStepWrite;

    /**  */
    @Excel(name = "")
    private Long idStepInput;

    /**  */
    @Excel(name = "")
    private Long idStepOutput;

    /**  */
    @Excel(name = "")
    private Long idStepUpdate;

    /**  */
    @Excel(name = "")
    private Long idDatabaseLog;

    /**  */
    @Excel(name = "")
    private String tableNameLog;

    /**  */
    @Excel(name = "")
    private Integer useBatchid;

    /**  */
    @Excel(name = "")
    private Integer useLogfield;

    /**  */
    @Excel(name = "")
    private Long idDatabaseMaxdate;

    /**  */
    @Excel(name = "")
    private String tableNameMaxdate;

    /**  */
    @Excel(name = "")
    private String fieldNameMaxdate;

    /**  */
    @Excel(name = "")
    private Long offsetMaxdate;

    /**  */
    @Excel(name = "")
    private Long diffMaxdate;

    /**  */
    @Excel(name = "")
    private String createdUser;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdDate;

    /**  */
    @Excel(name = "")
    private String modifiedUser;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifiedDate;

    /**  */
    @Excel(name = "")
    private Long sizeRowset;

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public void setIdTransformation(Long idTransformation)
    {
        this.idTransformation = idTransformation;
    }

    public Long getIdTransformation() 
    {
        return idTransformation;
    }
    public void setIdDirectory(Long idDirectory) 
    {
        this.idDirectory = idDirectory;
    }

    public Long getIdDirectory() 
    {
        return idDirectory;
    }
    public void setNAME(String NAME) 
    {
        this.NAME = NAME;
    }

    public String getNAME() 
    {
        return NAME;
    }
    public void setDESCRIPTION(String DESCRIPTION) 
    {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getDESCRIPTION() 
    {
        return DESCRIPTION;
    }
    public void setExtendedDescription(String extendedDescription) 
    {
        this.extendedDescription = extendedDescription;
    }

    public String getExtendedDescription() 
    {
        return extendedDescription;
    }
    public void setTransVersion(String transVersion) 
    {
        this.transVersion = transVersion;
    }

    public String getTransVersion() 
    {
        return transVersion;
    }
    public void setTransStatus(Long transStatus) 
    {
        this.transStatus = transStatus;
    }

    public Long getTransStatus() 
    {
        return transStatus;
    }
    public void setIdStepRead(Long idStepRead) 
    {
        this.idStepRead = idStepRead;
    }

    public Long getIdStepRead() 
    {
        return idStepRead;
    }
    public void setIdStepWrite(Long idStepWrite) 
    {
        this.idStepWrite = idStepWrite;
    }

    public Long getIdStepWrite() 
    {
        return idStepWrite;
    }
    public void setIdStepInput(Long idStepInput) 
    {
        this.idStepInput = idStepInput;
    }

    public Long getIdStepInput() 
    {
        return idStepInput;
    }
    public void setIdStepOutput(Long idStepOutput) 
    {
        this.idStepOutput = idStepOutput;
    }

    public Long getIdStepOutput() 
    {
        return idStepOutput;
    }
    public void setIdStepUpdate(Long idStepUpdate) 
    {
        this.idStepUpdate = idStepUpdate;
    }

    public Long getIdStepUpdate() 
    {
        return idStepUpdate;
    }
    public void setIdDatabaseLog(Long idDatabaseLog) 
    {
        this.idDatabaseLog = idDatabaseLog;
    }

    public Long getIdDatabaseLog() 
    {
        return idDatabaseLog;
    }
    public void setTableNameLog(String tableNameLog) 
    {
        this.tableNameLog = tableNameLog;
    }

    public String getTableNameLog() 
    {
        return tableNameLog;
    }
    public void setUseBatchid(Integer useBatchid) 
    {
        this.useBatchid = useBatchid;
    }

    public Integer getUseBatchid() 
    {
        return useBatchid;
    }
    public void setUseLogfield(Integer useLogfield) 
    {
        this.useLogfield = useLogfield;
    }

    public Integer getUseLogfield() 
    {
        return useLogfield;
    }
    public void setIdDatabaseMaxdate(Long idDatabaseMaxdate) 
    {
        this.idDatabaseMaxdate = idDatabaseMaxdate;
    }

    public Long getIdDatabaseMaxdate() 
    {
        return idDatabaseMaxdate;
    }
    public void setTableNameMaxdate(String tableNameMaxdate) 
    {
        this.tableNameMaxdate = tableNameMaxdate;
    }

    public String getTableNameMaxdate() 
    {
        return tableNameMaxdate;
    }
    public void setFieldNameMaxdate(String fieldNameMaxdate) 
    {
        this.fieldNameMaxdate = fieldNameMaxdate;
    }

    public String getFieldNameMaxdate() 
    {
        return fieldNameMaxdate;
    }
    public void setOffsetMaxdate(Long offsetMaxdate) 
    {
        this.offsetMaxdate = offsetMaxdate;
    }

    public Long getOffsetMaxdate() 
    {
        return offsetMaxdate;
    }
    public void setDiffMaxdate(Long diffMaxdate) 
    {
        this.diffMaxdate = diffMaxdate;
    }

    public Long getDiffMaxdate() 
    {
        return diffMaxdate;
    }
    public void setCreatedUser(String createdUser) 
    {
        this.createdUser = createdUser;
    }

    public String getCreatedUser() 
    {
        return createdUser;
    }
    public void setCreatedDate(Date createdDate) 
    {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() 
    {
        return createdDate;
    }
    public void setModifiedUser(String modifiedUser) 
    {
        this.modifiedUser = modifiedUser;
    }

    public String getModifiedUser() 
    {
        return modifiedUser;
    }
    public void setModifiedDate(Date modifiedDate) 
    {
        this.modifiedDate = modifiedDate;
    }

    public Date getModifiedDate() 
    {
        return modifiedDate;
    }
    public void setSizeRowset(Long sizeRowset) 
    {
        this.sizeRowset = sizeRowset;
    }

    public Long getSizeRowset() 
    {
        return sizeRowset;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTransformation", getIdTransformation())
            .append("idDirectory", getIdDirectory())
            .append("NAME", getNAME())
            .append("DESCRIPTION", getDESCRIPTION())
            .append("extendedDescription", getExtendedDescription())
            .append("transVersion", getTransVersion())
            .append("transStatus", getTransStatus())
            .append("idStepRead", getIdStepRead())
            .append("idStepWrite", getIdStepWrite())
            .append("idStepInput", getIdStepInput())
            .append("idStepOutput", getIdStepOutput())
            .append("idStepUpdate", getIdStepUpdate())
            .append("idDatabaseLog", getIdDatabaseLog())
            .append("tableNameLog", getTableNameLog())
            .append("useBatchid", getUseBatchid())
            .append("useLogfield", getUseLogfield())
            .append("idDatabaseMaxdate", getIdDatabaseMaxdate())
            .append("tableNameMaxdate", getTableNameMaxdate())
            .append("fieldNameMaxdate", getFieldNameMaxdate())
            .append("offsetMaxdate", getOffsetMaxdate())
            .append("diffMaxdate", getDiffMaxdate())
            .append("createdUser", getCreatedUser())
            .append("createdDate", getCreatedDate())
            .append("modifiedUser", getModifiedUser())
            .append("modifiedDate", getModifiedDate())
            .append("sizeRowset", getSizeRowset())
            .toString();
    }
}
