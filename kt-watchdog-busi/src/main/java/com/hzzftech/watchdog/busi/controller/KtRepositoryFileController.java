package com.hzzftech.watchdog.busi.controller;

import com.hzzftech.watchdog.busi.constants.BusiConstant;
import com.hzzftech.watchdog.busi.domain.KtRepositoryFile;
import com.hzzftech.watchdog.busi.service.IKtRepositoryFileService;
import com.hzzftech.watchdog.busi.service.IKtRepositoryService;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.core.domain.Ztree;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.common.utils.StringUtils;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * GIT仓库文件Controller
 * 
 * @author liquanxiang
 * @date 2021-12-04
 */
@Controller
@RequestMapping("/busi/repository_file")
public class KtRepositoryFileController extends BaseController
{
    private String prefix = "busi/repository_file";

    @Autowired
    private IKtRepositoryFileService ktRepositoryFileService;

    @Autowired
    private IKtRepositoryService repositoryService;

    @RequiresPermissions("busi:repository_file:view")
    @GetMapping("/{repoId}")
    public String repository_file(@PathVariable("repoId") Long repoId, ModelMap mmap, @RequestParam("selectedIds") String selectedIds)
    {
        mmap.addAttribute("repoId", repoId);
        mmap.addAttribute("repoName", repositoryService.selectKtRepositoryByRepositoryId(repoId).getRepositoryName());
        mmap.addAttribute("selectedIds", selectedIds);
        return "busi/dispatcher/git_repo_file";
    }

    /**
     * 查询仓库文件列表
     */
    @RequiresPermissions("busi:repository_file:list")
    @PostMapping("/list/{repositoryId}")
    @ResponseBody
    public List<KtRepositoryFile> list(@PathVariable("repositoryId") Long repositoryId, KtRepositoryFile ktRepositoryFile)
    {
        ktRepositoryFile.setRepositoryId(repositoryId);
        return ktRepositoryFileService.selectKtRepositoryFileList(ktRepositoryFile);
    }

    /**
     * 查询仓库文件列表
     */
    @RequiresPermissions("busi:repository_file:list")
    @PostMapping("/list2/{repositoryId}")
    @ResponseBody
    public TableDataInfo list2(@PathVariable("repositoryId") Long repositoryId, KtRepositoryFile ktRepositoryFile)
    {
        startPage();
        ktRepositoryFile.setRepositoryId(repositoryId);
        ktRepositoryFile.setType(BusiConstant.FILE_TYPE_F);
        return getDataTable(ktRepositoryFileService.selectKtRepositoryFileList(ktRepositoryFile));
    }

    /**
     * 新增按钮
     * @param parentId
     * @param repositoryId
     * @param mmap
     * @return
     */
    @GetMapping("/add/{repositoryId}/{parentId}")
    public String add(@PathVariable("repositoryId") Long repositoryId, @PathVariable("parentId") Long parentId, ModelMap mmap)
    {
        KtRepositoryFile file = ktRepositoryFileService.selectRepoById(repositoryId, parentId);
        if (0L == parentId) {
            file.setfName(file.getfName()+"("+file.getRepositoryName()+")");
        }
        mmap.put("repo", file);
        return prefix + "/add";
    }


    /**
     * 文件树结构
     * @param repositoryId
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("selectRepoFileTree/{repositoryId}/{id}")
    public String selectRepoFileTree(@PathVariable("repositoryId") Long repositoryId,@PathVariable("id") Long id, ModelMap mmap) {
        KtRepositoryFile f = ktRepositoryFileService.selectRepositoryFileByRepoIdAndFid(repositoryId, id);
        mmap.put("repo",f);
        return prefix + "/tree";
    }


    /**
     * 文件树结构构造
     * @param repoId
     * @return
     */
    @ResponseBody
    @GetMapping("fileTreeData/{repoId}")
    public  List<Ztree> zTree(@PathVariable("repoId") Long repoId) {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<KtRepositoryFile> files = ktRepositoryFileService.selectKtRepositoryByRepoId(repoId);
        boolean isCheck = StringUtils.isNotNull(files);
        for (KtRepositoryFile file : files)
        {
            Ztree ztree = new Ztree();
            ztree.setId(file.getId());
            ztree.setpId(file.getParentId());
            ztree.setName(file.getfName());
            if (file.getParentId().equals(0L)) {
                ztree.setTitle(file.getRepositoryName());
            }
            if (isCheck)
            {
                ztree.setChecked(true);
            }
            ztrees.add(ztree);
        }
        return ztrees;
    }

    /**
     * 导出仓库文件
     */
    @RequiresPermissions("busi:repository_file:export")
    @Log(title = "仓库文件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtRepositoryFile ktRepositoryFile)
    {
        List<KtRepositoryFile> list = ktRepositoryFileService.selectKtRepositoryFileList(ktRepositoryFile);
        ExcelUtil<KtRepositoryFile> util = new ExcelUtil<KtRepositoryFile>(KtRepositoryFile.class);
        return util.exportExcel(list, "仓库文件数据");
    }

    /**
     * 新增仓库文件
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存仓库文件
     */
    @RequiresPermissions("busi:repository_file:add")
    @Log(title = "仓库文件", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtRepositoryFile ktRepositoryFile)
    {
        return toAjax(ktRepositoryFileService.insertKtRepositoryFile(ktRepositoryFile));
    }

    /**
     * 修改仓库文件
     */
    @RequiresPermissions("busi:repository_file:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        KtRepositoryFile ktRepositoryFile = ktRepositoryFileService.selectKtRepositoryFileById(id);
        mmap.put("ktRepositoryFile", ktRepositoryFile);
        return prefix + "/edit";
    }

    /**
     * 修改保存仓库文件
     */
    @RequiresPermissions("busi:repository_file:edit")
    @Log(title = "仓库文件", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtRepositoryFile ktRepositoryFile)
    {
        return toAjax(ktRepositoryFileService.updateKtRepositoryFile(ktRepositoryFile));
    }

    /**
     * 删除仓库文件
     */
    @RequiresPermissions("busi:repository_file:remove")
    @Log(title = "仓库文件", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktRepositoryFileService.deleteKtRepositoryFileByIds(ids));
    }
}
