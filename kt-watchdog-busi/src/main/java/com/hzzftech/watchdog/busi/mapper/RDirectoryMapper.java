package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.RDirectory;

/**
 * 数据库资源库_文件夹Mapper接口
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public interface RDirectoryMapper 
{
    /**
     * 查询数据库资源库_文件夹
     * 
     * @param idDirectory 数据库资源库_文件夹主键
     * @return 数据库资源库_文件夹
     */
    public RDirectory selectRDirectoryByIdDirectory(Long idDirectory);

    /**
     * 查询数据库资源库_文件夹列表
     * 
     * @param rDirectory 数据库资源库_文件夹
     * @return 数据库资源库_文件夹集合
     */
    public List<RDirectory> selectRDirectoryList(RDirectory rDirectory);

    /**
     * 新增数据库资源库_文件夹
     * 
     * @param rDirectory 数据库资源库_文件夹
     * @return 结果
     */
    public int insertRDirectory(RDirectory rDirectory);

    /**
     * 修改数据库资源库_文件夹
     * 
     * @param rDirectory 数据库资源库_文件夹
     * @return 结果
     */
    public int updateRDirectory(RDirectory rDirectory);

    /**
     * 删除数据库资源库_文件夹
     * 
     * @param idDirectory 数据库资源库_文件夹主键
     * @return 结果
     */
    public int deleteRDirectoryByIdDirectory(Long idDirectory);

    /**
     * 批量删除数据库资源库_文件夹
     * 
     * @param idDirectorys 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRDirectoryByIdDirectorys(String[] idDirectorys);
}
