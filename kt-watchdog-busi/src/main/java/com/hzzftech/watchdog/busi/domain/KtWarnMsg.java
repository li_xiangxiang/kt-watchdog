package com.hzzftech.watchdog.busi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * kettle运行提醒信息对象 kt_warn_msg
 * 
 * @author kt-watchdog
 * @date 2022-01-03
 */
public class KtWarnMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 联系类型 */
    @Excel(name = "联系类型")
    private String contactType;

    /** 电话号码 */
    @Excel(name = "电话号码")
    private String phoneNum;

    /** e_mail地址 */
    @Excel(name = "e_mail地址")
    private String eMailAddr;

    private String dingWebHook;

    private String dingSecretPrefix;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDingWebHook() {
        return dingWebHook;
    }

    public void setDingWebHook(String dingWebHook) {
        this.dingWebHook = dingWebHook;
    }

    public String getDingSecretPrefix() {
        return dingSecretPrefix;
    }

    public void setDingSecretPrefix(String dingSecretPrefix) {
        this.dingSecretPrefix = dingSecretPrefix;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setContactType(String contactType) 
    {
        this.contactType = contactType;
    }

    public String getContactType() 
    {
        return contactType;
    }
    public void setPhoneNum(String phoneNum) 
    {
        this.phoneNum = phoneNum;
    }

    public String getPhoneNum() 
    {
        return phoneNum;
    }
    public void seteMailAddr(String eMailAddr) 
    {
        this.eMailAddr = eMailAddr;
    }

    public String geteMailAddr() 
    {
        return eMailAddr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contactType", getContactType())
            .append("phoneNum", getPhoneNum())
            .append("eMailAddr", geteMailAddr())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
