package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtRepository;

/**
 * 文件资源库Mapper接口
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
public interface KtRepositoryMapper 
{
    /**
     * 查询文件资源库
     * 
     * @param repositoryId 文件资源库主键
     * @return 文件资源库
     */
    public KtRepository selectKtRepositoryByRepositoryId(Long repositoryId);

    /**
     * 查询文件资源库列表
     * 
     * @param ktRepository 文件资源库
     * @return 文件资源库集合
     */
    public List<KtRepository> selectKtRepositoryList(KtRepository ktRepository);

    /**
     * 新增文件资源库
     * 
     * @param ktRepository 文件资源库
     * @return 结果
     */
    public int insertKtRepository(KtRepository ktRepository);

    /**
     * 修改文件资源库
     * 
     * @param ktRepository 文件资源库
     * @return 结果
     */
    public int updateKtRepository(KtRepository ktRepository);

    /**
     * 删除文件资源库
     * 
     * @param repositoryId 文件资源库主键
     * @return 结果
     */
    public int deleteKtRepositoryByRepositoryId(Long repositoryId);

    /**
     * 批量删除文件资源库
     * 
     * @param repositoryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtRepositoryByRepositoryIds(String[] repositoryIds);

    public KtRepository selectByRepositoryName(String name);

    public KtRepository selectByRepositoryLocation(String location);

}
