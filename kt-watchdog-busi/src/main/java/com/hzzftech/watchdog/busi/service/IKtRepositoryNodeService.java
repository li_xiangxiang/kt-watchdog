package com.hzzftech.watchdog.busi.service;

import java.util.Date;
import java.util.List;

import com.hzzftech.watchdog.busi.domain.KtExecutionLog;
import com.hzzftech.watchdog.busi.domain.KtRepositoryNode;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.ShiroUtils;
import com.hzzftech.watchdog.system.domain.Item;

/**
 * 资源库节点Service接口
 * 
 * @author liquanxiang
 * @date 2021-11-27
 */
public interface IKtRepositoryNodeService 
{
    /**
     * 查询资源库节点
     * 
     * @param nodeId 资源库节点主键
     * @return 资源库节点
     */
    public KtRepositoryNode selectKtRepositoryNodeByNodeId(Long nodeId);

    /**
     * 查询资源库节点列表
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 资源库节点集合
     */
    public List<KtRepositoryNode> selectKtRepositoryNodeList(KtRepositoryNode ktRepositoryNode);

    /**
     * 新增资源库节点
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 结果
     */
    public int insertKtRepositoryNode(KtRepositoryNode ktRepositoryNode);

    /**
     * 修改资源库节点
     * 
     * @param ktRepositoryNode 资源库节点
     * @return 结果
     */
    public int updateKtRepositoryNode(KtRepositoryNode ktRepositoryNode);

    /**
     * 批量删除资源库节点
     * 
     * @param nodeIds 需要删除的资源库节点主键集合
     * @return 结果
     */
    public int deleteKtRepositoryNodeByNodeIds(String nodeIds);

    /**
     * 删除资源库节点信息
     * 
     * @param nodeId 资源库节点主键
     * @return 结果
     */
    public int deleteKtRepositoryNodeByNodeId(Long nodeId);

    default void addCommonHandle(KtRepositoryNode node) {
        node.setCreatedBy(ShiroUtils.getUserId());
        node.setCreatedTime(new Date());
    }

    default void updateCommonHandle(KtRepositoryNode node) {
        node.setUpdatedBy(ShiroUtils.getUserId());
        node.setUpdatedTime(new Date());
    }

    AjaxResult checkNodeStatus(Long nodeId);

    List<KtRepositoryNode> selectKtRepositoryNodeListMonitor(KtRepositoryNode ktRepositoryNode);

    List<Item> selectNodeRunLogByCarteId(KtExecutionLog log);
}
