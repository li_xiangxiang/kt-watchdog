package com.hzzftech.watchdog.busi.controller;

import com.hzzftech.watchdog.busi.core.executor.starter.KettleEnvironmentStarter;
import com.hzzftech.watchdog.busi.domain.RTransformation;
import com.hzzftech.watchdog.busi.objs.WatchdogPageInfo;
import com.hzzftech.watchdog.busi.service.IKtParamsService;
import com.hzzftech.watchdog.busi.service.IKtRepositoryNodeService;
import com.hzzftech.watchdog.busi.service.IRTransformationService;
import com.hzzftech.watchdog.busi.service.impl.KtDispatcherServiceImpl;
import com.hzzftech.watchdog.busi.utils.PageInfoUtils;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.config.KtWatchdogConfig;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;
import com.hzzftech.watchdog.common.enums.BusinessType;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.pentaho.di.repository.LongObjectId;
import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
import org.pentaho.di.trans.TransMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据库资源库_transController
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
@Controller
@RequestMapping("/busi/TRANSFORMATION")
public class RTransformationController extends BaseController
{
    private String prefix = "busi/TRANSFORMATION";

    @Autowired
    private IRTransformationService rTransformationService;

    @Autowired
    private KtDispatcherServiceImpl ktDispatcherService;

    @Autowired
    private IKtParamsService iKtParamsService;

    @Autowired
    private IKtRepositoryNodeService nodeService;

    @Value("${kt-watchdog.DES_SECURITY_KEY}")
    private String desSecurityKey;

    @RequiresPermissions("busi:TRANSFORMATION:view")
    @GetMapping()
    public String TRANSFORMATION()
    {
        return prefix + "/TRANSFORMATION";
    }

    @RequiresPermissions("busi:TRANSFORMATION:view")
    @GetMapping("/select")
    public String TRANSFORMATIONSelect()
    {
        return prefix + "/TRANSFORMATIONSelect";
    }

    /**
     * 查询数据库资源库_trans列表
     */
    @RequiresPermissions("busi:TRANSFORMATION:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RTransformation rTransformation)
    {
        Integer count = rTransformationService.selectRTransformationCount(rTransformation);
        if (count > 0) {
            WatchdogPageInfo pageInfo = PageInfoUtils.getInfo();
            List<RTransformation> list = rTransformationService.selectRTransformationList(rTransformation, pageInfo.getPgNum(), pageInfo.getPgSize(), null);
            return PageInfoUtils.getDataTable(list, count);
        } else {
            return PageInfoUtils.getDataTable(new ArrayList<>(0), 0);
        }
    }
    /**
     * 删除数据库资源库_trans
     */
    @RequiresPermissions("busi:TRANSFORMATION:remove")
    @Log(title = "数据库资源库_trans", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        KettleDatabaseRepository repository = (KettleDatabaseRepository) KettleEnvironmentStarter.instance.getRepository();
        int delCount = 0;
        try {
            for (String id : ids.split(",")) {
                repository.deleteTransformation(new LongObjectId(Long.parseLong(id)));
                delCount ++;
            }
        } catch (Exception e) {
            logger.error("删除失败", e);
        }
        return toAjax(delCount);
    }

    /**
     * 导出 JOB xml
     */
    @RequiresPermissions("busi:JOB:export")
    @Log(title = "kettle运行提醒信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RTransformation trans)
    {
        KettleDatabaseRepository repository = (KettleDatabaseRepository) KettleEnvironmentStarter.instance.getRepository();
        FileOutputStream out = null;
        try {
            TransMeta transMeta = repository.loadTransformation(new LongObjectId(trans.getIdTransformation()), "");
            String xml = transMeta.getXML();
            String fileName = transMeta.getName()+".ktr";
            String downloadPath = KtWatchdogConfig.getDownloadPath() +fileName;
            File desc = new File(downloadPath);
            if (!desc.getParentFile().exists())
            {
                desc.getParentFile().mkdirs();
            }
            out = new FileOutputStream(downloadPath);
            out.write(xml.getBytes());

            return AjaxResult.success(fileName);
        } catch (Exception e) {
            logger.error("导出JOB异常", e);
            return AjaxResult.error();
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.error("导出JOB异常", e);
                }
            }
        }
    }

}
