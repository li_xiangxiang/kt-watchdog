package com.hzzftech.watchdog.busi.service;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtFileRepository;

/**
 * 文件资源库Service接口
 * 
 * @author kt-watchdog
 * @date 2021-12-09
 */
public interface IKtFileRepositoryService 
{
    /**
     * 查询文件资源库
     * 
     * @param id 文件资源库主键
     * @return 文件资源库
     */
    public KtFileRepository selectKtFileRepositoryById(Long id);

    /**
     * 查询文件资源库列表
     * 
     * @param ktFileRepository 文件资源库
     * @return 文件资源库集合
     */
    public List<KtFileRepository> selectKtFileRepositoryList(KtFileRepository ktFileRepository);

    /**
     * 新增文件资源库
     * 
     * @param ktFileRepository 文件资源库
     * @return 结果
     */
    public int insertKtFileRepository(KtFileRepository ktFileRepository);

    /**
     * 修改文件资源库
     * 
     * @param ktFileRepository 文件资源库
     * @return 结果
     */
    public int updateKtFileRepository(KtFileRepository ktFileRepository);

    /**
     * 批量删除文件资源库
     * 
     * @param ids 需要删除的文件资源库主键集合
     * @return 结果
     */
    public int deleteKtFileRepositoryByIds(String ids);

    /**
     * 删除文件资源库信息
     * 
     * @param id 文件资源库主键
     * @return 结果
     */
    public int deleteKtFileRepositoryById(Long id);

    List<KtFileRepository> selectAllEnableFileRepository();

    List<KtFileRepository> selectDirectoryUnique(Long id, String name);
}
