package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtExecutionLogMapper;
import com.hzzftech.watchdog.busi.domain.KtExecutionLog;
import com.hzzftech.watchdog.busi.service.IKtExecutionLogService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 任务调度日志Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-12-14
 */
@Service
public class KtExecutionLogServiceImpl implements IKtExecutionLogService 
{
    @Autowired
    private KtExecutionLogMapper ktExecutionLogMapper;

    /**
     * 查询任务调度日志
     * 
     * @param id 任务调度日志主键
     * @return 任务调度日志
     */
    @Override
    public KtExecutionLog selectKtExecutionLogById(String id)
    {
        return ktExecutionLogMapper.selectKtExecutionLogById(id);
    }

    /**
     * 查询任务调度日志列表
     * 
     * @param ktExecutionLog 任务调度日志
     * @return 任务调度日志
     */
    @Override
    public List<KtExecutionLog> selectKtExecutionLogList(KtExecutionLog ktExecutionLog)
    {
        return ktExecutionLogMapper.selectKtExecutionLogList(ktExecutionLog);
    }

    /**
     * 新增任务调度日志
     * 
     * @param ktExecutionLog 任务调度日志
     * @return 结果
     */
    @Override
    public int insertKtExecutionLog(KtExecutionLog ktExecutionLog)
    {
        return ktExecutionLogMapper.insertKtExecutionLog(ktExecutionLog);
    }

    /**
     * 修改任务调度日志
     * 
     * @param ktExecutionLog 任务调度日志
     * @return 结果
     */
    @Override
    public int updateKtExecutionLog(KtExecutionLog ktExecutionLog)
    {
        return ktExecutionLogMapper.updateKtExecutionLog(ktExecutionLog);
    }

    /**
     * 批量删除任务调度日志
     * 
     * @param ids 需要删除的任务调度日志主键
     * @return 结果
     */
    @Override
    public int deleteKtExecutionLogByIds(String ids)
    {
        return ktExecutionLogMapper.deleteKtExecutionLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务调度日志信息
     * 
     * @param id 任务调度日志主键
     * @return 结果
     */
    @Override
    public int deleteKtExecutionLogById(Long id)
    {
        return ktExecutionLogMapper.deleteKtExecutionLogById(id);
    }
}
