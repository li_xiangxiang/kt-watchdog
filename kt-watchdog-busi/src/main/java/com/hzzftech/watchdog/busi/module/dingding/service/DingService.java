package com.hzzftech.watchdog.busi.module.dingding.service;

public interface DingService {

    boolean sendDingTalk(String msg);
}
