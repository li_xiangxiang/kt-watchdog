package com.hzzftech.watchdog.busi.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.hzzftech.watchdog.busi.domain.KtDispatcher;
import com.hzzftech.watchdog.busi.service.IKtDispatcherService;
import com.hzzftech.watchdog.busi.service.IKtParamsService;
import com.hzzftech.watchdog.common.core.text.Convert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtParamsGroup;
import com.hzzftech.watchdog.busi.service.IKtParamsGroupService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 运行参数组Controller
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
@Controller
@RequestMapping("/watchdog/paramsGroup")
public class KtParamsGroupController extends BaseController
{
    private String prefix = "busi/param/paramgroup";

    @Autowired
    private IKtParamsGroupService ktParamsGroupService;

    @RequiresPermissions("busi:paramsGroup:view")
    @GetMapping()
    public String paramsGroup()
    {
        return prefix + "/paramgroup";
    }

    /**
     * 查询运行参数组列表
     */
    @RequiresPermissions("busi:paramsGroup:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtParamsGroup ktParamsGroup)
    {
        startPage();
        List<KtParamsGroup> list = ktParamsGroupService.selectKtParamsGroupList(ktParamsGroup);
        return getDataTable(list);
    }

    /**
     * 导出运行参数组列表
     */
    @RequiresPermissions("busi:paramsGroup:export")
    @Log(title = "运行参数组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtParamsGroup ktParamsGroup)
    {
        List<KtParamsGroup> list = ktParamsGroupService.selectKtParamsGroupList(ktParamsGroup);
        ExcelUtil<KtParamsGroup> util = new ExcelUtil<KtParamsGroup>(KtParamsGroup.class);
        return util.exportExcel(list, "运行参数组数据");
    }

    /**
     * 新增运行参数组
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存运行参数组
     */
    @RequiresPermissions("busi:paramsGroup:add")
    @Log(title = "运行参数组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated KtParamsGroup ktParamsGroup)
    {

        KtParamsGroup group = ktParamsGroupService.selectKtParamsGroupByName(ktParamsGroup.getParamsGroupName());
        if (!Objects.isNull(group)) {
            return AjaxResult.error("参数组名称“"+ktParamsGroup.getParamsGroupName()+"”已经存在");
        }
        return toAjax(ktParamsGroupService.insertKtParamsGroup(ktParamsGroup));
    }

    /**
     * 修改运行参数组
     */
    @RequiresPermissions("busi:paramsGroup:edit")
    @GetMapping("/edit/{paramsGroupId}")
    public String edit(@PathVariable("paramsGroupId") Long paramsGroupId, ModelMap mmap)
    {
        KtParamsGroup ktParamsGroup = ktParamsGroupService.selectKtParamsGroupByParamsGroupId(paramsGroupId);
        mmap.put("ktParamsGroup", ktParamsGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存运行参数组
     */
    @RequiresPermissions("busi:paramsGroup:edit")
    @Log(title = "运行参数组", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtParamsGroup ktParamsGroup)
    {
        KtParamsGroup group = ktParamsGroupService.selectKtParamsGroupByName(ktParamsGroup.getParamsGroupName());
        if (!Objects.isNull(group) && !ktParamsGroup.getParamsGroupId().equals(group.getParamsGroupId())) {
            return AjaxResult.error("参数组名称“"+ktParamsGroup.getParamsGroupName()+"”已经存在");
        }
        return toAjax(ktParamsGroupService.updateKtParamsGroup(ktParamsGroup));
    }

    @Autowired
    private IKtDispatcherService dispatcherService;

    @Autowired
    private IKtParamsService paramsService;

    /**
     * 删除运行参数组
     */
    @RequiresPermissions("busi:paramsGroup:remove")
    @Log(title = "运行参数组", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        int count = 0;
        for (String id : Convert.toStrArray(ids)) {
            long _id = Long.parseLong(id);
            KtDispatcher dispatcher = new KtDispatcher();
            dispatcher.setParamsGroupId(_id);
            List<KtDispatcher> list = dispatcherService.selectKtDispatcherList(dispatcher);
            if (!list.isEmpty()) {
                return AjaxResult.error("删除失败，有任务引用了该参数组");
            }

            // 先删除该参数组下的所有参数
            paramsService.deleteKtParamsByParamsGroupId(_id);

            // 后删除此参数组
            ktParamsGroupService.deleteKtParamsGroupByParamsGroupId(_id);
            count ++;
        }
        return toAjax(count);
    }

    /**
     * 校验参数组
     */
    @PostMapping("/checkDictTypeUnique")
    @ResponseBody
    public String checkParamGroupUnique(KtParamsGroup ktParamsGroup) {
        return ktParamsGroupService.checkDictTypeUnique(ktParamsGroup);
    }

    @RequiresPermissions("busi:paramsGroup:list")
    @GetMapping("/detail/{dictId}")
    public String detail(@PathVariable("dictId") Long dictId, ModelMap mmap)
    {
        mmap.put("dict", ktParamsGroupService.selectKtParamsGroupByParamsGroupId(dictId));
        mmap.put("dictList", ktParamsGroupService.selectAllKtParamsGroupList());
        return "busi/param/params/params";
    }
}
