package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 kt_dispatcher_failed_msg
 * 
 * @author kt-watchdog
 * @date 2022-03-06
 */
public class KtDispatcherFailedMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  ID */
    private Long id;

    /** 调度ID */
    @Excel(name = "调度ID")
    private Long dpId;

    /** 调度名称 */
    @Excel(name = "调度名称")
    private String dpName;

    /** 失败时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "失败时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date failedTime;

    /** 发送次数 */
    @Excel(name = "发送次数")
    private Long sendTime;

    /** 最后一次发送时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后一次发送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastSendTime;

    private String status;

    private Long userId;

    private String userName;

    private String email;

    private String phone;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDpId(Long dpId) 
    {
        this.dpId = dpId;
    }

    public Long getDpId() 
    {
        return dpId;
    }
    public void setDpName(String dpName) 
    {
        this.dpName = dpName;
    }

    public String getDpName() 
    {
        return dpName;
    }
    public void setFailedTime(Date failedTime) 
    {
        this.failedTime = failedTime;
    }

    public Date getFailedTime() 
    {
        return failedTime;
    }
    public void setSendTime(Long sendTime) 
    {
        this.sendTime = sendTime;
    }

    public Long getSendTime() 
    {
        return sendTime;
    }
    public void setLastSendTime(Date lastSendTime) 
    {
        this.lastSendTime = lastSendTime;
    }

    public Date getLastSendTime() 
    {
        return lastSendTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dpId", getDpId())
            .append("dpName", getDpName())
            .append("failedTime", getFailedTime())
            .append("sendTime", getSendTime())
            .append("lastSendTime", getLastSendTime())
            .toString();
    }
}
