package com.hzzftech.watchdog.busi.controller;

import java.util.List;

import com.hzzftech.watchdog.busi.service.IKtParamsGroupService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hzzftech.watchdog.common.annotation.Log;
import com.hzzftech.watchdog.common.enums.BusinessType;
import com.hzzftech.watchdog.busi.domain.KtParams;
import com.hzzftech.watchdog.busi.service.IKtParamsService;
import com.hzzftech.watchdog.common.core.controller.BaseController;
import com.hzzftech.watchdog.common.core.domain.AjaxResult;
import com.hzzftech.watchdog.common.utils.poi.ExcelUtil;
import com.hzzftech.watchdog.common.core.page.TableDataInfo;

/**
 * 运行参数Controller
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
@Controller
@RequestMapping("/watchdog/params")
public class KtParamsController extends BaseController
{
    private String prefix = "busi/param/params";

    @Autowired
    private IKtParamsService ktParamsService;

    @RequiresPermissions("busi:params:view")
    @GetMapping()
    public String params()
    {
        return prefix + "/params";
    }

    /**
     * 查询运行参数列表
     */
    @RequiresPermissions("busi:params:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(KtParams ktParams)
    {
        startPage();
        List<KtParams> list = ktParamsService.selectKtParamsList(ktParams);
        return getDataTable(list);
    }

    /**
     * 导出运行参数列表
     */
    @RequiresPermissions("busi:params:export")
    @Log(title = "运行参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(KtParams ktParams)
    {
        List<KtParams> list = ktParamsService.selectKtParamsList(ktParams);
        ExcelUtil<KtParams> util = new ExcelUtil<KtParams>(KtParams.class);
        return util.exportExcel(list, "运行参数数据");
    }

    /**
     * 新增运行参数
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    @Autowired
    private IKtParamsGroupService paramsGroupService;

    @GetMapping("/add/{paramsGroupId}")
    public String addPage(@PathVariable("paramsGroupId") Long paramsGroupId, ModelMap mmp)
    {
        mmp.put("paramsGroup", paramsGroupService.selectKtParamsGroupByParamsGroupId(paramsGroupId));
        return prefix + "/add";
    }

    /**
     * 新增保存运行参数
     */
    @RequiresPermissions("busi:params:add")
    @Log(title = "运行参数", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(KtParams ktParams)
    {
        KtParams p = new KtParams();
        p.setParamsGroupId(ktParams.getParamsGroupId());
        p.setParamsKey(ktParams.getParamsKey());
        List<KtParams> list = ktParamsService.selectKtParamsList(p);
        if (list.size() > 0) {
            return AjaxResult.error("参数键重复");
        }
        return toAjax(ktParamsService.insertKtParams(ktParams));
    }

    /**
     * 修改运行参数
     */
    @RequiresPermissions("busi:params:edit")
    @GetMapping("/edit/{paramsId}")
    public String edit(@PathVariable("paramsId") Long paramsId, ModelMap mmap)
    {
        KtParams ktParams = ktParamsService.selectKtParamsByParamsId(paramsId);
        mmap.put("ktParams", ktParams);
        return prefix + "/edit";
    }

    /**
     * 修改保存运行参数
     */
    @RequiresPermissions("busi:params:edit")
    @Log(title = "运行参数", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(KtParams ktParams)
    {
        KtParams p = new KtParams();
        p.setParamsGroupId(ktParams.getParamsGroupId());
        p.setParamsKey(ktParams.getParamsKey());
        List<KtParams> list = ktParamsService.selectKtParamsList(p);
        for (KtParams p1 : list ) {
            if (p1.getParamsKey().equals(ktParams.getParamsKey()) && !p1.getParamsValue().equals(ktParams.getParamsValue())) {
                return AjaxResult.error();
            }
        }
        return toAjax(ktParamsService.updateKtParams(ktParams));
    }

    /**
     * 删除运行参数
     */
    @RequiresPermissions("busi:params:remove")
    @Log(title = "运行参数", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ktParamsService.deleteKtParamsByParamsIds(ids));
    }
}
