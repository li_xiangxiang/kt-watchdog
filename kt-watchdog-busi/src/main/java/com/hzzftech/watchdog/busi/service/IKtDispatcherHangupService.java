package com.hzzftech.watchdog.busi.service;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtDispatcherHangup;

/**
 * 任务挂起Service接口
 * 
 * @author kt-watchdog
 * @date 2022-02-21
 */
public interface IKtDispatcherHangupService 
{
    /**
     * 查询任务挂起
     * 
     * @param id 任务挂起主键
     * @return 任务挂起
     */
    public KtDispatcherHangup selectKtDispatcherHangupById(Long id);

    /**
     * 查询任务挂起列表
     * 
     * @param ktDispatcherHangup 任务挂起
     * @return 任务挂起集合
     */
    public List<KtDispatcherHangup> selectKtDispatcherHangupList(KtDispatcherHangup ktDispatcherHangup);

    /**
     * 新增任务挂起
     * 
     * @param ktDispatcherHangup 任务挂起
     * @return 结果
     */
    public int insertKtDispatcherHangup(KtDispatcherHangup ktDispatcherHangup);

    /**
     * 修改任务挂起
     * 
     * @param ktDispatcherHangup 任务挂起
     * @return 结果
     */
    public int updateKtDispatcherHangup(KtDispatcherHangup ktDispatcherHangup);

    /**
     * 批量删除任务挂起
     * 
     * @param ids 需要删除的任务挂起主键集合
     * @return 结果
     */
    public int deleteKtDispatcherHangupByIds(String ids);

    /**
     * 删除任务挂起信息
     * 
     * @param id 任务挂起主键
     * @return 结果
     */
    public int deleteKtDispatcherHangupById(Long id);

    int hangUpJob(KtDispatcherHangup ktDispatcherHangup);
}
