package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 数据库资源库_job对象 R_JOB
 * 
 * @author kt-watchdog
 * @date 2022-01-27
 */
public class RJob extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idJob;

    /**  */
    @Excel(name = "")
    private Long idDirectory;

    private String directoryPath;

    /**  */
    @Excel(name = "")
    private String NAME;

    /**  */
    @Excel(name = "")
    private String DESCRIPTION;

    /**  */
    @Excel(name = "")
    private String extendedDescription;

    /**  */
    @Excel(name = "")
    private String jobVersion;

    /**  */
    @Excel(name = "")
    private Long jobStatus;

    /**  */
    @Excel(name = "")
    private Long idDatabaseLog;

    /**  */
    @Excel(name = "")
    private String tableNameLog;

    /**  */
    @Excel(name = "")
    private String createdUser;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdDate;

    /**  */
    @Excel(name = "")
    private String modifiedUser;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifiedDate;

    /**  */
    @Excel(name = "")
    private Integer useBatchId;

    /**  */
    @Excel(name = "")
    private Integer passBatchId;

    /**  */
    @Excel(name = "")
    private Integer useLogfield;

    /**  */
    @Excel(name = "")
    private String sharedFile;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdJob() {
        return idJob;
    }

    public void setIdJob(Long idJob) {
        this.idJob = idJob;
    }

    public Long getIdDirectory() {
        return idDirectory;
    }

    public void setIdDirectory(Long idDirectory) {
        this.idDirectory = idDirectory;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getExtendedDescription() {
        return extendedDescription;
    }

    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public String getJobVersion() {
        return jobVersion;
    }

    public void setJobVersion(String jobVersion) {
        this.jobVersion = jobVersion;
    }

    public Long getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Long jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Long getIdDatabaseLog() {
        return idDatabaseLog;
    }

    public void setIdDatabaseLog(Long idDatabaseLog) {
        this.idDatabaseLog = idDatabaseLog;
    }

    public String getTableNameLog() {
        return tableNameLog;
    }

    public void setTableNameLog(String tableNameLog) {
        this.tableNameLog = tableNameLog;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getUseBatchId() {
        return useBatchId;
    }

    public void setUseBatchId(Integer useBatchId) {
        this.useBatchId = useBatchId;
    }

    public Integer getPassBatchId() {
        return passBatchId;
    }

    public void setPassBatchId(Integer passBatchId) {
        this.passBatchId = passBatchId;
    }

    public Integer getUseLogfield() {
        return useLogfield;
    }

    public void setUseLogfield(Integer useLogfield) {
        this.useLogfield = useLogfield;
    }

    public String getSharedFile() {
        return sharedFile;
    }

    public void setSharedFile(String sharedFile) {
        this.sharedFile = sharedFile;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idJob", getIdJob())
            .append("idDirectory", getIdDirectory())
            .append("NAME", getNAME())
            .append("DESCRIPTION", getDESCRIPTION())
            .append("extendedDescription", getExtendedDescription())
            .append("jobVersion", getJobVersion())
            .append("jobStatus", getJobStatus())
            .append("idDatabaseLog", getIdDatabaseLog())
            .append("tableNameLog", getTableNameLog())
            .append("createdUser", getCreatedUser())
            .append("createdDate", getCreatedDate())
            .append("modifiedUser", getModifiedUser())
            .append("modifiedDate", getModifiedDate())
            .append("useBatchId", getUseBatchId())
            .append("passBatchId", getPassBatchId())
            .append("useLogfield", getUseLogfield())
            .append("sharedFile", getSharedFile())
            .toString();
    }
}
