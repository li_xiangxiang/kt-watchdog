package com.hzzftech.watchdog.busi.service.impl;

import java.util.List;

import com.hzzftech.watchdog.common.core.domain.entity.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hzzftech.watchdog.busi.mapper.KtRepositoryFileMapper;
import com.hzzftech.watchdog.busi.domain.KtRepositoryFile;
import com.hzzftech.watchdog.busi.service.IKtRepositoryFileService;
import com.hzzftech.watchdog.common.core.text.Convert;

/**
 * 仓库文件Service业务层处理
 * 
 * @author liquanxiang
 * @date 2021-12-04
 */
@Service
public class KtRepositoryFileServiceImpl implements IKtRepositoryFileService 
{
    @Autowired
    private KtRepositoryFileMapper ktRepositoryFileMapper;

    /**
     * 查询仓库文件
     * 
     * @param id 仓库文件主键
     * @return 仓库文件
     */
    @Override
    public KtRepositoryFile selectKtRepositoryFileById(Long id)
    {
        return ktRepositoryFileMapper.selectKtRepositoryFileById(id);
    }

    /**
     * 查询仓库文件列表
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 仓库文件
     */
    @Override
    public List<KtRepositoryFile> selectKtRepositoryFileList(KtRepositoryFile ktRepositoryFile)
    {
        return ktRepositoryFileMapper.selectKtRepositoryFileList(ktRepositoryFile);
    }

    /**
     * 新增仓库文件
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 结果
     */
    @Override
    public int insertKtRepositoryFile(KtRepositoryFile ktRepositoryFile)
    {
        return ktRepositoryFileMapper.insertKtRepositoryFile(ktRepositoryFile);
    }

    /**
     * 修改仓库文件
     * 
     * @param ktRepositoryFile 仓库文件
     * @return 结果
     */
    @Override
    public int updateKtRepositoryFile(KtRepositoryFile ktRepositoryFile)
    {
        return ktRepositoryFileMapper.updateKtRepositoryFile(ktRepositoryFile);
    }

    /**
     * 批量删除仓库文件
     * 
     * @param ids 需要删除的仓库文件主键
     * @return 结果
     */
    @Override
    public int deleteKtRepositoryFileByIds(String ids)
    {
        return ktRepositoryFileMapper.deleteKtRepositoryFileByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除仓库文件信息
     * 
     * @param id 仓库文件主键
     * @return 结果
     */
    @Override
    public int deleteKtRepositoryFileById(Long id)
    {
        return ktRepositoryFileMapper.deleteKtRepositoryFileById(id);
    }

    @Override
    public void clearRepositoryById(Long repositoryId) {
        ktRepositoryFileMapper.deleteKtRepositoryFileByRepositoryId(repositoryId);
    }

    @Override
    public void cleanRepositoryFileByIds(Long repositoryId, Long parentId, String fName) {
        ktRepositoryFileMapper.deleteRepositoryFileByIds( repositoryId,  parentId,  fName);
    }

    @Override
    public KtRepositoryFile selectRepositoryFileByIds(Long repositoryId, Long parentId, String fName) {
        return ktRepositoryFileMapper.selectRepositoryFileByIds(repositoryId,  parentId,  fName);
    }

    @Override
    public KtRepositoryFile selectKtRepositoryByName(String name) {
        return ktRepositoryFileMapper.selectKtRepositoryByName(name);
    }

    @Override
    public List<KtRepositoryFile> selectKtRepositoryFileByParentId(Long repositoryId) {
        return ktRepositoryFileMapper.selectKtRepositoryFileByParentId(repositoryId);
    }

    @Override
    public KtRepositoryFile selectRepoById(Long repositoryId, Long parentId) {
        return ktRepositoryFileMapper.selectRepoById(repositoryId, parentId);
    }

    @Override
    public KtRepositoryFile selectRepositoryFileByRepoIdAndFid(Long repoId, Long fileId) {
        return ktRepositoryFileMapper.selectRepositoryFileByRepoIdAndPid(repoId, fileId);
    }

    @Override
    public List<KtRepositoryFile> selectKtRepositoryByRepoId(Long repoId) {
        return ktRepositoryFileMapper.selectKtRepositoryByRepoId(repoId);
    }
}
