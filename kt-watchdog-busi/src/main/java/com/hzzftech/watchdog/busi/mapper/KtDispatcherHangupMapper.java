package com.hzzftech.watchdog.busi.mapper;

import java.util.List;
import com.hzzftech.watchdog.busi.domain.KtDispatcherHangup;

/**
 * 任务挂起Mapper接口
 * 
 * @author kt-watchdog
 * @date 2022-02-21
 */
public interface KtDispatcherHangupMapper 
{
    /**
     * 查询任务挂起
     * 
     * @param id 任务挂起主键
     * @return 任务挂起
     */
    public KtDispatcherHangup selectKtDispatcherHangupById(Long id);

    /**
     * 查询任务挂起列表
     * 
     * @param ktDispatcherHangup 任务挂起
     * @return 任务挂起集合
     */
    public List<KtDispatcherHangup> selectKtDispatcherHangupList(KtDispatcherHangup ktDispatcherHangup);

    /**
     * 新增任务挂起
     * 
     * @param ktDispatcherHangup 任务挂起
     * @return 结果
     */
    public int insertKtDispatcherHangup(KtDispatcherHangup ktDispatcherHangup);

    /**
     * 修改任务挂起
     * 
     * @param ktDispatcherHangup 任务挂起
     * @return 结果
     */
    public int updateKtDispatcherHangup(KtDispatcherHangup ktDispatcherHangup);

    /**
     * 删除任务挂起
     * 
     * @param id 任务挂起主键
     * @return 结果
     */
    public int deleteKtDispatcherHangupById(Long id);

    /**
     * 批量删除任务挂起
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteKtDispatcherHangupByIds(String[] ids);
}
