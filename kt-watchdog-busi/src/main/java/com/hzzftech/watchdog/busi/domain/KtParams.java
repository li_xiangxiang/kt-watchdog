package com.hzzftech.watchdog.busi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hzzftech.watchdog.common.annotation.Excel;
import com.hzzftech.watchdog.common.core.domain.BaseEntity;

/**
 * 运行参数对象 kt_params
 * 
 * @author liquanxiang
 * @date 2021-11-29
 */
public class KtParams extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 参数ID */
    private Long paramsId;

    /** 参数组ID */
    @Excel(name = "参数组ID")
    private Long paramsGroupId;

    /** 参数键 */
    @Excel(name = "参数键")
    private String paramsKey;

    /** 参数值 */
    @Excel(name = "参数值")
    private String paramsValue;

    /** 参数值是否加密 */
    @Excel(name = "参数值是否加密")
    private String encrypt;

    /** 状态 0正常 1禁用 */
    @Excel(name = "状态 0正常 1禁用")
    private String status;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    private String paramsGroupName;

    public String getParamsGroupName() {
        return paramsGroupName;
    }

    public void setParamsGroupName(String paramsGroupName) {
        this.paramsGroupName = paramsGroupName;
    }

    public void setParamsId(Long paramsId)
    {
        this.paramsId = paramsId;
    }

    public Long getParamsId() 
    {
        return paramsId;
    }
    public void setParamsGroupId(Long paramsGroupId) 
    {
        this.paramsGroupId = paramsGroupId;
    }

    public Long getParamsGroupId() 
    {
        return paramsGroupId;
    }
    public void setParamsKey(String paramsKey) 
    {
        this.paramsKey = paramsKey;
    }

    public String getParamsKey() 
    {
        return paramsKey;
    }
    public void setParamsValue(String paramsValue) 
    {
        this.paramsValue = paramsValue;
    }

    public String getParamsValue() 
    {
        return paramsValue;
    }
    public void setEncrypt(String encrypt) 
    {
        this.encrypt = encrypt;
    }

    public String getEncrypt() 
    {
        return encrypt;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("paramsId", getParamsId())
            .append("paramsGroupId", getParamsGroupId())
            .append("paramsKey", getParamsKey())
            .append("paramsValue", getParamsValue())
            .append("encrypt", getEncrypt())
            .append("status", getStatus())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .append("remark", getRemark())
            .toString();
    }
}
