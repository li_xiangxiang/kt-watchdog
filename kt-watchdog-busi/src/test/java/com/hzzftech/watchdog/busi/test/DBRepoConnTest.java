//package com.hzzftech.watchdog.busi.test;
//
//import com.hzzftech.watchdog.busi.core.executor.starter.KettleEnvironmentStarter;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.pentaho.di.core.Result;
//import org.pentaho.di.core.exception.KettleException;
//import org.pentaho.di.core.exception.KettleXMLException;
//import org.pentaho.di.core.util.EnvUtil;
//import org.pentaho.di.job.Job;
//import org.pentaho.di.job.JobMeta;
//import org.pentaho.di.repository.LongObjectId;
//import org.pentaho.di.repository.ObjectId;
//import org.pentaho.di.repository.RepositoryDirectoryInterface;
//import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
//import org.pentaho.di.trans.TransMeta;
//import org.quartz.JobDetail;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class DBRepoConnTest {
//
////    @Autowired
////    private KettleEnvironmentStarter starter;
//
////    @Test
////    public void testConnRepo() throws KettleException {
////        KettleDatabaseRepository repository = (KettleDatabaseRepository) KettleEnvironmentStarter.instance.getRepository();
////        RepositoryDirectoryInterface userHomeDirectory = repository.getUserHomeDirectory();
////        for (ObjectId id : userHomeDirectory.getDirectoryIDs()) {
//////            System.out.println(id.getId());
//////            RepositoryDirectoryInterface directory = repository.findDirectory(userHomeDirectory.getObjectId());
//////            System.out.println(directory);
////            System.out.println(repository.findDirectory(new LongObjectId(id)).getPath());
////        }
////
////        JobMeta jobMeta = repository.loadJob(new LongObjectId(1), "");
////        System.out.println(jobMeta.getName());
////    }
//
////    public static void main(String[] args) {
////        String transName = "";
////
////        try {
////            //初始化kettle环境
////            KettleEnvironment.init();
////            //创建资源库对象，此时的对象还是一个空对象
////            KettleDatabaseRepository repository = new KettleDatabaseRepository();
////            //创建资源库数据库对象，类似我们在spoon里面创建资源库
////            DatabaseMeta dataMeta =
////                    new DatabaseMeta("kt-watchdog","mysql","Native","127.0.0.1","kt-watchdog","3306","root","123456");
////            //资源库元对象,名称参数，id参数，描述等可以随便定义
////            KettleDatabaseRepositoryMeta kettleDatabaseMeta =
////                    new KettleDatabaseRepositoryMeta("kt-watchdog", "kt-watchdog-repo", "kt-watchdog repository",dataMeta);
////            //给资源库赋值
////            repository.init(kettleDatabaseMeta);
////            //连接资源库
////            repository.connect("admin","admin");
////            //根据变量查找到模型所在的目录对象,此步骤很重要。
////            RepositoryDirectoryInterface directory = repository.findDirectory("/enfo_worker/wxj");
////            //创建ktr元对象
////            TransMeta transformationMeta = ((Repository) repository).loadTransformation(transName, directory, null, true, null ) ;
////            // 需要的信息 JobMeta meta, JobExecutionConfiguration configuration
////            //创建ktr
////            Trans trans = new Trans(transformationMeta);
////            //执行ktr
////            trans.execute(null);
////            //等待执行完毕
////            trans.waitUntilFinished();
////
////            if(trans.getErrors()>0)
////            {
////                System.err.println("Transformation run Failure!");
////            }
////            else
////            {
////                System.out.println("Transformation run successfully!");
////            }
////        } catch (KettleException e) {
////            e.printStackTrace();
////        }
////    }
//
//////    @Test
////    public void test1() throws Exception {
////        JobMeta jobMeta = KettleEnvironmentStarter.instance.getRepository().loadJob(new LongObjectId(3), "");
////        Job job = new Job( KettleEnvironmentStarter.instance.getRepository(),jobMeta);
////        // nr 副本
////        Result execute = job.execute(0, new Result());
////        System.out.println("logId= "+execute.getLogChannelId());
////    }
//}
