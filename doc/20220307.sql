CREATE TABLE `kt_dispatcher_failed_msg` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' ID',
                                            `dp_id` bigint(20) DEFAULT NULL COMMENT '调度ID',
                                            `dp_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '调度名称',
                                            `failed_time` datetime DEFAULT NULL COMMENT '失败时间',
                                            `send_time` int(11) DEFAULT NULL COMMENT '发送次数',
                                            `last_send_time` datetime DEFAULT NULL COMMENT '最后一次发送时间',
                                            `status` char(2) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '状态（0 正常， 1禁用）',
                                            `user_id` bigint(20) DEFAULT NULL COMMENT '运维人员ID',
                                            `user_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
                                            `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
                                            `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '电话',
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

alter table kt_dispatcher add column `warn_level` int(11) DEFAULT NULL COMMENT '预警等级（1 低级 2中级 3高级）';
alter table kt_dispatcher add column `status` varchar(2) DEFAULT NULL COMMENT '状态 (0 正常 1禁用 2挂起)';

/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 07/03/2022 14:18:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
                                  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2008);
INSERT INTO `sys_role_menu` VALUES (2, 2009);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2011);
INSERT INTO `sys_role_menu` VALUES (2, 2012);
INSERT INTO `sys_role_menu` VALUES (2, 2013);
INSERT INTO `sys_role_menu` VALUES (2, 2014);
INSERT INTO `sys_role_menu` VALUES (2, 2015);
INSERT INTO `sys_role_menu` VALUES (2, 2016);
INSERT INTO `sys_role_menu` VALUES (2, 2017);
INSERT INTO `sys_role_menu` VALUES (2, 2018);
INSERT INTO `sys_role_menu` VALUES (2, 2019);
INSERT INTO `sys_role_menu` VALUES (2, 2020);
INSERT INTO `sys_role_menu` VALUES (2, 2021);
INSERT INTO `sys_role_menu` VALUES (2, 2022);
INSERT INTO `sys_role_menu` VALUES (2, 2023);
INSERT INTO `sys_role_menu` VALUES (2, 2024);
INSERT INTO `sys_role_menu` VALUES (2, 2025);
INSERT INTO `sys_role_menu` VALUES (2, 2026);
INSERT INTO `sys_role_menu` VALUES (2, 2027);
INSERT INTO `sys_role_menu` VALUES (2, 2034);
INSERT INTO `sys_role_menu` VALUES (2, 2035);
INSERT INTO `sys_role_menu` VALUES (2, 2036);
INSERT INTO `sys_role_menu` VALUES (2, 2037);
INSERT INTO `sys_role_menu` VALUES (2, 2038);
INSERT INTO `sys_role_menu` VALUES (2, 2039);
INSERT INTO `sys_role_menu` VALUES (2, 2040);
INSERT INTO `sys_role_menu` VALUES (2, 2041);
INSERT INTO `sys_role_menu` VALUES (2, 2042);
INSERT INTO `sys_role_menu` VALUES (2, 2043);
INSERT INTO `sys_role_menu` VALUES (2, 2044);
INSERT INTO `sys_role_menu` VALUES (2, 2045);
INSERT INTO `sys_role_menu` VALUES (2, 2046);
INSERT INTO `sys_role_menu` VALUES (2, 2048);
INSERT INTO `sys_role_menu` VALUES (2, 2051);
INSERT INTO `sys_role_menu` VALUES (2, 2052);
INSERT INTO `sys_role_menu` VALUES (2, 2053);
INSERT INTO `sys_role_menu` VALUES (2, 2054);
INSERT INTO `sys_role_menu` VALUES (2, 2055);
INSERT INTO `sys_role_menu` VALUES (2, 2056);
INSERT INTO `sys_role_menu` VALUES (2, 2057);
INSERT INTO `sys_role_menu` VALUES (2, 2058);
INSERT INTO `sys_role_menu` VALUES (2, 2059);
INSERT INTO `sys_role_menu` VALUES (2, 2060);
INSERT INTO `sys_role_menu` VALUES (2, 2061);
INSERT INTO `sys_role_menu` VALUES (2, 2062);
INSERT INTO `sys_role_menu` VALUES (2, 2063);
INSERT INTO `sys_role_menu` VALUES (2, 2064);
INSERT INTO `sys_role_menu` VALUES (2, 2065);
INSERT INTO `sys_role_menu` VALUES (2, 2066);
INSERT INTO `sys_role_menu` VALUES (2, 2067);
INSERT INTO `sys_role_menu` VALUES (2, 2068);
INSERT INTO `sys_role_menu` VALUES (2, 2069);
INSERT INTO `sys_role_menu` VALUES (2, 2082);
INSERT INTO `sys_role_menu` VALUES (2, 2083);
INSERT INTO `sys_role_menu` VALUES (2, 2084);
INSERT INTO `sys_role_menu` VALUES (2, 2085);
INSERT INTO `sys_role_menu` VALUES (2, 2086);
INSERT INTO `sys_role_menu` VALUES (2, 2087);
INSERT INTO `sys_role_menu` VALUES (2, 2088);
INSERT INTO `sys_role_menu` VALUES (2, 2089);
INSERT INTO `sys_role_menu` VALUES (2, 2090);
INSERT INTO `sys_role_menu` VALUES (2, 2091);
INSERT INTO `sys_role_menu` VALUES (2, 2092);
INSERT INTO `sys_role_menu` VALUES (2, 2093);
INSERT INTO `sys_role_menu` VALUES (2, 2094);
INSERT INTO `sys_role_menu` VALUES (2, 2095);
INSERT INTO `sys_role_menu` VALUES (2, 2096);
INSERT INTO `sys_role_menu` VALUES (2, 2097);
INSERT INTO `sys_role_menu` VALUES (2, 2098);
INSERT INTO `sys_role_menu` VALUES (2, 2099);
INSERT INTO `sys_role_menu` VALUES (2, 2100);
INSERT INTO `sys_role_menu` VALUES (2, 2101);
INSERT INTO `sys_role_menu` VALUES (2, 2102);
INSERT INTO `sys_role_menu` VALUES (2, 2103);
INSERT INTO `sys_role_menu` VALUES (2, 2104);
INSERT INTO `sys_role_menu` VALUES (2, 2105);
INSERT INTO `sys_role_menu` VALUES (2, 2106);
INSERT INTO `sys_role_menu` VALUES (2, 2107);
INSERT INTO `sys_role_menu` VALUES (2, 2108);
INSERT INTO `sys_role_menu` VALUES (2, 2109);
INSERT INTO `sys_role_menu` VALUES (2, 2110);
INSERT INTO `sys_role_menu` VALUES (2, 2111);
INSERT INTO `sys_role_menu` VALUES (2, 2112);
INSERT INTO `sys_role_menu` VALUES (2, 2113);
INSERT INTO `sys_role_menu` VALUES (2, 2114);
INSERT INTO `sys_role_menu` VALUES (2, 2115);
INSERT INTO `sys_role_menu` VALUES (2, 2116);
INSERT INTO `sys_role_menu` VALUES (2, 2117);
INSERT INTO `sys_role_menu` VALUES (2, 2118);
INSERT INTO `sys_role_menu` VALUES (2, 2119);
INSERT INTO `sys_role_menu` VALUES (2, 2120);
INSERT INTO `sys_role_menu` VALUES (2, 2121);
INSERT INTO `sys_role_menu` VALUES (2, 2122);
INSERT INTO `sys_role_menu` VALUES (2, 2123);
INSERT INTO `sys_role_menu` VALUES (2, 2124);
INSERT INTO `sys_role_menu` VALUES (2, 2125);
INSERT INTO `sys_role_menu` VALUES (2, 2126);
INSERT INTO `sys_role_menu` VALUES (2, 2127);
INSERT INTO `sys_role_menu` VALUES (2, 2128);
INSERT INTO `sys_role_menu` VALUES (2, 2129);
INSERT INTO `sys_role_menu` VALUES (2, 2130);
INSERT INTO `sys_role_menu` VALUES (2, 2131);
INSERT INTO `sys_role_menu` VALUES (2, 2132);
INSERT INTO `sys_role_menu` VALUES (2, 2133);
INSERT INTO `sys_role_menu` VALUES (2, 2134);
INSERT INTO `sys_role_menu` VALUES (2, 2135);
INSERT INTO `sys_role_menu` VALUES (2, 2136);
INSERT INTO `sys_role_menu` VALUES (2, 2137);
INSERT INTO `sys_role_menu` VALUES (2, 2138);
INSERT INTO `sys_role_menu` VALUES (2, 2139);
INSERT INTO `sys_role_menu` VALUES (2, 2140);
INSERT INTO `sys_role_menu` VALUES (2, 2141);
INSERT INTO `sys_role_menu` VALUES (2, 2142);
INSERT INTO `sys_role_menu` VALUES (2, 2143);
INSERT INTO `sys_role_menu` VALUES (2, 2144);
INSERT INTO `sys_role_menu` VALUES (2, 2145);
INSERT INTO `sys_role_menu` VALUES (2, 2146);
INSERT INTO `sys_role_menu` VALUES (2, 2147);
INSERT INTO `sys_role_menu` VALUES (2, 2148);
INSERT INTO `sys_role_menu` VALUES (2, 2149);
INSERT INTO `sys_role_menu` VALUES (2, 2150);
INSERT INTO `sys_role_menu` VALUES (2, 2151);
INSERT INTO `sys_role_menu` VALUES (2, 2152);

SET FOREIGN_KEY_CHECKS = 1;


/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 07/03/2022 14:19:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
                             `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
                             `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
                             `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                             `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '请求地址',
                             `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
                             `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
                             `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
                             `is_refresh` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
                             `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '权限标识',
                             `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '菜单图标',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注',
                             PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2153 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-11-24 16:39:23', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-11-24 16:39:23', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2021-11-24 16:39:23', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2021-11-24 16:39:23', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2021-11-24 16:39:23', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2021-11-24 16:39:23', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2021-11-24 16:39:23', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2021-11-24 16:39:23', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2021-11-24 16:39:23', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2021-11-24 16:39:23', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2021-11-24 16:39:23', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-11-24 16:39:23', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2021-11-24 16:39:23', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 10, '/monitor/job', 'menuItem', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2021-11-24 16:39:23', 'admin', '2022-01-05 14:18:50', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2021-11-24 16:39:23', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2021-11-24 16:39:23', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2021-11-24 16:39:23', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2021-11-24 16:39:23', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2021-11-24 16:39:23', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2021-11-24 16:39:23', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2021-11-24 16:39:23', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2021-11-24 16:39:24', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2021-11-24 16:39:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2021-11-24 16:39:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2021-11-24 16:39:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, 'KETTLE管理', 0, 0, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-bank', 'admin', '2021-11-24 17:11:49', 'admin', '2021-11-24 18:55:19', '');
INSERT INTO `sys_menu` VALUES (2001, 'GIT日志信息', 2046, 2, '/watchdog/repository-log', 'menuItem', 'C', '0', '1', 'watchdog:repository_log:view', '#', 'admin', '2021-11-24 19:10:17', 'admin', '2021-12-08 21:38:40', '文件资源库操作日志菜单');
INSERT INTO `sys_menu` VALUES (2002, '文件资源库操作日志查询', 2001, 1, '#', '', 'F', '0', '1', 'watchdog:repository_log:list', '#', 'admin', '2021-11-24 19:10:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '文件资源库操作日志新增', 2001, 2, '#', '', 'F', '0', '1', 'watchdog:repository_log:add', '#', 'admin', '2021-11-24 19:10:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '文件资源库操作日志修改', 2001, 3, '#', '', 'F', '0', '1', 'watchdog:repository_log:edit', '#', 'admin', '2021-11-24 19:10:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '文件资源库操作日志删除', 2001, 4, '#', '', 'F', '0', '1', 'watchdog:repository_log:remove', '#', 'admin', '2021-11-24 19:10:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '文件资源库操作日志导出', 2001, 5, '#', '', 'F', '0', '1', 'watchdog:repository_log:export', '#', 'admin', '2021-11-24 19:10:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '运行服务器管理', 2000, 0, '/watchdog/node', 'menuItem', 'C', '0', '1', 'busi:node:view', '#', 'admin', '2021-11-27 19:44:41', 'admin', '2021-12-14 23:17:09', '资源库节点菜单');
INSERT INTO `sys_menu` VALUES (2008, '运行服务器查询', 2007, 1, '#', '', 'F', '0', '1', 'busi:node:list', '#', 'admin', '2021-11-27 19:44:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '运行服务器新增', 2007, 2, '#', '', 'F', '0', '1', 'busi:node:add', '#', 'admin', '2021-11-27 19:44:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '运行服务器修改', 2007, 3, '#', '', 'F', '0', '1', 'busi:node:edit', '#', 'admin', '2021-11-27 19:44:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '运行服务器删除', 2007, 4, '#', '', 'F', '0', '1', 'busi:node:remove', '#', 'admin', '2021-11-27 19:44:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '运行服务器导出', 2007, 5, '#', '', 'F', '0', '1', 'busi:node:export', '#', 'admin', '2021-11-27 19:44:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, 'GIT文件资源库', 2046, 1, '/watchdog/repository', 'menuItem', 'C', '0', '1', 'busi:repository:view', '#', 'admin', '2021-11-27 19:44:54', 'admin', '2021-12-08 21:36:27', '文件资源库菜单');
INSERT INTO `sys_menu` VALUES (2014, '文件资源库查询', 2013, 1, '#', '', 'F', '0', '1', 'busi:repository:list', '#', 'admin', '2021-11-27 19:44:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '文件资源库新增', 2013, 2, '#', '', 'F', '0', '1', 'busi:repository:add', '#', 'admin', '2021-11-27 19:44:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '文件资源库修改', 2013, 3, '#', 'menuItem', 'F', '0', '1', 'watchdog:repository:edit', '#', 'admin', '2021-11-27 19:44:54', 'admin', '2022-03-07 13:30:05', '');
INSERT INTO `sys_menu` VALUES (2017, '文件资源库删除', 2013, 4, '#', 'menuItem', 'F', '0', '1', 'watchdog:repository:remove', '#', 'admin', '2021-11-27 19:44:54', 'admin', '2022-03-07 13:31:55', '');
INSERT INTO `sys_menu` VALUES (2018, '文件资源库导出', 2013, 5, '#', 'menuItem', 'F', '0', '1', 'watchdog:repository_log:exportFlag', '#', 'admin', '2021-11-27 19:44:54', 'admin', '2022-03-07 13:37:33', '');
INSERT INTO `sys_menu` VALUES (2019, '更新资源仓库', 2013, 6, '#', 'menuItem', 'F', '0', '1', 'watchdog:repository_log:updateRepository', '#', 'admin', '2021-11-28 17:28:04', 'admin', '2021-11-28 17:28:32', '');
INSERT INTO `sys_menu` VALUES (2020, '强制更新资源库', 2013, 7, '#', 'menuItem', 'F', '0', '1', 'watchdog:repository_log:forceUpdateRepository', '#', 'admin', '2021-11-28 17:29:17', 'admin', '2022-03-07 13:31:11', '');
INSERT INTO `sys_menu` VALUES (2021, '获取GIT分支', 2013, 8, '#', 'menuItem', 'F', '0', '1', 'watchdog:repository_log:allBranch', '#', 'admin', '2021-11-28 17:31:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '运行参数管理', 2000, 4, '/watchdog/paramsGroup', '', 'C', '0', '1', 'busi:paramsGroup:view', '#', 'admin', '2021-11-29 21:05:33', '', NULL, '运行参数组菜单');
INSERT INTO `sys_menu` VALUES (2023, '运行参数组查询', 2022, 1, '#', '', 'F', '0', '1', 'busi:paramsGroup:list', '#', 'admin', '2021-11-29 21:05:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '运行参数组新增', 2022, 2, '#', '', 'F', '0', '1', 'busi:paramsGroup:add', '#', 'admin', '2021-11-29 21:05:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '运行参数组修改', 2022, 3, '#', '', 'F', '0', '1', 'busi:paramsGroup:edit', '#', 'admin', '2021-11-29 21:05:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '运行参数组删除', 2022, 4, '#', '', 'F', '0', '1', 'busi:paramsGroup:remove', '#', 'admin', '2021-11-29 21:05:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '运行参数组导出', 2022, 5, '#', '', 'F', '0', '1', 'busi:paramsGroup:export', '#', 'admin', '2021-11-29 21:05:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '运行参数', 2000, 1, '/watchdog/params', '', 'C', '1', '1', 'busi:params:view', '#', 'admin', '2021-11-29 21:12:49', '', NULL, '运行参数菜单');
INSERT INTO `sys_menu` VALUES (2035, '运行参数查询', 2034, 1, '#', '', 'F', '0', '1', 'busi:params:list', '#', 'admin', '2021-11-29 21:12:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '运行参数新增', 2034, 2, '#', '', 'F', '0', '1', 'busi:params:add', '#', 'admin', '2021-11-29 21:12:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '运行参数修改', 2034, 3, '#', '', 'F', '0', '1', 'busi:params:edit', '#', 'admin', '2021-11-29 21:12:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '运行参数删除', 2034, 4, '#', '', 'F', '0', '1', 'busi:params:remove', '#', 'admin', '2021-11-29 21:12:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '运行参数导出', 2034, 5, '#', '', 'F', '0', '1', 'busi:params:export', '#', 'admin', '2021-11-29 21:12:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '仓库文件', 2013, 1, '/busi/repository_file', 'menuItem', 'C', '1', '1', 'busi:repository_file:view', '#', 'admin', '2021-12-04 15:24:42', 'admin', '2021-12-09 09:53:23', '仓库文件菜单');
INSERT INTO `sys_menu` VALUES (2041, '仓库文件查询', 2040, 1, '#', '', 'F', '0', '1', 'busi:repository_file:list', '#', 'admin', '2021-12-04 15:24:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '仓库文件新增', 2040, 2, '#', '', 'F', '0', '1', 'busi:repository_file:add', '#', 'admin', '2021-12-04 15:24:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '仓库文件修改', 2040, 3, '#', '', 'F', '0', '1', 'busi:repository_file:edit', '#', 'admin', '2021-12-04 15:24:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '仓库文件删除', 2040, 4, '#', '', 'F', '0', '1', 'busi:repository_file:remove', '#', 'admin', '2021-12-04 15:24:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '仓库文件导出', 2040, 5, '#', '', 'F', '0', '1', 'busi:repository_file:export', '#', 'admin', '2021-12-04 15:24:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, 'GIT资源库管理', 2000, 1, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-university', 'admin', '2021-12-08 21:35:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '文件资源库管理', 2000, 3, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-calendar-plus-o', 'admin', '2021-12-09 11:16:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '文件资源库-文件信息', 2048, 1, '/busi/file/msg', 'menuItem', 'C', '1', '1', 'busi:msg:view', '#', 'admin', '2021-12-09 13:49:21', 'admin', '2021-12-13 20:53:34', '文件资源库-文件信息菜单');
INSERT INTO `sys_menu` VALUES (2052, '文件资源库-文件信息查询', 2051, 1, '#', '', 'F', '0', '1', 'busi:msg:list', '#', 'admin', '2021-12-09 13:49:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '文件资源库-文件信息新增', 2051, 2, '#', '', 'F', '0', '1', 'busi:msg:add', '#', 'admin', '2021-12-09 13:49:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '文件资源库-文件信息修改', 2051, 3, '#', '', 'F', '0', '1', 'busi:msg:edit', '#', 'admin', '2021-12-09 13:49:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '文件资源库-文件信息删除', 2051, 4, '#', '', 'F', '0', '1', 'busi:msg:remove', '#', 'admin', '2021-12-09 13:49:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '文件资源库-文件信息导出', 2051, 5, '#', '', 'F', '0', '1', 'busi:msg:export', '#', 'admin', '2021-12-09 13:49:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '文件资源库', 2048, 1, '/busi/file/repository', '', 'C', '0', '1', 'busi:repository:view', '#', 'admin', '2021-12-09 13:49:45', '', NULL, '文件资源库菜单');
INSERT INTO `sys_menu` VALUES (2058, '文件资源库查询', 2057, 1, '#', '', 'F', '0', '1', 'busi:repository:list', '#', 'admin', '2021-12-09 13:49:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '文件资源库新增', 2057, 2, '#', '', 'F', '0', '1', 'busi:repository:add', '#', 'admin', '2021-12-09 13:49:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '文件资源库修改', 2057, 3, '#', '', 'F', '0', '1', 'busi:repository:edit', '#', 'admin', '2021-12-09 13:49:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '文件资源库删除', 2057, 4, '#', '', 'F', '0', '1', 'busi:repository:remove', '#', 'admin', '2021-12-09 13:49:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '文件资源库导出', 2057, 5, '#', '', 'F', '0', '1', 'busi:repository:export', '#', 'admin', '2021-12-09 13:49:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '文件版本信息', 2048, 1, '/busi/file/version', 'menuItem', 'C', '1', '1', 'busi:version:view', '#', 'admin', '2021-12-09 13:50:06', 'admin', '2021-12-13 20:53:59', '文件版本信息菜单');
INSERT INTO `sys_menu` VALUES (2064, '文件版本信息查询', 2063, 1, '#', '', 'F', '0', '1', 'busi:version:list', '#', 'admin', '2021-12-09 13:50:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2065, '文件版本信息新增', 2063, 2, '#', '', 'F', '0', '1', 'busi:version:add', '#', 'admin', '2021-12-09 13:50:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, '文件版本信息修改', 2063, 3, '#', '', 'F', '0', '1', 'busi:version:edit', '#', 'admin', '2021-12-09 13:50:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '文件版本信息删除', 2063, 4, '#', '', 'F', '0', '1', 'busi:version:remove', '#', 'admin', '2021-12-09 13:50:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2068, '文件版本信息导出', 2063, 5, '#', '', 'F', '0', '1', 'busi:version:export', '#', 'admin', '2021-12-09 13:50:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, '任务管理', 2000, 5, '#', 'menuItem', 'M', '1', '1', '', 'fa fa-navicon', 'admin', '2021-12-14 21:04:32', 'admin', '2022-02-20 21:48:21', '');
INSERT INTO `sys_menu` VALUES (2082, '任务调度管理', 2000, 5, '/busi/dispatcher', 'menuItem', 'C', '0', '1', 'busi:dispatcher:view', '#', 'admin', '2021-12-14 23:09:24', 'admin', '2022-02-20 21:48:07', '任务调度菜单');
INSERT INTO `sys_menu` VALUES (2083, '任务调度查询', 2082, 1, '#', '', 'F', '0', '1', 'busi:dispatcher:list', '#', 'admin', '2021-12-14 23:09:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2084, '任务调度新增', 2082, 2, '#', '', 'F', '0', '1', 'busi:dispatcher:add', '#', 'admin', '2021-12-14 23:09:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2085, '任务调度修改', 2082, 3, '#', '', 'F', '0', '1', 'busi:dispatcher:edit', '#', 'admin', '2021-12-14 23:09:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2086, '任务调度删除', 2082, 4, '#', '', 'F', '0', '1', 'busi:dispatcher:remove', '#', 'admin', '2021-12-14 23:09:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2087, '任务调度导出', 2082, 5, '#', '', 'F', '0', '1', 'busi:dispatcher:export', '#', 'admin', '2021-12-14 23:09:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2088, '任务调度日志', 2107, 1, '/busi/executionLog', 'menuItem', 'C', '0', '1', 'busi:executionLog:view', '#', 'admin', '2021-12-14 23:09:38', 'admin', '2021-12-15 00:03:34', '任务调度日志菜单');
INSERT INTO `sys_menu` VALUES (2089, '任务调度日志查询', 2088, 1, '#', '', 'F', '0', '1', 'busi:executionLog:list', '#', 'admin', '2021-12-14 23:09:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2090, '任务调度日志新增', 2088, 2, '#', '', 'F', '0', '1', 'busi:executionLog:add', '#', 'admin', '2021-12-14 23:09:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2091, '任务调度日志修改', 2088, 3, '#', '', 'F', '0', '1', 'busi:executionLog:edit', '#', 'admin', '2021-12-14 23:09:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2092, '任务调度日志删除', 2088, 4, '#', '', 'F', '0', '1', 'busi:executionLog:remove', '#', 'admin', '2021-12-14 23:09:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2093, '任务调度日志导出', 2088, 5, '#', '', 'F', '0', '1', 'busi:executionLog:export', '#', 'admin', '2021-12-14 23:09:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2094, '任务调度GIT', 2106, 1, '/busi/dispatcherGit', 'menuItem', 'C', '0', '1', 'busi:dispatcher:view', '#', 'admin', '2021-12-14 23:33:32', 'admin', '2021-12-15 00:04:59', '任务调度GIT菜单');
INSERT INTO `sys_menu` VALUES (2095, '任务调度查询', 2094, 1, '#', '', 'F', '0', '1', 'busi:dispatcher:list', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2096, '任务调度新增', 2094, 2, '#', '', 'F', '0', '1', 'busi:dispatcher:add', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2097, '任务调度修改', 2094, 3, '#', '', 'F', '0', '1', 'busi:dispatcher:edit', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2098, '任务调度删除', 2094, 4, '#', '', 'F', '0', '1', 'busi:dispatcher:remove', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2099, '任务调度导出', 2094, 5, '#', '', 'F', '0', '1', 'busi:dispatcher:export', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2100, '任务调度日志GIT', 2106, 1, '/busi/executionLogGit', 'menuItem', 'C', '0', '1', 'busi:executionLog:view', '#', 'admin', '2021-12-14 23:50:22', 'admin', '2021-12-15 00:02:33', '任务调度日志菜单');
INSERT INTO `sys_menu` VALUES (2101, '任务调度日志查询', 2100, 1, '#', '', 'F', '0', '1', 'busi:executionLog:list', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2102, '任务调度日志新增', 2100, 2, '#', '', 'F', '0', '1', 'busi:executionLog:add', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2103, '任务调度日志修改', 2100, 3, '#', '', 'F', '0', '1', 'busi:executionLog:edit', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2104, '任务调度日志删除', 2100, 4, '#', '', 'F', '0', '1', 'busi:executionLog:remove', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2105, '任务调度日志导出', 2100, 5, '#', '', 'F', '0', '1', 'busi:executionLog:export', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2106, 'GIT任务管理', 2069, 1, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-navicon', 'admin', '2021-12-14 23:55:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2107, '文件任务管理', 2069, 2, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-bars', 'admin', '2021-12-14 23:56:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2108, '错误预警', 2000, 8, '/busi/warnmsg', 'menuItem', 'C', '1', '1', 'busi:warnmsg:view', '#', 'admin', '2022-01-03 12:09:17', 'admin', '2022-03-06 15:58:52', 'kettle运行提醒信息菜单');
INSERT INTO `sys_menu` VALUES (2109, 'kettle运行提醒信息查询', 2108, 1, '#', '', 'F', '0', '1', 'busi:warnmsg:list', '#', 'admin', '2022-01-03 12:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2110, 'kettle运行提醒信息新增', 2108, 2, '#', '', 'F', '0', '1', 'busi:warnmsg:add', '#', 'admin', '2022-01-03 12:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2111, 'kettle运行提醒信息修改', 2108, 3, '#', '', 'F', '0', '1', 'busi:warnmsg:edit', '#', 'admin', '2022-01-03 12:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2112, 'kettle运行提醒信息删除', 2108, 4, '#', '', 'F', '0', '1', 'busi:warnmsg:remove', '#', 'admin', '2022-01-03 12:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2113, 'kettle运行提醒信息导出', 2108, 5, '#', '', 'F', '0', '1', 'busi:warnmsg:export', '#', 'admin', '2022-01-03 12:09:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2114, '定时任务', 2000, 9, '/busi/job', 'menuItem', 'C', '0', '1', 'busi:job:view', 'fa fa-hourglass-start', 'admin', '2022-01-05 14:27:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2115, '任务查询', 2114, 1, '#', 'menuItem', 'F', '0', '1', 'busi:job:list', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:49:34', '');
INSERT INTO `sys_menu` VALUES (2116, '任务新增', 2114, 2, '#', 'menuItem', 'F', '0', '1', 'busi:job:add', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:49:53', '');
INSERT INTO `sys_menu` VALUES (2117, '任务修改', 2114, 3, '#', 'menuItem', 'F', '0', '1', 'busi:job:edit', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:50:10', '');
INSERT INTO `sys_menu` VALUES (2118, '任务删除', 2114, 4, '#', 'menuItem', 'F', '0', '1', 'busi:job:remove', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:50:39', '');
INSERT INTO `sys_menu` VALUES (2119, '状态修改', 2114, 5, '#', 'menuItem', 'F', '0', '1', 'busi:job:changeStatus', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:50:56', '');
INSERT INTO `sys_menu` VALUES (2120, '任务详细', 2114, 6, '#', 'menuItem', 'F', '0', '1', 'busi:job:detail', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:51:24', '');
INSERT INTO `sys_menu` VALUES (2121, '任务导出', 2114, 7, '#', 'menuItem', 'F', '0', '1', 'busi:job:export', '#', 'admin', '2021-11-24 16:39:26', 'admin', '2022-03-07 13:51:43', '');
INSERT INTO `sys_menu` VALUES (2122, '数据库资源库管理', 2000, 3, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-database', 'admin', '2022-01-27 17:41:31', 'admin', '2022-01-27 17:42:01', '');
INSERT INTO `sys_menu` VALUES (2123, 'JOB管理', 2122, 1, '/busi/JOB', 'menuItem', 'C', '0', '1', 'busi:JOB:view', '#', 'admin', '2022-01-27 18:22:28', 'admin', '2022-01-29 11:12:51', '数据库资源库_job菜单');
INSERT INTO `sys_menu` VALUES (2124, '数据库资源库_job查询', 2123, 1, '#', '', 'F', '0', '1', 'busi:JOB:list', '#', 'admin', '2022-01-27 18:22:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2125, '数据库资源库_job删除', 2123, 4, '#', '', 'F', '0', '1', 'busi:JOB:remove', '#', 'admin', '2022-01-27 18:22:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2126, '转换管理', 2122, 1, '/busi/TRANSFORMATION', 'menuItem', 'C', '0', '1', 'busi:TRANSFORMATION:view', '#', 'admin', '2022-01-27 18:23:15', 'admin', '2022-01-29 11:13:24', '数据库资源库_trans菜单');
INSERT INTO `sys_menu` VALUES (2127, '数据库资源库_trans查询', 2126, 1, '#', '', 'F', '0', '1', 'busi:TRANSFORMATION:list', '#', 'admin', '2022-01-27 18:23:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2128, '数据库资源库_trans删除', 2126, 4, '#', '', 'F', '0', '1', 'busi:TRANSFORMATION:remove', '#', 'admin', '2022-01-27 18:23:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2129, '数据库任务管理', 2069, 3, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-database', 'admin', '2022-02-05 10:06:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2130, '任务调度DB', 2129, 1, '/busi/dispatcherDB', 'menuItem', 'C', '0', '1', 'busi:dispatcher:view', '#', 'admin', '2021-12-14 23:33:32', 'admin', '2021-12-15 00:04:59', '任务调度GIT菜单');
INSERT INTO `sys_menu` VALUES (2131, '任务调度查询', 2130, 1, '#', '', 'F', '0', '1', 'busi:dispatcher:list', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2132, '任务调度新增', 2130, 2, '#', '', 'F', '0', '1', 'busi:dispatcher:add', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2133, '任务调度修改', 2130, 3, '#', '', 'F', '0', '1', 'busi:dispatcher:edit', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2134, '任务调度删除', 2130, 4, '#', '', 'F', '0', '1', 'busi:dispatcher:remove', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2135, '任务调度导出', 2130, 5, '#', '', 'F', '0', '1', 'busi:dispatcher:export', '#', 'admin', '2021-12-14 23:33:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2136, '任务调度日志DB', 2129, 1, '/busi/executionLogDB', 'menuItem', 'C', '0', '1', 'busi:executionLog:view', '#', 'admin', '2021-12-14 23:50:22', 'admin', '2021-12-15 00:02:33', '任务调度日志菜单');
INSERT INTO `sys_menu` VALUES (2137, '任务调度日志查询', 2136, 1, '#', '', 'F', '0', '1', 'busi:executionLog:list', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2138, '任务调度日志新增', 2136, 2, '#', '', 'F', '0', '1', 'busi:executionLog:add', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2139, '任务调度日志修改', 2136, 3, '#', '', 'F', '0', '1', 'busi:executionLog:edit', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2140, '任务调度日志删除', 2136, 4, '#', '', 'F', '0', '1', 'busi:executionLog:remove', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2141, '任务调度日志导出', 2136, 5, '#', '', 'F', '0', '1', 'busi:executionLog:export', '#', 'admin', '2021-12-14 23:50:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2142, '任务挂起', 2082, 1, '/busi/hangup', '', 'C', '1', '1', 'busi:hangup:view', '#', 'admin', '2022-02-21 16:46:14', '', NULL, '任务挂起菜单');
INSERT INTO `sys_menu` VALUES (2143, '任务挂起查询', 2142, 1, '#', '', 'F', '0', '1', 'busi:hangup:list', '#', 'admin', '2022-02-21 16:46:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2144, '任务挂起新增', 2142, 2, '#', '', 'F', '0', '1', 'busi:hangup:add', '#', 'admin', '2022-02-21 16:46:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2145, '任务挂起修改', 2142, 3, '#', '', 'F', '0', '1', 'busi:hangup:edit', '#', 'admin', '2022-02-21 16:46:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2146, '任务挂起删除', 2142, 4, '#', '', 'F', '0', '1', 'busi:hangup:remove', '#', 'admin', '2022-02-21 16:46:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2147, '任务挂起导出', 2142, 5, '#', '', 'F', '0', '1', 'busi:hangup:export', '#', 'admin', '2022-02-21 16:46:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2148, '监控管理', 2000, 10, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-bar-chart-o', 'admin', '2022-02-22 10:34:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2149, '运行服务器监控', 2148, 1, '/watchdog/node/monitor', 'menuItem', 'C', '0', '1', 'busi:node:view', 'fa fa-microchip', 'admin', '2022-02-22 10:51:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2150, '任务监控', 2148, 2, '/busi/dispatcher/monitor', 'menuItem', 'C', '0', '1', 'busi:dispatcher:view', 'fa fa-hourglass-start', 'admin', '2022-02-27 19:22:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2151, '运维人员管理', 2148, 3, '/busi/monitor/user', 'menuItem', 'C', '0', '1', 'monitor:user:view', 'fa fa-user-o', 'admin', '2022-03-06 12:40:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2152, '执行一次', 2088, 6, '#', 'menuItem', 'F', '0', '1', 'busi:dispatcher:launch', '#', 'admin', '2022-03-07 13:42:07', '', NULL, '');

SET FOREIGN_KEY_CHECKS = 1;


/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 07/03/2022 14:22:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
                             `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
                             `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '祖级列表',
                             `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
                             `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                             `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人',
                             `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
                             `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '中房科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-11-24 16:39:21', '', NULL);
INSERT INTO `sys_dept` VALUES (110, 100, '0,100', '运维部门', 1, NULL, NULL, NULL, '0', '0', 'admin', '2022-03-06 12:26:20', '', NULL);

SET FOREIGN_KEY_CHECKS = 1;


/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 07/03/2022 14:22:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
                                  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`dict_id`) USING BTREE,
                                  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-11-24 16:39:30', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-11-24 16:39:30', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-11-24 16:39:30', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, 'GIT分支', 'busi_git_branch', '0', 'admin', '2021-11-28 17:40:33', '', NULL, 'GIT分支');
INSERT INTO `sys_dict_type` VALUES (101, '是否加密', 'ENCRYPT_STATUS', '0', 'admin', '2021-11-30 17:17:09', '', NULL, '加密状态：0加密 1不加密');
INSERT INTO `sys_dict_type` VALUES (102, 'GIT命令执行状态', 'GIT_EXECUTED_STATUS', '0', 'admin', '2021-12-03 16:21:03', '', NULL, 'GIT命令执行状态');
INSERT INTO `sys_dict_type` VALUES (103, '文件是否删除(逻辑删除)', 'del_flag', '0', 'admin', '2021-12-05 21:23:49', '', NULL, '文件是否删除(逻辑删除)');
INSERT INTO `sys_dict_type` VALUES (104, '文件类型', 'file_type', '0', 'admin', '2021-12-10 14:15:18', '', NULL, '文件类型');
INSERT INTO `sys_dict_type` VALUES (105, '执行状态', 'kt_exe_status', '0', 'admin', '2021-12-21 10:38:24', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (106, '任务类型', 'task_type', '0', 'admin', '2021-12-21 10:45:13', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (107, '资源库类型', 'repo_type', '0', 'admin', '2022-02-12 13:21:37', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (108, '预警等级', 'warn_level', '0', 'admin', '2022-03-06 14:49:09', '', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;


/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 07/03/2022 14:22:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
                                  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
                                  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典键值',
                                  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
                                  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
                                  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 122 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-11-24 16:39:31', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-11-24 16:39:32', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 2, '加密', '1', 'ENCRYPT_STATUS', '', 'primary', 'N', '0', 'admin', '2021-11-30 17:18:00', 'admin', '2021-11-30 19:55:19', '加密');
INSERT INTO `sys_dict_data` VALUES (102, 1, '不加密', '0', 'ENCRYPT_STATUS', '', 'warning', 'Y', '0', 'admin', '2021-11-30 17:20:35', 'admin', '2021-11-30 17:21:19', '不加密');
INSERT INTO `sys_dict_data` VALUES (103, 1, '执行成功', '0', 'GIT_EXECUTED_STATUS', '', 'success', 'Y', '0', 'admin', '2021-12-03 16:22:04', 'admin', '2021-12-03 16:23:27', '');
INSERT INTO `sys_dict_data` VALUES (104, 2, '执行失败', '1', 'GIT_EXECUTED_STATUS', '', 'warning', 'Y', '0', 'admin', '2021-12-03 16:22:39', 'admin', '2021-12-03 16:23:20', '');
INSERT INTO `sys_dict_data` VALUES (105, 1, '正常', '0', 'del_flag', NULL, NULL, 'Y', '0', 'admin', '2021-12-05 21:24:34', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 2, '删除', '1', 'del_flag', NULL, NULL, 'Y', '0', 'admin', '2021-12-05 21:24:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 1, '文件夹', '0', 'file_type', NULL, 'primary', 'Y', '0', 'admin', '2021-12-10 14:16:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 2, '文件', '1', 'file_type', NULL, 'info', 'Y', '0', 'admin', '2021-12-10 14:16:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 1, '等待执行', '1', 'kt_exe_status', NULL, 'default', 'Y', '0', 'admin', '2021-12-21 10:39:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 2, '正在执行', '2', 'kt_exe_status', NULL, 'primary', 'Y', '0', 'admin', '2021-12-21 10:39:50', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 3, '执行成功', '3', 'kt_exe_status', NULL, 'info', 'Y', '0', 'admin', '2021-12-21 10:40:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 4, '执行失败', '4', 'kt_exe_status', NULL, 'danger', 'Y', '0', 'admin', '2021-12-21 10:40:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 5, '强制停止', '5', 'kt_exe_status', NULL, 'warning', 'Y', '0', 'admin', '2021-12-21 10:41:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 1, '转换', '1', 'task_type', NULL, 'default', 'Y', '0', 'admin', '2021-12-21 10:45:58', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 2, 'job', '2', 'task_type', NULL, 'default', 'Y', '0', 'admin', '2021-12-21 10:46:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (116, 1, 'GIT资源库', '0', 'repo_type', NULL, NULL, 'Y', '0', 'admin', '2022-02-12 13:22:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (117, 2, '文件资源库', '1', 'repo_type', NULL, NULL, 'Y', '0', 'admin', '2022-02-12 13:22:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 1, '低级', '1', 'warn_level', '', 'primary', 'Y', '0', 'admin', '2022-03-06 14:50:00', 'admin', '2022-03-06 14:50:37', '');
INSERT INTO `sys_dict_data` VALUES (120, 2, '中级', '2', 'warn_level', NULL, 'warning', 'Y', '0', 'admin', '2022-03-06 14:50:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (121, 3, '高级', '3', 'warn_level', NULL, 'danger', 'Y', '0', 'admin', '2022-03-06 14:50:59', '', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
