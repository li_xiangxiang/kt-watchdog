ALTER TABLE kt_execution_log ADD COLUMN `job_id` BIGINT COMMENT '任务ID';
ALTER TABLE kt_execution_log ADD COLUMN `job_name` VARCHAR(255) COMMENT '任务名称';