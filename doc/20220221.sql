/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 21/02/2022 16:24:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kt_dispatcher_hangup
-- ----------------------------
DROP TABLE IF EXISTS `kt_dispatcher_hangup`;
CREATE TABLE `kt_dispatcher_hangup`  (
                                         `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
                                         `dp_id` int(11) NULL DEFAULT NULL COMMENT '调度ID',
                                         `dp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '调度名称',
                                         `begin_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
                                         `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
                                         `created_by` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                         `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                         `hang_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '挂起状态 1未挂起 2挂起 3挂起结束 4挂起失败',
                                         PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kt_dispatcher_hangup
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;


-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('任务挂起', '2082', '1', '/busi/hangup', 'C', '1', 'busi:hangup:view', '#', 'admin', sysdate(), '', null, '任务挂起菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('任务挂起查询', @parentId, '1',  '#',  'F', '0', 'busi:hangup:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('任务挂起新增', @parentId, '2',  '#',  'F', '0', 'busi:hangup:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('任务挂起修改', @parentId, '3',  '#',  'F', '0', 'busi:hangup:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('任务挂起删除', @parentId, '4',  '#',  'F', '0', 'busi:hangup:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('任务挂起导出', @parentId, '5',  '#',  'F', '0', 'busi:hangup:export',       '#', 'admin', sysdate(), '', null, '');

INSERT INTO `kt_watchdog`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2150, '任务监控', 2148, 2, '/busi/dispatcher/monitor', 'menuItem', 'C', '0', '1', 'busi:dispatcher:view', 'fa fa-hourglass-start', 'admin', '2022-02-27 19:22:38', '', NULL, '');

