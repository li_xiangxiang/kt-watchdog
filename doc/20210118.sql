/*
 Navicat Premium Data Transfer

 Source Server         : 8.141.49.92
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 8.141.49.92:3306
 Source Schema         : kt_watchdog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 18/01/2022 15:03:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kt_dispatcher
-- ----------------------------
DROP TABLE IF EXISTS `kt_dispatcher`;
CREATE TABLE `kt_dispatcher`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '调度文件ID',
  `dp_id` bigint(20) NULL DEFAULT NULL COMMENT '调度文件ID',
  `dp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '调度名称',
  `dp_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '调度类型 (0trans 1job)',
  `dp_repo_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '调度的仓库类型 （0GIT仓库 1文件资源仓库）',
  `dp_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '调度地址',
  `repository_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库地址',
  `carte_id` bigint(20) NULL DEFAULT NULL COMMENT 'carte服务ID',
  `params_group_id` bigint(20) NULL DEFAULT NULL COMMENT '参数组ID',
  `warn_msg_ids` varchar(130) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'kettle预警',
  `created_by` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_dispatcher_job
-- ----------------------------
DROP TABLE IF EXISTS `kt_dispatcher_job`;
CREATE TABLE `kt_dispatcher_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注信息',
  `dp_id` bigint(20) NULL DEFAULT NULL COMMENT '调度ID',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_dispatcher_steps
-- ----------------------------
DROP TABLE IF EXISTS `kt_dispatcher_steps`;
CREATE TABLE `kt_dispatcher_steps`  (
  `id` varchar(125) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT ' 主键',
  `task_sort` double NULL DEFAULT NULL COMMENT '排序',
  `step_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '步骤名称',
  `copy` int(11) NULL DEFAULT NULL COMMENT 'copyNr',
  `lines_read` double NULL DEFAULT NULL COMMENT '读取',
  `lines_written` double NULL DEFAULT NULL COMMENT '写入',
  `lines_input` double NULL DEFAULT NULL COMMENT '输入',
  `lines_output` double NULL DEFAULT NULL COMMENT '输出',
  `lines_updated` double NULL DEFAULT NULL COMMENT '更新',
  `lines_rejected` double NULL DEFAULT NULL COMMENT '拒绝',
  `log_errors` double NULL DEFAULT NULL COMMENT '错误',
  `status_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '状态',
  `log_seconds` double NULL DEFAULT NULL COMMENT '耗时',
  `speed` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '速度',
  `log_priortty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `stopped` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `paused` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `task_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_execution_log
-- ----------------------------
DROP TABLE IF EXISTS `kt_execution_log`;
CREATE TABLE `kt_execution_log`  (
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '日志ID',
  `task_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务名称',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `execution_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '执行方式',
  `slave_id` int(11) NULL DEFAULT NULL COMMENT '远程服务ID',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态（1等待执行，2正在执行，3执行成功，4执行失败，5强制停止）',
  `execution_configuration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '配置',
  `execution_log` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '任务日志详细信息',
  `execution_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '执行方式 1：串行，2并行',
  `task_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务组',
  `carte_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'carte服务的ID',
  `image_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片路径',
  `task_id` int(11) NULL DEFAULT NULL COMMENT '任务ID',
  `task_sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '排序',
  `batch_id` int(11) NULL DEFAULT NULL COMMENT '批次',
  `task_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务类型：1转换 2job 3任务组',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '备注',
  `dp_repo_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务仓库类型 0git仓库 1文件仓库',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kt_file_repository
-- ----------------------------
DROP TABLE IF EXISTS `kt_file_repository`;
CREATE TABLE `kt_file_repository`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源库名称',
  `directory` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件夹',
  `created_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kt_file_repository_msg
-- ----------------------------
DROP TABLE IF EXISTS `kt_file_repository_msg`;
CREATE TABLE `kt_file_repository_msg`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' ID',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父目录ID',
  `ancestors` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '祖先ID',
  `f_repo_id` bigint(20) NULL DEFAULT NULL COMMENT '所属资源库ID',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件名',
  `enable_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '当前版本',
  `file_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型 （0文件夹 1文件）',
  `file_size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `del_flag` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '删除标识（0正常，1删除）',
  `del_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件状态（0正常，1禁用）',
  `created_by` bigint(255) NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kt_file_version
-- ----------------------------
DROP TABLE IF EXISTS `kt_file_version`;
CREATE TABLE `kt_file_version`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `repo_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库ID',
  `file_id` bigint(20) NULL DEFAULT NULL COMMENT '文件ID',
  `version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '版本号',
  `_enable` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_deleted` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否被删除',
  `deleted_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  `deleted_by` bigint(255) NULL DEFAULT NULL COMMENT '删除人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kt_params
-- ----------------------------
DROP TABLE IF EXISTS `kt_params`;
CREATE TABLE `kt_params`  (
  `params_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' 参数ID',
  `params_group_id` bigint(20) NULL DEFAULT NULL COMMENT '参数组ID',
  `params_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数键',
  `params_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数值',
  `encrypt` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数值是否加密',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态 0正常 1禁用',
  `created_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者ID',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`params_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_params_group
-- ----------------------------
DROP TABLE IF EXISTS `kt_params_group`;
CREATE TABLE `kt_params_group`  (
  `params_group_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '参数组ID',
  `params_group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数组名',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态',
  `created_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`params_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_repository
-- ----------------------------
DROP TABLE IF EXISTS `kt_repository`;
CREATE TABLE `kt_repository`  (
  `repository_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '资源仓库ID',
  `repository_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源仓库名称',
  `repository_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源仓库路径',
  `repository_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源仓库描述',
  `repository_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源仓库类型',
  `git_auth_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git权限认证方式',
  `git_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git地址',
  `git_branch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git分支',
  `git_ssh_key` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git的SSHkey，用于认证',
  `git_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git用户名',
  `git_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git密码',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态',
  `created_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`repository_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_repository_file
-- ----------------------------
DROP TABLE IF EXISTS `kt_repository_file`;
CREATE TABLE `kt_repository_file`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' 主键',
  `repository_id` bigint(20) NULL DEFAULT NULL COMMENT '资源库ID',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '上级文件夹ID',
  `f_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件名称',
  `f_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型，后缀名',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '类型 0文件夹 1文件',
  `f_created_time` datetime(0) NULL DEFAULT NULL COMMENT '文件创建时间',
  `size` bigint(20) NULL DEFAULT NULL COMMENT '磁盘大小',
  `git_enable_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git仓库有效版本',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否启用 0启用 1禁用',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否被删除 0正常 1删除',
  `del_date` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kt_repository_log
-- ----------------------------
DROP TABLE IF EXISTS `kt_repository_log`;
CREATE TABLE `kt_repository_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'git仓库更新日志',
  `repository_id` bigint(20) NULL DEFAULT NULL COMMENT '资源库ID',
  `git_command` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git命令',
  `git_executed_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git命令的执行状态',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `git_remote_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git地址',
  `git_branch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git分支',
  `git_commited_time` datetime(0) NULL DEFAULT NULL COMMENT 'git最近一次提交时间',
  `git_commited_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git最近一次hash',
  `git_commited_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git最近一次提交人',
  `git_commited_message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git最近一次提交信息',
  `git_executed_error_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'git执行失败信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_repository_node
-- ----------------------------
DROP TABLE IF EXISTS `kt_repository_node`;
CREATE TABLE `kt_repository_node`  (
  `node_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `repository_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '节点名称',
  `node_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '节点IP地址',
  `node_port` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '节点端口',
  `node_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '节点用户名称',
  `node_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '节点密码',
  `node_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '节点描述',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态',
  `created_by` bigint(255) NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for kt_warn_msg
-- ----------------------------
DROP TABLE IF EXISTS `kt_warn_msg`;
CREATE TABLE `kt_warn_msg`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `contact_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系类型 （1短信 2email 3钉钉）',
  `phone_num` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电话号码',
  `e_mail_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'e_mail地址',
  `ding_web_hook` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '钉钉webhook',
  `ding_secret_prefix` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '钉钉消息前缀 最多10条',
  `status` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'kettle运行提醒信息' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
