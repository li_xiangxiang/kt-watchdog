//package com.hzzftech.watchdog;
//
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//
///**
// * web容器中进行部署
// *
// * @author kt-watchdog
// */
//public class TtWatchdogServletInitializer extends SpringBootServletInitializer
//{
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
//    {
//        return application.sources(TtWatchdogServletInitializer.class);
//    }
//}
