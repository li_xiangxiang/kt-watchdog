package com.hzzftech.watchdog.system.domain;

public class Item {
    private String _key;
    private String name;
    private Integer _value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_key() {
        return _key;
    }

    public void set_key(String _key) {
        this._key = _key;
    }

    public Integer get_value() {
        return _value;
    }

    public void set_value(Integer _value) {
        this._value = _value;
    }
}
