package com.hzzftech.watchdog.system.domain;

public class MonthData {

    private Integer _month;
    private Integer _count;

    public Integer get_month() {
        return _month;
    }

    public void set_month(Integer _month) {
        this._month = _month;
    }

    public Integer get_count() {
        return _count;
    }

    public void set_count(Integer _count) {
        this._count = _count;
    }
}
