package com.hzzftech.watchdog.system.mapper;

import com.hzzftech.watchdog.system.domain.Item;
import com.hzzftech.watchdog.system.domain.MonthData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface StatisticsMapper {

    List<Item> selectItems();
    List<MonthData> selectExeCount(@Param("status") String status);
}
