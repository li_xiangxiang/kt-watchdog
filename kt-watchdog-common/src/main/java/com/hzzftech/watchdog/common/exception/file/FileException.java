package com.hzzftech.watchdog.common.exception.file;

import com.hzzftech.watchdog.common.exception.base.BaseException;

/**
 * 文件信息异常类
 * 
 * @author kt-watchdog
 */
public class FileException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args)
    {
        super("file", code, args, null);
    }

    public FileException(String msg) {
        super("file", msg);
    }
}
