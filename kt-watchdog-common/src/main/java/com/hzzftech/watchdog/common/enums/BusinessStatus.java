package com.hzzftech.watchdog.common.enums;

/**
 * 操作状态
 * 
 * @author kt-watchdog
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
