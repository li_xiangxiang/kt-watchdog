package com.hzzftech.watchdog.common.exception;

/**
 * 演示模式异常
 * 
 * @author kt-watchdog
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
