package com.hzzftech.watchdog.common.enums;

/**
 * 数据源
 * 
 * @author kt-watchdog
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
